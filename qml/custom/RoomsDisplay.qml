import QtQuick 2.0
import Sailfish.Silica 1.0

SilicaListView {
    VerticalScrollDecorator {}

    id: list
    anchors.fill: parent

    property string type;

    spacing: Theme.paddingLarge
    clip: true

    currentIndex: -1
    property int savedY: 0

    function restoreAfterUpdate() {
        contentY = savedY
    }

    onDraggingChanged: {
        if (!dragging) {
            savedY = contentY
        }
    }


    onContentYChanged: {
        console.log(contentY)
        if (contentY != 0)
            savedY = contentY

    }

    onModelChanged: {
        contentY = savedY
    }

    PullDownMenu {
        MenuItem {
            text: "Settings"
            onClicked: pageStack.push("../pages/Settings.qml")
        }

        MenuItem {
            text: "Explore public rooms"
            visible: type === "Rooms"
            onClicked: pageStack.push("../pages/RoomDirectory.qml")
        }

        MenuItem {
            text: "Create new room"
            visible: type === "Rooms"
            onClicked: pageStack.push("../pages/CreateRoom.qml");
        }
    }

    section.property: "section_name"
    section.criteria: ViewSection.FullString
    section.delegate: sectionHeading

    ViewPlaceholder {
        visible: parent.count == 0
        text: "No " + type;
        hintText: type === "Rooms" ? "Pull down to join new rooms" : undefined
    }

    delegate: ListItem {
        id: item

        anchors.left: parent.left
        anchors.right: parent.righ

        anchors.leftMargin: Theme.horizontalPageMargin
        anchors.rightMargin: Theme.horizontalPageMargin


        menu: ContextMenu {
            MenuItem {
                text: "Leave"
                onClicked: item.remorseDelete(function() { rooms.leave(rid); });
            }
        }


        onClicked: {
            rooms.mark_read(rid);
            active_room = rid;
            if (is_invite) {
                pageStack.push("../pages/Invite.qml", { name: name, room_id: rid, is_direct: is_direct});
            } else {
                pageStack.push("../pages/Messages.qml", { room_name: name, room_id: rid});
            }
        }


        Image {
            id: img
            source: avatar ? avatar : ("image://theme/icon-m-chat?" + (pressed ? Theme.highlightColor : Theme.primaryColor))
            height: roomName.height + preview.height
            width: height

            onSourceChanged: console.log(source);
        }
        Label {
            id: roomName
            text: unread > 0 ? "(" + unread + ") " + name : name
            anchors.left: img.right
            anchors.leftMargin: Theme.horizontalPageMargin
            truncationMode: TruncationMode.Fade
            font.pixelSize: Theme.fontSizeMedium
            width: page.width - img.width - Theme.horizontalPageMargin - Theme.horizontalPageMargin
            font.bold: unread > 0
            color: Theme.primaryColor
        }
        Label {
            id: preview
            text: latestMessage
            anchors.top: roomName.bottom
            anchors.left: img.right
            anchors.right: parent.right
            anchors.leftMargin: Theme.horizontalPageMargin
            anchors.rightMargin: Theme.horizontalPageMargin

            font.pixelSize: Theme.fontSizeExtraSmall
            truncationMode: TruncationMode.Fade
            width: page.width - img.width - Theme.horizontalPageMargin - Theme.horizontalPageMargin
            color: Theme.secondaryColor
        }
    }
}
