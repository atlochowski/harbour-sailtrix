import QtQuick 2.0
import Sailfish.Silica 1.0
import RoomDirectoryBackend 1.0

Page {
    id: page

    property string searchTerm
    property int col_height
    property bool display_noresults

    allowedOrientations: Orientation.All

    RoomDirectoryBackend {
        id: backend
    }

    SilicaListView {

        id: list
        anchors.fill: parent
        header: Column {
            id: col
            width: parent.width

            PageHeader {
                title: "Explore public rooms"
            }

            SearchField {
                width: parent.width
                placeholderText: "Find a room..."
                id: search
                onTextChanged: searchTerm = text
                EnterKey.enabled: text.length > 0
                EnterKey.iconSource: "image://theme/icon-m-enter-accept"
                EnterKey.onClicked: {
                    backend.search(text, combo.currentIndex == 1)
                    display_noresults = true
                }
            }

            ComboBox {
                id: combo
                label: "Search in"
                width: parent.width
                menu: ContextMenu {
                    MenuItem { text: "My homeserver" }
                    MenuItem { text: "Matrix.org" }
                }
            }

            onHeightChanged: col_height = height
        }


        ViewPlaceholder {
            id: ph
            text: "No results"
            hintText: "Please try a different search term"
            enabled: list.count == 0 && searchTerm && !backend.searching && display_noresults
            onEnabledChanged: {
                console.log("Enabled:" + enabled)
            }
            verticalOffset: col_height
        }

        ViewPlaceholder {
            id: ph2
            text: "Search for a room"
            hintText: "Use the search box to search for a room"
            enabled: searchTerm == ""
            onEnabledChanged: {
                console.log("Enabled:" + enabled)
            }
            verticalOffset: col_height
        }

        spacing: Theme.paddingLarge
        model: backend.results
        delegate: ListItem {
            height: r.height
           // width: parent.width
            anchors.left: parent.left
            anchors.right: parent.right

            anchors.leftMargin: Theme.horizontalPageMargin
            anchors.rightMargin: Theme.horizontalPageMargin

            onClicked: {
                pageStack.push(join, { name: name, num_members: joined_count, room_id: room_id, avatar_path: avatar_path})
            }

            Row {
                id: r
                width: parent.width
                height: n.height + d.height + a.height

                spacing: Theme.paddingMedium

                Image {
                    id: img
                    source: avatar_path ? avatar_path : "image://theme/icon-m-chat"
                    height: Theme.iconSizeMedium
                    width: height

                }

                Column {
                    id: c
                    width: parent.width - img.width - Theme.paddingMedium
                    height: n.height + d.height + a.height

                    Label {
                        text: name
                        font.pixelSize: Theme.fontSizeMedium
                        font.weight: Font.Bold
                        truncationMode: TruncationMode.Fade
                        width: parent.width
                        id: n
                    }

                    Label {
                        text: model.alias
                        id: a
                        font.pixelSize: Theme.fontSizeExtraSmall
                        font.weight: Font.Light
                    }

                    Label {
                        text: description
                        font.pixelSize: Theme.fontSizeSmall
                        truncationMode: TruncationMode.Fade
                        width: parent.width
                        id: d
                    }
                }
            }
        }
    }

    PageBusyIndicator {
        running: backend.searching
    }

    Component {
        id: join
        JoinPublicRoom {
            onJoined: {
                console.log("Join room done");
                pageStack.replace("Messages.qml", { room_name: name, room_id: room_id});
            }
        }
    }
}
