import QtQuick 2.0
import Sailfish.Silica 1.0
import LoginBridge 1.0

Dialog {
    id: loginDialog

    allowedOrientations: Orientation.All
    onAccepted: {
        bridge.homeserverUrl = homeserverUrl.text
        bridge.username = username.text
        bridge.password = password.text
        bridge.login()
        pageStack.push("LoginWaiting.qml", { lBridge: bridge });
    }

    LoginBridge {
        id: bridge
    }

    Column {
        id: column

        width: parent.width
        spacing: Theme.paddingLarge

        DialogHeader {
            title: qsTr("Login")
        }

        TextField {
            label: "Homeserver"
            placeholderText: "Homeserver URL (matrix.org)"
            width: parent.width
            id: homeserverUrl
            EnterKey.iconSource: "image://theme/icon-m-enter-next"
            EnterKey.onClicked: username.focus = true
        }

        TextField {
            label: "Username"
            placeholderText: "Username (user1)"
            width: parent.width
            id: username
            EnterKey.iconSource: "image://theme/icon-m-enter-next"
            EnterKey.onClicked: password.focus = true
        }

        PasswordField {
            label: "Password"
            width: parent.width
            id: password
            EnterKey.iconSource: "image://theme/icon-m-enter-accept"
            EnterKey.onClicked: {
                bridge.homeserverUrl = homeserverUrl.text
                bridge.username = username.text
                bridge.password = password.text
                bridge.login()
                pageStack.push("LoginWaiting.qml", { lBridge: bridge });
            }
        }
    }
}
