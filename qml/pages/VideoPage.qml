import QtQuick 2.0
import Sailfish.Silica 1.0
import Nemo.Notifications 1.0
import QtMultimedia 5.0

Page {
    id: page

    property string path;
    property string name;
    property string durationStr;

    allowedOrientations: Orientation.All

    Notification {
        id: saved
        summary: "Video saved"
        body: "Video saved to Videos directory"
        expireTimeout: 1500
        previewBody: "Video saved";
        icon: "image://theme/icon-s-cloud-download"
    }

    Notification {
        id: error
        summary: "Video error"
        body: "Video not saved to Videos directory"
        expireTimeout: 1500
        previewBody: "Video error";
        icon: "image://theme/icon-s-blocked"
    }

    MediaPlayer {
        id: player
        source: path
        autoPlay: true
        autoLoad: true
        onDurationChanged: {
            var secondsA = Math.floor(duration / 1000)
            var date = new Date(null);
            if (secondsA < 0)
                secondsA = 0
            date.setSeconds(secondsA);
            var durationString = date.toISOString().substr(11, 8);
            if (durationString.indexOf("00") === 0)
                durationString = durationString.substr(3)
            durationStr = durationString
        }
    }

    SilicaFlickable {
        anchors.fill: parent

        Column {
            width: page.width
            height: page.height

            PageHeader {
                title: name
                id: header
            }

            VideoOutput {
                source: player
                width: parent.width
                height: parent.height - header.height - r.height
                fillMode: Image.PreserveAspectFit

                IconButton {
                    anchors.centerIn: parent
                    icon.source: "image://theme/icon-l-" + (player.playbackState === MediaPlayer.PlayingState ? "pause" : "play")
                    height: Theme.itemSizeLarge
                    width: Theme.itemSizeLarge
                    icon.height: height
                    icon.width: width
                    visible: player.playbackState != MediaPlayer.PlayingState
                    onClicked: player.play()
                }

                MouseArea {
                    anchors.fill: parent
                    enabled: (player.playbackState == MediaPlayer.PlayingState)
                    onClicked: player.pause()
                }
            }

            Row {
                id: r
                height: p.height

                Label {
                    text: durationStr
                    id: durationLabel
                }

                Slider {
                    id: p
                    enabled: player.seekable
                    maximumValue: player.duration
                    value: player.position
                    onReleased: {
                        player.seek(value);
                        value = Qt.binding(function() { return player.position })
                    }
                    width: page.width - durationLabel.width - Theme.paddingMedium
                }
            }
        }

        PageBusyIndicator {
            running: player.status === MediaPlayer.Loading
        }
    }
}
