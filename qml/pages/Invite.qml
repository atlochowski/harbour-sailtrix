import QtQuick 2.0
import Sailfish.Silica 1.0
import InviteBackend 1.0

Page {
    id: page

    property string room_id
    property string name
    property bool is_direct

    allowedOrientations: Orientation.All

    PageHeader {
        title: qsTr("Invite")
    }

    InviteBackend {
        id: backend
        onJoinDone: {
            pageStack.replace("Messages.qml", { room_name: name, room_id: room_id});
        }
        onRejectDone: {
            pageStack.pop();
        }
    }

    Column {
        id: column
        width: page.width
        anchors.verticalCenter: parent.verticalCenter
        spacing: Theme.paddingLarge

        Label {
            text: "You've been invited to" + (is_direct ? " chat with" : "")
            color: Theme.highlightColor
            font.family: Theme.fontFamilyHeading
            font.pixelSize: Theme.fontSizeMedium
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Label {
            text: name
            color: Theme.highlightColor
            font.family: Theme.fontFamily
            font.pixelSize: Theme.fontSizeLarge
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Button {
            text: "Join"
            anchors.horizontalCenter: parent.horizontalCenter
            onClicked: {
                if (is_direct) {
                    backend.join_direct(room_id, name)
                } else {
                    backend.join(room_id)
                }

                indicator.running = true;
            }
        }

        Button {
            text: "Reject"
            anchors.horizontalCenter: parent.horizontalCenter
            onClicked: {
                backend.reject(room_id);
                indicator.running = true;
            }
        }
    }

    PageBusyIndicator {
        id: indicator
    }
}
