import QtQuick 2.4
import Sailfish.Silica 1.0
import Messages 1.0
import Sailfish.Pickers 1.0
import QtMultimedia 5.0
import Nemo.Notifications 1.0
import "../custom"

Page {
    id: page

    allowedOrientations: Orientation.All

    property string room_id
    property string room_name
    property string file_name;
    property bool downloading;

    function formatTimestamp(timestamp) {
        var date = new Date(timestamp);
        var today = new Date();

        if (date.getDate() === today.getDate()
                && date.getMonth() === today.getMonth()
                && date.getFullYear() === today.getFullYear()) {
            console.log("is today");
            return date.toLocaleTimeString(Qt.locale(), "H:mm");
        } else {
            return date.toLocaleDateString(Qt.locale(), "MMM d");
        }
    }

    Notification {
        id: saved
        summary: "File saved"
        body: "File saved to Downloads directory"
        expireTimeout: 1500
        previewBody: "File saved";
        icon: "image://theme/icon-s-cloud-download"
    }

    Messages {
        id: backend
        rid: room_id
        onMessagesChanged: {
            console.log("Messages changed");
            if (list.atYEnd || !fullyLoaded) {
                list.positionViewAtEnd();
                fully_read();
                console.log("Going to end");
            }
        }

        onNewMessage: {
            console.log("new message");

            if (!list.atYEnd) {
                goToBottom.visible = true;
            } else {
                list.positionViewAtEnd();
                fully_read();
                console.log("Going to end");

            }
        }

        onFullyLoadedChanged: {
            console.log("LOADED:" + fullyLoaded);
        }

        onFileDownloaded: {
            downloading = false
            saved.publish();
        }

    }

    PageHeader {
        title: room_name
        anchors.top: page.top
        id: page_header
    }

    SilicaFlickable {
        width: parent.width
        height: parent.height - page_header.height
        anchors.bottom: parent.bottom


        SilicaListView {
            VerticalScrollDecorator {}

            id: list
            width: page.width
            spacing: Theme.paddingLarge

            anchors.top: parent.top
            anchors.bottom: messageArea.top
            anchors.bottomMargin: Theme.paddingLarge
            clip: true

           /* header: PageHeader {
                title: room_name
                anchors.top:
            } */

            model: backend.messages

            delegate: ListItem {
                height: chatBox.height + img.height + the_menu.height
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottomMargin: Theme.paddingMedium
                id: item

                anchors.leftMargin: Theme.horizontalPageMargin
                anchors.rightMargin: Theme.horizontalPageMargin

                menu: ContextMenu {
                    id: the_menu;
                    MenuItem {
                        text: "Reply"
                        onClicked: {
                            replyLabel.visible = true;
                            replyLabel.text = "Replying to " + (display_name ? display_name : user_id);
                            replyRect.visible = true;
                            list.anchors.bottom = replyRect.top;
                            backend.reply_mid = event_id;
                            backend.reply_orig_uid = user_id
                            backend.reply_orig_body = orig_body
                            backend.reply_orig_formatted = content
                            backend.edit_event_id = "";
                        }
                        visible: event_id != undefined
                    }

                    MenuItem {
                        text: "Edit"
                        onClicked: {
                            replyLabel.visible = true;
                            replyLabel.text = "Editing message";
                            replyRect.visible = true;
                            list.anchors.bottom = replyRect.top;
                            messageArea.text = orig_body;
                            messageArea.focus = true;
                            backend.edit_event_id = event_id;
                            backend.reply_mid = "";
                            console.log("editing event " + event_id);
                        }

                        visible: is_self && !is_deleted

                    }

                    MenuItem {
                        text: "Delete"
                        onClicked: item.remorseDelete(function() { backend.redact(event_id);
                            texts.text = "<b>" + (display_name ? display_name : user_id) + "</b><br>🗑️ Message deleted"; } );
                        visible: is_self && !is_deleted
                    }

                }

                Image {
                    id: icon
                    height: Theme.itemSizeExtraSmall
                    width: height

                    visible: !grouped

                    source: avatar ? avatar : "image://theme/icon-m-media-artists"
                    anchors.right: is_self ? parent.right : undefined


                    MouseArea {
                        anchors.fill: parent
                        onClicked: pageStack.push("User.qml", { user_id: user_id, display_name: display_name, avatar_url: avatar, display_ignore: !is_self, prev_page: pageStack.currentPage })
                    }

                    onStatusChanged: {
                        if (status == Image.Error) {
                            source = "image://theme/icon-m-media-artists";
                        }
                    }
                }


                Rectangle {
                    id: chatBox
                    radius: 5
                    anchors.right: is_self ? icon.left : parent.right
                    anchors.left: !is_self ? icon.right : parent.left
                    color: is_notice || is_image ? Theme.rgba(Theme.primaryColor, 0) : ((!is_self ? Theme.rgba(Theme.primaryColor, Theme.opacityFaint) : Theme.rgba(Theme.highlightBackgroundColor, Theme.highlightBackgroundOpacity)));
                    width: texts.paintedWidth + Theme.paddingMedium
                    height: is_image ? texts.contentHeight : texts.contentHeight + Theme.paddingMedium + audio.height + vid.height + audio_dl.height + dl_file.height
                    anchors.rightMargin: is_self ? 0 : Theme.itemSizeExtraSmall
                    anchors.leftMargin: is_self ? Theme.itemSizeExtraSmall : 0


                    MediaPlayer {
                        id: player
                        autoPlay: false
                        source: image_path
                    }

                    Label {
                        id: texts

                        text: is_image ?  "<style>small{color:" + Theme.secondaryColor + ";</style>" + (grouped ?"" : "<b>" + (display_name ? display_name : user_id) + "</b>") + " <small>" + formatTimestamp(timestamp) + "</small>" : "<style>a:link{color:" + Theme.highlightColor + ";} small { color:" + Theme.secondaryColor + ";}</style>" + (grouped ? "" : "<b>" + (display_name ? display_name : user_id) + "</b>") +" <small>" + formatTimestamp(timestamp) + "</small><br>" + content
                        color: highlighted ? Theme.highlightColor : Theme.primaryColor
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        leftPadding: Theme.paddingSmall
                        font.pixelSize: Theme.fontSizeSmall

                        width: is_image ? undefined :  page.width - icon.width - Theme.itemSizeExtraSmall - Theme.paddingSmall - (Theme.horizontalPageMargin*2)

                        textFormat: Text.RichText

                        onLinkActivated: {
                            backend.openLink(link);
                        }

                    }


                    Button {
                        id: audio_dl
                        anchors.top: texts.bottom
                        text: "Download audio"
                        visible: is_audio != undefined && is_audio && !image_path
                        height: is_audio == undefined || !is_audio || is_deleted || image_path ? 0 : Theme.itemSizeMedium
                        onClicked: {
                            backend.download(img_url)
                            audio_indicator.running = false
                        }
                        BusyIndicator {
                            id: audio_indicator
                            anchors.centerIn: parent
                        }

                        anchors.leftMargin: Theme.paddingSmall
                    }

                    Audio {
                        id: audio
                        path: image_path
                        anchors.top: texts.bottom
                        width: is_image ? undefined : page.width - icon.width - Theme.itemSizeExtraSmall - Theme.paddingSmall
                        visible: is_audio != undefined && is_audio && image_path != undefined && image_path != ""
                        height:  is_audio == undefined || !is_audio || is_deleted || !image_path || image_path == ""? 0 : Theme.iconSizeMedium
                        onVisibleChanged: {
                            audio_indicator.running = false
                        }
                        anchors.leftMargin: Theme.paddingSmall
                    }

                    Button {
                        id: vid
                        anchors.leftMargin: Theme.paddingSmall

                        text: image_path ? "View video" : "Download"
                        onClicked: {
                            if (image_path) {
                                pageStack.push("VideoPage.qml", { path: image_path, name: content})
                            } else {
                               backend.download(img_url);
                               vid_indicator.running = true;
                            }
                        }

                        anchors.top: texts.bottom
                        visible: is_video != undefined && is_video
                        height:  is_video == undefined || !is_video || is_deleted ? 0 : Theme.itemSizeMedium

                        BusyIndicator {
                            id: vid_indicator
                            anchors.centerIn: parent
                        }

                        onTextChanged: {
                            if (text == "View video") {
                                vid_indicator.running = false
                            }
                        }
                    }

                    Button {
                        id: dl_file
                        anchors.leftMargin: Theme.paddingSmall
                        text: "Download file"
                        onClicked: {
                            backend.download(img_url)
                            downloading = true
                        }
                        anchors.top: texts.bottom
                        visible: is_file != undefined && is_file
                        height:  is_file == undefined || !is_file || is_deleted ? 0 : Theme.itemSizeMedium

                        BusyIndicator {
                            id: dl_indicator
                            anchors.centerIn: parent
                            running: downloading
                        }
                    }
                }

                AnimatedImage {
                    id: img
                    visible: is_image
                    anchors.right: is_self ? icon.left : parent.right
                    anchors.left:  is_self ? (width < parent.width) ? undefined : parent.left : icon.right
                    anchors.leftMargin: is_self ? icon.width + Theme.paddingSmall : undefined
                    anchors.rightMargin: is_self ? undefined : icon.width + Theme.paddingSmall
                    anchors.top: chatBox.bottom
                    fillMode: Image.PreserveAspectFit
                    source: (image_path) ? image_path : "image://theme/icon-l-image"
                    onSourceChanged: console.log(source);
                    onPaintedWidthChanged: {
                        if (paintedWidth < width) {
                            width = paintedWidth;
                        }
                    }
                    height: is_deleted || !is_image ? 0 : undefined

                    MouseArea {
                        anchors.fill: parent
                        onClicked: pageStack.push("PictureDisplay.qml", { image_name: content, mxc: img_url })
                    }
                }

            }

            onYChanged: {
                if (list.atYEnd) {
                    backend.fully_read();
                }
            }
        }

        Rectangle {
            color: Theme.rgba(Theme.primaryColor, 0)
            //color: Theme.colorScheme === Theme.DarkOnLight
                 //  ? Qt.darker(Theme.highlightBackgroundColor, Theme.highlightBackgroundOpacity)
                   //: Qt.lighter(Theme.highlightBackgroundColor, Theme.highlightBackgroundOpacity)
            width: parent.width
            height: replyLabel.height
            anchors.bottom: messageArea.top
            visible: false
            id: replyRect
            anchors.left: parent.left
            anchors.right: parent.right

            Label {
                id: replyLabel
                visible: false
                truncationMode: elide
                topPadding: Theme.paddingSmall
                bottomPadding: Theme.paddingSmall
                anchors.leftMargin: Theme.horizontalPageMargin
                anchors.rightMargin: Theme.horizontalPageMargin
                anchors.left: parent.left
                anchors.right: cancelButton.left
            }

            IconButton {
                id: cancelButton
                height: parent.height
                width: height
                icon.source: "image://theme/icon-m-cancel"
                anchors.right: parent.right
                onClicked: {
                    replyRect.visible = false;
                    list.anchors.bottom = messageArea.bottom
                    messageArea.text = "";
                    backend.reply_mid = "";
                    backend.edit_event_id = "";
                }
            }
        }

        TextArea {
            label: "Message"
            placeholderText: "Send message to room"
            id: messageArea
            anchors.bottom: parent.bottom
            anchors.right: sendButton.left
            anchors.left: parent.left
            placeholderColor: Theme.secondaryHighlightColor
            font.pixelSize: Theme.fontSizeSmall
            color: Theme.highlightColor

            background: Rectangle {
                width: messageArea.width
                height: messageArea.height
                color: Theme.rgba(Theme.primaryColor, 0)

               // color: Theme.colorScheme === Theme.DarkOnLight
                      // ? Qt.darker(Theme.highlightBackgroundColor, Theme.highlightBackgroundOpacity)
                       //: Qt.lighter(Theme.highlightBackgroundColor, Theme.highlightBackgroundOpacity)
            }

        }


        IconButton {
            id: sendButton
            icon.source: "image://theme/icon-m-send"
            width: Theme.itemSizeMedium
            anchors.bottom: parent.bottom
            anchors.right: parent.right

            height: messageArea.height
            enabled: messageArea.text != ""

            onClicked: {
                backend.send(messageArea.text);
                messageArea.text = "";
                list.positionViewAtEnd()
                replyRect.visible = false;
                list.anchors.bottom = messageArea.top
            }
        }

        Button {
            id: goToBottom
            text: "New Messages"
            width: parent.width - (parent.width * .20)
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottomMargin: messageArea.height + Theme.paddingLarge
            opacity: 1
            visible: false;
            backgroundColor: Theme.highlightBackgroundColor
            color: Theme.primaryColor
            onClicked: {
                list.positionViewAtEnd();
                visible = false;
            }
        }

        PageBusyIndicator {
            running: !backend.fullyLoaded
        }

        PushUpMenu {
            MenuItem {
                text: "Send file"
                onClicked: pageStack.push(filePickerPage)
            }
        }
    }

    Component {
        id: filePickerPage
        ContentPickerPage {
            onSelectedContentPropertiesChanged: {
                page.file_name = selectedContentProperties.filePath
            }
        }
    }

    onFile_nameChanged: {
        if (file_name != "") {
            backend.send_file(file_name);
            file_name = "";
        }
    }

    Component.onCompleted: backend.load()
}
