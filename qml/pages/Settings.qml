import QtQuick 2.0
import Sailfish.Silica 1.0
import SettingsBackend 1.0

Page {
    id: page

    allowedOrientations: Orientation.All

    property bool running;

    SettingsBackend {
        id: backend;
        onDone: {
            pageStack.replaceAbove(null, "Rooms.qml");
           }
        onConfigClearDone: {
            pageStack.replaceAbove(null, "Start.qml");
        }
        onNotificationDisabledChanged: {
            if (notificationDisabled) {
                console.log("Disabling notifs")
                sailtrixSignals.emitNotificationsDisabled();
            } else {
                sailtrixSignals.emitNotificationsEnabled();
            }
        }

        onNotifIntervalChanged: {
            sailtrixSignals.emitNotificationIntervalChanged(notifInterval);
        }
    }

    Column {
        id: column
        height: page.height;
        spacing: Theme.paddingLarge
        anchors.leftMargin: Theme.horizontalPageMargin
        anchors.rightMargin: Theme.horizontalPageMargin
        anchors.left: parent.left
        anchors.right: parent.right

        PageHeader {
            title: "Settings"
        }

        SectionHeader {
            text: "Notifications"
        }

        TextSwitch {
            id: notif_enable
            text: "Enable notifications"
            checked: !backend.notificationDisabled
            onCheckedChanged: backend.notificationDisabled = !checked
        }

        ComboBox {
            visible: notif_enable.checked
            label: "Notification checking interval"
            menu: ContextMenu {
                MenuItem { text: "30 seconds" }
                MenuItem { text: "2.5 minutes" }
                MenuItem { text: "5 minutes" }
            }

            currentIndex: backend.notifInterval
            onCurrentIndexChanged: {
                backend.notifInterval = currentIndex
            }
        }

        SectionHeader {
            text: "Display"
        }

        ComboBox {
            id: sort_type
            label: "Sort rooms by"
            menu: ContextMenu {
                MenuItem { text: "Activity" }
                MenuItem { text: "Alphabetical" }
            }

            currentIndex: backend.sortType
            onCurrentIndexChanged: backend.sortType = currentIndex
        }

        SectionHeader {
            text: "Global"
        }

        Button {
            text: "Clear cache";
            onClicked: Remorse.popupAction(root, "Cleared cache", function() { backend.clear_cache() });
        }

        Button {
            text: "Logout"
            onClicked: {
                Remorse.popupAction(root, "Logging out", function() {
                    running = true;
                    backend.clear_config();
                });
            }
        }


        PageBusyIndicator {
            id: indicator
            running: running
        }
    }
}
