import QtQuick 2.0
import Sailfish.Silica 1.0
import PictureDisplayBackend 1.0
import Nemo.Notifications 1.0

Page {
    id: page

    property string image_name;
    property string mxc;
    property bool saving;

    allowedOrientations: Orientation.All

    Notification {
        id: saved
        summary: "Image saved"
        body: "Image saved to Pictures directory"
        expireTimeout: 1500
        previewSummary: "Image saved"
        icon: "image://theme/icon-s-cloud-download"
    }

    Notification {
        id: error
        summary: "Image error"
        body: "Image not saved to Pictures directory"
        expireTimeout: 1500
        previewSummary: "Image error";
        icon: "image://theme/icon-s-blocked"
    }

    PictureDisplayBackend {
        id: backend

        onImage_saved: {
            saving = false;
            saved.publish();
        }
        onImage_error: {
            saving = false;
            error.publish();
        }
    }

    SilicaFlickable {
        anchors.fill: parent

        PullDownMenu {
            MenuItem {
                text: "Save to Pictures";
                onClicked: {
                    saving = true;
                    backend.save(image_name);
                }
            }
        }

        Column {
            width: page.width
            height: page.height

            PageHeader {
                title: image_name
                id: header
            }

            AnimatedImage {
                source: backend.path
                width: parent.width
                height: parent.height - header.height
                fillMode: Image.PreserveAspectFit
            }
        }

        PageBusyIndicator {
            running: !backend.path || saving
        }
    }

    Component.onCompleted: {
        backend.load(mxc);
    }
}
