import QtQuick 2.0
import Sailfish.Silica 1.0
import InviteBackend 1.0

Dialog {
    id: page

    property string room_id
    property string name
    property int num_members
    property string avatar_path

    signal joined(string room_id, string name)

    allowedOrientations: Orientation.All

    DialogHeader {
        id: header
        title: "Join Room"
        acceptText: "Join"
    }


    Component {
        id: waiting_page

        Page {
            PageBusyIndicator {
                id: indicator
                running: true
            }
        }
    }

    acceptDestination: waiting_page

    onAccepted: {
        backend.join(room_id);
    }

    InviteBackend {
        id: backend
        onJoinDone: {
            pageStack.pop()
            pageStack.completeAnimation();
            pageStack.pop();
            pageStack.completeAnimation();

            page.joined(room_id, name);
        }
    }

    Column {
        id: column
        width: page.width
        anchors.verticalCenter: parent.verticalCenter
        spacing: Theme.paddingLarge

        Image {
            source: avatar_path ? avatar_path : "image://theme/icon-m-chat"
            anchors.horizontalCenter: parent.horizontalCenter
            height: Theme.iconSizeLarge
            width: height
        }

        Label {
            text: name
            color: Theme.highlightColor
            font.family: Theme.fontFamily
            font.pixelSize: Theme.fontSizeLarge
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width
            horizontalAlignment: Qt.AlignCenter
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        }

        Label {
            text: num_members + " members"
            color: Theme.highlightColor
            font.family: Theme.fontFamily
            font.pixelSize: Theme.fontSizeSmall
            anchors.horizontalCenter: parent.horizontalCenter
            visible: num_members != undefined && num_members != 0
        }
    }
}
