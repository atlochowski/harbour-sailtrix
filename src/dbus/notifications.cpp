#include <QJsonDocument>
#include <QJsonArray>
#include <QTimer>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QFile>
#include <QStandardPaths>
#include <nemonotifications-qt5/notification.h>
#include <keepalive/backgroundactivity.h>

#include "notifications.h"
#include "sailtrixsignals.h"
#include "../enc-util.h"

Notifications::Notifications(QObject *parent, SailtrixSignals* _sig) : QObject(parent), manager { new QNetworkAccessManager(this) }, activity { new BackgroundActivity(this) }, sig { _sig }
{
    connect(activity, &BackgroundActivity::running, this, &Notifications::every30Seconds);
    connect(manager, &QNetworkAccessManager::finished, this, &Notifications::processNotifications);
    connect(sig, &SailtrixSignals::notificationsDisabled, this, &Notifications::disable);
    connect(sig, &SailtrixSignals::notificationsEnabled, this, &Notifications::enable);
    connect(sig, &SailtrixSignals::notificationIntervalChanged, this, &Notifications::changeFrequency);

    start_time = QDateTime::currentMSecsSinceEpoch();
    activity->setWakeupFrequency(freq);
    activity->wait(freq);
}

Notifications::~Notifications() {
    delete manager;
    delete activity;
}

void Notifications::changeFrequency(int n) {
    if (activity->isWaiting()) {
        activity->stop();
    }

    switch (n) {
    case 0:
        freq = BackgroundActivity::ThirtySeconds;
        break;
    case 1:
        freq = BackgroundActivity::TwoAndHalfMinutes;
        break;
    case 2:
        freq = BackgroundActivity::FiveMinutes;
        break;
    }

    qDebug() << "Changing notification frequency to " << freq;
    activity->setWakeupFrequency(freq);

    if (!m_disabled) {
        activity->wait(freq);
    }
}

void Notifications::disable() {
    qDebug() << "Disabling notifications";
    if (!m_disabled) {
        m_disabled = true;
        activity->stop();
    }

}

void Notifications::enable() {
    if (m_disabled) {
        m_disabled = false;
        start_time = QDateTime::currentMSecsSinceEpoch();
        activity->setWakeupFrequency(freq);
        activity->wait(freq);
    }
}

void Notifications::every30Seconds() {
    if (m_disabled) {
        return;
    }

    QByteArray secret_stored = get_secret(&m_secretManager, config_secret_id());
    if (secret_stored != nullptr) {
        QJsonDocument document = QJsonDocument::fromJson(secret_stored);
        m_access_token = document.object().value("access_token").toString();
        m_hs_url = document.object().value("home_server").toString();
    } else {
        return;
    }

    qDebug() << "Polling for notifications";
    QString url(m_hs_url + "/_matrix/client/r0/notifications?limit=2");

    /* if (!next.isNull() && !next.isEmpty()) {
        url.append("&from=" + next);
        qDebug() << "From:" << next;
    } */

    QNetworkRequest req(url);
    req.setRawHeader(QByteArray("Authorization"), (QString("Bearer " + m_access_token).toUtf8()));

    manager->get(req);
}

void Notifications::processNotifications(QNetworkReply* reply) {
    if (m_disabled) {
        return;
    }

    if (reply->error() == QNetworkReply::NoError) {
        QJsonDocument doc = QJsonDocument::fromJson(reply->readAll());
        qDebug() << "Received notifications object:" << doc;
        QJsonObject notif_obj = doc.object();
        QJsonArray notifications = notif_obj.value("notifications").toArray();

        bool has_notif = false;
        int num = 0;

        QJsonObject top_notif;

        for (QJsonValue notif : notifications) {
            QJsonObject this_notif = notif.toObject();

            if (this_notif.value("actions").toArray().contains("notify")
                || this_notif.value("actions").toArray().contains("coalesce")
                || this_notif.value("actions").toString() == QStringLiteral("notify")
                || this_notif.value("actions").toString() == QStringLiteral("coalesce")) {


                QString room_id = this_notif.value("room_id").toString();
                QJsonObject event = this_notif.value("event").toObject();

                qint64 notif_time = this_notif.value("ts").toVariant().toULongLong();
                if (notif_time > start_time) {
                    qDebug() << "Received notification:" << event.value("content").toObject();
                    has_notif = true;
                    top_notif = this_notif;
                    ++num;
                }
            }
        }

        if (has_notif) {
            Notification *n = new Notification();
            n->setAppIcon("harbour-sailtrix");
            n->setAppName("Sailtrix");
            n->setCategory("x-nemo.messaging.im");
            n->setSummary(num > 1 ? "New messages" : "New message");

            QString body = num > 1 ? "Multiple new messages" : "";

            if (num == 1) {
                QFile cache_file(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/org.yeheng/sailtrix/matrix-rooms.cache.json");
                if (cache_file.open(QFile::ReadOnly)) {
                    QJsonDocument doc = QJsonDocument::fromJson(cache_file.readAll());
                    QString room_name = doc.object().value("rooms_list")
                                            .toObject()
                                            .value(top_notif.value("room_id").toString()).toObject().value("name").toString();

                    body = "New message in " + room_name;

                    QString icon = doc.object().value("rooms_list").toObject().value(top_notif.value("room_id").toString()).toObject().value("avatar").toString();
                    if (icon.startsWith("/")) {
                        n->setIcon(icon);
                    } else {
                        n->setIcon("image://theme/icon-lock-chat");
                    }
                }

                QVariantList args;
                args.append(top_notif.value("room_id").toString());

                QVariantList actions;
                actions.append(Notification::remoteAction(QStringLiteral("default"), QStringLiteral("showRoom"), QStringLiteral("org.yeheng.sailtrix"), QStringLiteral("/org/yeheng/sailtrix"), QStringLiteral("org.yeheng.sailtrix"), QStringLiteral("showRoom"), args));
                n->setRemoteActions(actions);
            } else {
                QVariantList actions;
                actions.append(Notification::remoteAction(QStringLiteral("default"),
                                                          QStringLiteral("open"),
                                                          QStringLiteral("org.yeheng.sailtrix"),
                                                          QStringLiteral("/org/yeheng/sailtrix"),
                                                          QStringLiteral("org.yeheng.sailtrix"),
                                                          QStringLiteral("open")));
                n->setRemoteActions(actions);
                n->setIcon("image://theme/icon-lock-chat");
            }

            n->setBody(body);
            n->setPreviewSummary(num > 1 ? "New messages" : "New message");
            n->setPreviewBody(body);
            n->setTimestamp(QDateTime::fromMSecsSinceEpoch(top_notif.value("ts").toVariant().toLongLong()));
            n->setUrgency(Notification::Normal);

            n->publish();
            notification_list.append(n);
        }

        next = notif_obj.value("next_token").toString();
        start_time = QDateTime::currentMSecsSinceEpoch();

        activity->wait(freq);
    } else {
        qWarning() << "Get notifications failed:" << reply->error() << reply->errorString() << reply->readAll();
    }
}

void Notifications::pause() {
    if (m_disabled) {
        return;
    }

    activity->stop();

    for (Notification* n : notification_list) {
        if (n) {
            n->close();
            delete n;
            notification_list.removeOne(n);
        }
    }
}

void Notifications::resume() {
    if (m_disabled) {
        return;
    }

    start_time = QDateTime::currentMSecsSinceEpoch();
    activity->wait(freq);
}

bool Notifications::isStopped() {
    if (m_disabled) {
        return true;
    }
    return activity->isStopped();
}
