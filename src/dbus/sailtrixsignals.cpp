#include "sailtrixsignals.h"

SailtrixSignals::SailtrixSignals(QObject *parent) : QObject(parent)
{

}

void SailtrixSignals::emitRoomOpenCommand(const QString &room_id) {
    emit roomOpenCommand(room_id);
}

void SailtrixSignals::emitJoinRoomOpenCommand(const QString& room_id, const QString& display_name) {
    emit joinRoomOpenCommand(room_id, display_name);
}

void SailtrixSignals::emitNotificationsDisabled() {
    emit notificationsDisabled();
}

void SailtrixSignals::emitNotificationsEnabled() {
    emit notificationsEnabled();
}

void SailtrixSignals::emitNotificationIntervalChanged(int n) {
    emit notificationIntervalChanged(n);
}

void SailtrixSignals::emitUserOpenCommand(const QString &user_id, const QString &display_name) {
    emit userOpenCommand(user_id, display_name);
}
