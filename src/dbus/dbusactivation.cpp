#include <QGuiApplication>
#include <QDBusConnection>
#include <QDBusConnectionInterface>
#include <QDBusMessage>
#include <QDebug>
#include <QQuickView>
#include <QQmlContext>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QStandardPaths>

#include <sailfishapp.h>

#include "../enc-util.h"
#include "dbusactivation.h"
#include "sailtrixadapter.h"
#include "notifications.h"

static const QString service_name = QStringLiteral("org.yeheng.sailtrix");
static const QString object_name = QStringLiteral("/org/yeheng/sailtrix");


DBusActivation::DBusActivation(QGuiApplication *parent) : app(parent), sig { new SailtrixSignals }
{
    notifications = new Notifications(this, sig);

    QDBusConnection conn = QDBusConnection::sessionBus();
    new SailtrixAdapter(this);
    conn.registerObject(object_name, this);

    qDebug() << "Registering Dbus Activation";


    if (!conn.isConnected()) {
        qWarning() << "Cannot connect to Dbus";
        return;
    }

    if (!conn.interface()->isServiceRegistered(service_name)) {
        bool ready = conn.registerService(service_name);
        if (!ready) {
            qWarning() << "Service already registered";
            app->quit();
        } else {
            qDebug() << "Service registered";
        }

        qDebug() << conn.lastError().message();
    }

    QFile settings(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/org.yeheng/sailtrix/settings.json");
    if (settings.open(QFile::ReadOnly)) {
        QJsonObject obj = QJsonDocument::fromJson(settings.readAll()).object();
        bool disabled = obj.value("notifications_disabled").toBool();
        int freq = obj.value("notification_interval").toInt();

        notifications->changeFrequency(freq);

        if (disabled) {
            notifications->disable();
        } else {
            notifications->enable();
        }

    }

    QByteArray secret_stored = get_secret(&m_secretManager, config_secret_id());
    if (secret_stored != nullptr) {
        QJsonDocument document = QJsonDocument::fromJson(secret_stored);
        m_access_token = document.object().value("access_token").toString();
        m_hs_url = document.object().value("home_server").toString();
        m_user_id = document.object().value("user_id").toString();

    }
}

DBusActivation::~DBusActivation() {
    QDBusConnection conn = QDBusConnection::sessionBus();
    conn.unregisterObject(object_name);
    conn.unregisterService(service_name);

    delete notifications;
    delete sig;
}

bool DBusActivation::sendOpenSignal() {
    qDebug() << "Sending open signal";
    QDBusConnection conn = QDBusConnection::sessionBus();
    QDBusMessage message = QDBusMessage::createMethodCall(service_name, object_name, service_name, "open");
    auto result = conn.call(message);

    if (result.errorName() == "") {
        return true;
    } else {
        qWarning() << "Could not send open signal: " << result.errorName() << result.errorMessage();
        return false;
    }
}

bool DBusActivation::sendOpenSignalWithUrl(const QString &url) {
    qDebug() << "Sending open wtih URL signal";
    QDBusConnection conn = QDBusConnection::sessionBus();
    QDBusMessage message = QDBusMessage::createMethodCall(service_name, object_name, service_name, "openWithUrl");
    QList<QVariant> args;
    args.append(url);

    message.setArguments(args);
    auto result = conn.call(message);

    if (result.errorName() != "") {
        qWarning() << "Could not send open with URL signal: " << result.errorName() << result.errorMessage();
        return false;
    }

    return true;
}

void DBusActivation::open() {
    qDebug() << "Received open signal";
    if (!view) {
        view = SailfishApp::createView();

        QByteArray arr = get_secret(&m_secretManager, config_secret_id());

        if (arr != nullptr) {
            view->setSource(SailfishApp::pathTo(QString("qml/logged-in.qml")));
        } else {
            view->setSource(SailfishApp::pathTo(QString("qml/harbour-sailtrix.qml")));
        }

        view->rootContext()->setContextProperty("sailtrixSignals", sig);

        connect(view, &QQuickView::windowStateChanged, this, &DBusActivation::onWindowStateChanged);

        view->show();

        qDebug() << "Showing";

        connect(view, &QQuickView::destroyed, this, &DBusActivation::onViewDestroyed);
        connect(view, &QQuickView::visibleChanged, this, &DBusActivation::onVisibleChanged);

        connect(view, SIGNAL(closing(QQuickCloseEvent *)), this, SLOT(onViewClosing(QQuickCloseEvent *)));
    } else if (view->windowState() == Qt::WindowNoState) {
        qDebug() << "WindowNoState";
        view->create();
        view->showFullScreen();
    } else {
        qDebug() << view->windowState();
        view->raise();
        view->requestActivate();
    }
}

void DBusActivation::openWithUrl(const QString& url_str) {
    open();
    qDebug() << "Opening" << url_str;
    QByteArray arr = get_secret(&m_secretManager, config_secret_id());
    if (arr != nullptr) {
        QJsonDocument document = QJsonDocument::fromJson(arr);
        m_access_token = document.object().value("access_token").toString();
        m_hs_url = document.object().value("home_server").toString();
        m_user_id = document.object().value("user_id").toString();

        QUrl url(url_str);
        if (url.scheme() == QString("matrix")) {
            QString path = url.path();
            if (path.startsWith("roomid/")) {
                QString room_id = url.fileName();
                qDebug() << "Launching room id:" << room_id;

                QFile cache_file(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/org.yeheng/sailtrix/matrix-rooms.cache.json");
                if (cache_file.open(QFile::ReadOnly)) {
                    if (!QJsonDocument::fromJson(cache_file.readAll()).object().value("rooms_list").toObject().value("!" + room_id).isUndefined()) {
                        showRoom(room_id);
                    } else {
                        qDebug() << "Room not available";
                        sig->emitJoinRoomOpenCommand("!" + room_id, "Room from link");
                    }
                    cache_file.close();
                }
            } else if (path.startsWith("r/")) {
                QString alias = url.fileName();
                qDebug() << "Launching room alias:" << alias;

                QNetworkAccessManager* manager = new QNetworkAccessManager(this);
                connect(manager, &QNetworkAccessManager::finished, this, [=](QNetworkReply* reply) {
                    qDebug() << "Got reply from room alias";
                    if (reply->error() == QNetworkReply::NoError) {
                        QString room_id = QJsonDocument::fromJson(reply->readAll()).object().value("room_id").toString();

                        QFile cache_file(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/org.yeheng/sailtrix/matrix-rooms.cache.json");
                        if (cache_file.open(QFile::ReadOnly)) {
                            if (!QJsonDocument::fromJson(cache_file.readAll()).object().value("rooms_list").toObject().value(room_id).isUndefined()) {
                                qDebug() << "Opening room:" << room_id;
                                showRoom(room_id);
                            } else {
                                qDebug() << "Room not available";
                                qDebug() << "Opening join room prompt:" << room_id;
                                sig->emitJoinRoomOpenCommand(room_id, "#" + alias);
                            }
                            cache_file.close();
                        }
                    } else {
                        qWarning() << "Network error:" << reply->error() << reply->errorString() << reply->readAll();
                     }
                     manager->deleteLater();
                });

                QNetworkRequest req(QUrl(m_hs_url + "/_matrix/client/r0/directory/room/%23" + alias));
                manager->get(req);


            } else if (path.startsWith("u/")) {
                QString user_id = url.fileName();
                qDebug() << "Launching user:" << user_id;

                QNetworkAccessManager* manager = new QNetworkAccessManager(this);

                connect(manager, &QNetworkAccessManager::finished, this, [=](QNetworkReply* reply) {
                    qDebug() << "Received user display name";
                    if (reply->error() == QNetworkReply::NoError) {
                        QString display_name = QJsonDocument::fromJson(reply->readAll()).object().value("displayname").toString();
                        if (display_name.isNull() || display_name.isEmpty()) {
                            sig->emitUserOpenCommand("@" + user_id, "@" + user_id);
                        } else {
                            sig->emitUserOpenCommand("@" + user_id, display_name);
                        }
                    } else {
                        qWarning() << "Network error:" << reply->error() << reply->errorString() << reply->readAll();
                    }

                    manager->deleteLater();
                });
                QNetworkRequest req(m_hs_url + "/_matrix/client/r0/profile/@" + user_id + "/displayname");
                manager->get(req);

            }
        } else if (url.scheme() == QString("https") && url.host() == QString("matrix.to")) {
            QString path = url.fragment();
            qDebug() << "Matrix.to:" << path;
            if (path.startsWith("/!")) {
                QString room_id = url.fragment().remove(0,1);;
                qDebug() << "Launching room id:" << room_id;

                QFile cache_file(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/org.yeheng/sailtrix/matrix-rooms.cache.json");
                if (cache_file.open(QFile::ReadOnly)) {
                    if (!QJsonDocument::fromJson(cache_file.readAll()).object().value("rooms_list").toObject().value(room_id).isUndefined()) {
                        showRoom(room_id);
                    } else {
                        qDebug() << "Room not available";
                        sig->emitJoinRoomOpenCommand(room_id, "Room from link");
                    }
                    cache_file.close();
                }
            } else if (path.startsWith("/#")) {
                QString alias = url.fragment().remove(0,1);
                qDebug() << "Launching room alias:" << alias;

                QNetworkAccessManager* manager = new QNetworkAccessManager(this);
                connect(manager, &QNetworkAccessManager::finished, this, [=](QNetworkReply* reply) {
                    qDebug() << "Got reply from room alias";
                    if (reply->error() == QNetworkReply::NoError) {
                        QString room_id = QJsonDocument::fromJson(reply->readAll()).object().value("room_id").toString();

                        QFile cache_file(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/org.yeheng/sailtrix/matrix-rooms.cache.json");
                        if (cache_file.open(QFile::ReadOnly)) {
                            if (!QJsonDocument::fromJson(cache_file.readAll()).object().value("rooms_list").toObject().value(room_id).isUndefined()) {
                                qDebug() << "Opening room:" << room_id;
                                showRoom(room_id);
                            } else {
                                qDebug() << "Room not available";
                                qDebug() << "Opening join room prompt:" << room_id;
                                sig->emitJoinRoomOpenCommand(room_id, alias);
                            }
                            cache_file.close();
                        }
                    } else {
                        qWarning() << "Network error:" << reply->error() << reply->errorString() << reply->readAll();
                    }
                    manager->deleteLater();
                });

                QNetworkRequest req(QUrl(m_hs_url + "/_matrix/client/r0/directory/room/" + QUrl::toPercentEncoding(alias)));
                manager->get(req);


            } else if (path.startsWith("/@")) {
                QString user_id = url.fragment().remove(0,1);
                qDebug() << "Launching user:" << user_id;

                QNetworkAccessManager* manager = new QNetworkAccessManager(this);

                connect(manager, &QNetworkAccessManager::finished, this, [=](QNetworkReply* reply) {
                    qDebug() << "Received user display name";
                    if (reply->error() == QNetworkReply::NoError) {
                        QString display_name = QJsonDocument::fromJson(reply->readAll()).object().value("displayname").toString();
                        if (display_name.isNull() || display_name.isEmpty()) {
                            sig->emitUserOpenCommand(user_id, user_id);
                        } else {
                            sig->emitUserOpenCommand(user_id, display_name);
                        }
                    } else {
                        qWarning() << "Network error:" << reply->error() << reply->errorString() << reply->readAll();
                    }

                    manager->deleteLater();
                });
                QNetworkRequest req(m_hs_url + "/_matrix/client/r0/profile/" + user_id + "/displayname");
                manager->get(req);

            }
        }
    }
}

void DBusActivation::onViewClosing(QQuickCloseEvent* v) {
    qDebug() << "Dbus closing";
    Q_UNUSED(v);

    if (view) {
        if (notifications->isStopped()) {
            notifications->resume();
        }

        view->destroy();
        view->deleteLater();
        view = nullptr;
    }
}

void DBusActivation::onViewDestroyed() {
    qDebug() << "Dbus view destroyed";
    //view->deleteLater();
    view = nullptr;
}

void DBusActivation::showRoom(const QString& id) {
    qDebug() << "Received show room command";
    open();
    sig->emitRoomOpenCommand(id);
}

void DBusActivation::onWindowStateChanged(Qt::WindowState state) {
    qDebug() << "Window in state:" << state;
    if (state == Qt::WindowMinimized || state == Qt::WindowNoState) {
        if (notifications->isStopped()) {
            notifications->resume();
        }
    } else {
        notifications->pause();
    }
}

void DBusActivation::onVisibleChanged(bool visible) {
    qDebug() << "VISIBLE CHANGED:" << visible;
    if (visible) {
        notifications->pause();
    } else {
        if (notifications->isStopped()) {
            notifications->resume();
        }
    }
}

