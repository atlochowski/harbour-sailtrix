#include "sailtrixadapter.h"

SailtrixAdapter::SailtrixAdapter(QObject* parent) : QDBusAbstractAdaptor(parent)
{
    setAutoRelaySignals(true);
}

void SailtrixAdapter::open() {
    QMetaObject::invokeMethod(parent(), "open");
}

void SailtrixAdapter::showRoom(const QString& id) {
    QMetaObject::invokeMethod(parent(), "showRoom", Q_ARG(QString, id));
}

void SailtrixAdapter::openWithUrl(const QString& url) {
    QMetaObject::invokeMethod(parent(), "openWithUrl", Q_ARG(QString, url));
}
