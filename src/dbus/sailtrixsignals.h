#ifndef SAILTRIXSIGNALS_H
#define SAILTRIXSIGNALS_H

#include <QObject>

class SailtrixSignals : public QObject
{
    Q_OBJECT
public:
    explicit SailtrixSignals(QObject *parent = nullptr);
    void emitRoomOpenCommand(const QString& room_id);
    void emitJoinRoomOpenCommand(const QString& room_id, const QString& display_name);
    void emitUserOpenCommand(const QString& user_id, const QString& display_name);
    Q_INVOKABLE void emitNotificationsDisabled();
    Q_INVOKABLE void emitNotificationsEnabled();
    Q_INVOKABLE void emitNotificationIntervalChanged(int n);

signals:
    void roomOpenCommand(const QString& room_id);
    void joinRoomOpenCommand(const QString& room_id, const QString& display_name);
    void userOpenCommand(const QString& user_id, const QString& display_name);
    void notificationsDisabled();
    void notificationsEnabled();
    void notificationIntervalChanged(int n);
};

#endif // SAILTRIXSIGNALS_H
