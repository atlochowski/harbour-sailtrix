#ifndef DBUSACTIVATION_H
#define DBUSACTIVATION_H

#include <QObject>
#include <QGuiApplication>
#include <QQuickView>
#include <Sailfish/Secrets/secretmanager.h>
#include "notifications.h"
#include "sailtrixsignals.h"

class DBusActivation : public QObject
{
    Q_OBJECT
public:
    explicit DBusActivation(QGuiApplication *parent = nullptr);
    ~DBusActivation();
    static bool sendOpenSignal();
    static bool sendOpenSignalWithUrl(const QString& url);
public slots:
    void open();
    void openWithUrl(const QString& url);
    void showRoom(const QString& room_id);
    void onViewDestroyed();
    void onViewClosing(QQuickCloseEvent* v);
    void onWindowStateChanged(Qt::WindowState state);
    void onVisibleChanged(bool visible);
signals:
private:
    QGuiApplication* app = nullptr;
    QQuickView* view = nullptr;
    Sailfish::Secrets::SecretManager m_secretManager;
    Notifications* notifications = nullptr;
    SailtrixSignals* sig = nullptr;
    QString m_hs_url;
    QString m_access_token;
    QString m_user_id;
};

#endif // DBUSACTIVATION_H
