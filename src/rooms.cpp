#include "rooms.h"

#include <QNetworkReply>
#include <QStandardPaths>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QStandardItem>
#include <QDir>
#include <QMimeType>
#include <QMimeDatabase>
#include <QUrl>
#include <QSortFilterProxyModel>
#include <openssl/rand.h>
#include "enc-util.h"

RoomsModel* Rooms::rooms() { return m_rooms; }

void Rooms::reloadSettings() {
    bool sort_by_alphabet = false;

    QFile settings(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/org.yeheng/sailtrix/settings.json");
    if (settings.open(QFile::ReadOnly)) {
        QJsonDocument doc = QJsonDocument::fromJson(settings.readAll());
        qDebug() << "Reloading:" << doc;
        if (doc.object().value("sort_type").toInt() == 1) {
            sort_by_alphabet = true;
        }

        settings.close();
    }

    if (sort_by_alphabet) {
        m_direct_rooms->setSortRole(RoomsModel::name);
        m_direct_rooms->setSortCaseSensitivity(Qt::CaseInsensitive);

        m_direct_rooms->sort(0);
        m_regular_rooms->setSortRole(RoomsModel::name);
        m_regular_rooms->setSortCaseSensitivity(Qt::CaseInsensitive);

        m_regular_rooms->sort(0);
        qDebug() << "Sorted by name";
    } else {
        m_direct_rooms->setSortRole(RoomsModel::timestamp);
        m_direct_rooms->setSortCaseSensitivity(Qt::CaseInsensitive);

        m_direct_rooms->sort(0, Qt::DescendingOrder);
        m_regular_rooms->setSortRole(RoomsModel::timestamp);
        m_regular_rooms->setSortCaseSensitivity(Qt::CaseInsensitive);

        m_regular_rooms->sort(0, Qt::DescendingOrder);
        qDebug() << "Sorted by timestamp";
    }
}

Rooms::Rooms() : m_rooms{ new RoomsModel }, m_direct_rooms { new QSortFilterProxyModel }, m_regular_rooms { new QSortFilterProxyModel }, m_invites { new QSortFilterProxyModel },  m_master_obj {new QJsonObject }, olm_sessions { new QJsonObject}, megolm_sessions {new QJsonObject} , m_reply { nullptr }, message_index { 0 }, get_all { false } , load_finished { false }, m_invite_total {0}, m_direct_total {0} {
    bool sort_by_alphabet = false;

    QFile settings(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/org.yeheng/sailtrix/settings.json");
    if (settings.open(QFile::ReadOnly)) {
        if (QJsonDocument::fromJson(settings.readAll()).object().value("sort_type").toInt() == 1) {
            sort_by_alphabet = true;
        }

        settings.close();
    }

    m_direct_rooms->setSourceModel(m_rooms);
    m_direct_rooms->setFilterRole(RoomsModel::section_name);
    m_direct_rooms->setFilterKeyColumn(0);
    m_direct_rooms->setFilterFixedString(QStringLiteral("Direct Messages"));

    if (sort_by_alphabet) {
        m_direct_rooms->setSortRole(RoomsModel::name);
        m_direct_rooms->setSortCaseSensitivity(Qt::CaseInsensitive);

        m_direct_rooms->sort(0);
    } else {
        m_direct_rooms->setSortRole(RoomsModel::timestamp);
        m_direct_rooms->setSortCaseSensitivity(Qt::CaseInsensitive);

        m_direct_rooms->sort(0, Qt::DescendingOrder);
    }

    m_regular_rooms->setSourceModel(m_rooms);
    m_regular_rooms->setFilterRole(RoomsModel::section_name);
    m_regular_rooms->setFilterKeyColumn(0);
    m_regular_rooms->setFilterFixedString(QStringLiteral("Rooms"));

    if (sort_by_alphabet) {
        m_regular_rooms->setSortRole(RoomsModel::name);
        m_regular_rooms->setSortCaseSensitivity(Qt::CaseInsensitive);

        m_regular_rooms->sort(0);
    } else {
        m_regular_rooms->setSortRole(RoomsModel::timestamp);
        m_regular_rooms->setSortCaseSensitivity(Qt::CaseInsensitive);

        m_regular_rooms->sort(0, Qt::DescendingOrder);
    }


    m_invites->setSourceModel(m_rooms);
    m_invites->setFilterRole(RoomsModel::section_name);
    m_invites->setFilterKeyColumn(0);
    m_invites->setFilterFixedString(QStringLiteral("Invites"));
    m_invites->setSortRole(RoomsModel::name);
    m_invites->sort(0);
    m_invites->setSortCaseSensitivity(Qt::CaseInsensitive);

    connect(this, &Rooms::roomsChanged, this, &Rooms::onRoomsChanged);
};

void Rooms::onRoomsChanged() {
    emit directRoomsChanged();
    emit regularRoomsChanged();
    emit invitesChanged();
}


Rooms::~Rooms() {
    qDebug() << "Deleting Rooms";
    delete m_rooms;
    delete m_master_obj;
    delete olm_sessions;
    delete megolm_sessions;
    delete manager;
    delete filterMaker;
    delete imageDownloader;
    delete leaver;
    delete key_uploader;
    delete message_index;
    delete account_data_getter;
}

QSortFilterProxyModel* Rooms::directRooms() {
    return m_direct_rooms;
}

QSortFilterProxyModel* Rooms::regularRooms() {
    return m_regular_rooms;
}


QSortFilterProxyModel* Rooms::invites() {
    return m_invites;
}

void Rooms::setRooms(RoomsModel *roomsModel) {
    m_rooms = roomsModel;
    emit roomsChanged();
}

bool Rooms::done() {
    return load_finished;
}

QUrl Rooms::getUrl() {
    if (!filter_id.isNull() && !filter_id.isEmpty() && !next_batch.isNull() && !next_batch.isEmpty() && !get_all) {
        return QUrl(hs_url + "/_matrix/client/r0/sync?filter=" + filter_id + "&since=" + next_batch + "&timeout=30000");
    }

    if (!filter_id.isNull() && !filter_id.isEmpty() && !get_all) {
        return QUrl(hs_url + "/_matrix/client/r0/sync?filter=" + filter_id + "&timeout=30000");
    }

    if (!filter_id.isNull() && !filter_id.isEmpty() && !next_batch.isNull() && !next_batch.isEmpty() && get_all) {
        return QUrl(hs_url + "/_matrix/client/r0/sync?filter=" + filter_id + "&since=" + next_batch);
    }

    qDebug() << "Not Using Filter";
    if (next_batch.isNull() || next_batch.isEmpty()) {
        return QUrl(hs_url + "/_matrix/client/r0/sync?filter={ \"account_data\": { \"types\" : [\"m.direct\",\"m.ignored_user_list\"] }, \"room\": { \"account_data\": { \"types\" : []}, \"state\": {\"lazy_load_members\": true, \"types\": [\"m.room.name\", \"m.room.avatar\", \"m.room.encrypted\",\"m.room.member\"] , \"limit\" : 1}, \"ephemeral\": { \"types\": [], \"limit\" : 1}, \"timeline\":{\"lazy_load_members\": true,\"types\":[\"m.room.message\",\"m.room.encrypted\"],\"limit\":1}}}&timeout=30000");
    }
    return QUrl(hs_url + "/_matrix/client/r0/sync?filter={\"lazy_load_members\":true, \"account_data\": { \"types\" : [ \"m.direct\",\"m.ignored_user_list\"] }, \"room\": { \"account_data\": { \"types\" : []}, \"state\": { \"lazy_load_members\": true,\"types\": [\"m.room.name\", \"m.room.avatar\", \"m.room.encrypted\",\"m.room.member\"] , \"limit\" : 1}, \"ephemeral\": { \"types\": [], \"limit\" : 1}, \"timeline\":{\"lazy_load_members\": true,\"types\":[\"m.room.message\",\"m.room.encrypted\"],\"limit\":1}}}&since=" + next_batch + "&timeout=30000");
}

QString Rooms::get_filter_json() {
    return QString("{\"account_data\": { \"types\" : [\"m.direct\",\"m.ignored_user_list\"] }, \"room\": { \"account_data\": { \"types\" : []}, \"state\": {\"lazy_load_members\": true, \"types\": [\"m.room.name\", \"m.room.avatar\",\"m.room.member\"] , \"limit\" : 1}, \"ephemeral\": { \"types\": [], \"limit\" : 1}, \"timeline\":{\"lazy_load_members\": true, \"types\":[\"m.room.message\",\"m.room.encrypted\"],\"limit\":1}}}");
}

void Rooms::write_cache() {
    m_master_obj->insert("version", "1.3");
    QFile cache_file(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/org.yeheng/sailtrix/matrix-rooms.cache.json");
    if (cache_file.open(QFile::WriteOnly)) {
        QJsonDocument master_doc;
        master_doc.setObject(*m_master_obj);
        cache_file.write(master_doc.toJson());
        cache_file.close();
    }
}

void Rooms::maintain_key_count() {
    int needed = (olm_account_max_number_of_one_time_keys(account) / 2) - num_keys_on_server;
    qDebug() << needed << " keys needed.";

    if (needed <= 0) {
        qDebug() << "Returning";
        return;
    }

    unsigned char* random_for_otk = (unsigned char*) std::malloc(olm_account_generate_one_time_keys_random_length(account, needed));

    if (random_for_otk == nullptr) {
        qFatal("Cannot allocate memory for one time keys");
    }

    RAND_bytes(random_for_otk, olm_account_generate_one_time_keys_random_length(account, needed));

    size_t error = olm_account_generate_one_time_keys(account, needed, random_for_otk, olm_account_generate_one_time_keys_random_length(account, needed));

    if (error != olm_error()) {
        uint8_t* otk_arr = (uint8_t*) malloc(olm_account_one_time_keys_length(account));
        size_t otk_json_len = olm_account_one_time_keys(account, otk_arr, olm_account_one_time_keys_length(account));
        QString otk_str = QString::fromUtf8((char*) otk_arr, otk_json_len);
        qDebug() << otk_str;

        QJsonObject one_time_keys_obj;
        QJsonDocument otk_doc = QJsonDocument::fromJson(otk_str.toUtf8());

        int k = 0;
        auto otk_ids = otk_doc.object().value("curve25519").toObject().keys();
        for (QJsonValue value : otk_doc.object().value("curve25519").toObject()) {
            QString key_id = otk_ids.at(k);
            QString the_key = value.toString();

            QJsonDocument to_sign;
            QJsonObject sign_obj;
            sign_obj.insert("key", the_key);
            to_sign.setObject(sign_obj);

            QString sign_message = to_sign.toJson(QJsonDocument::Compact);

            uint8_t* signed_key = (uint8_t*) std::malloc(olm_account_signature_length(account));
            size_t signed_key_length = olm_account_sign(account, (unsigned char*) sign_message.toUtf8().data(), sign_message.length(), signed_key, olm_account_signature_length(account));

            QJsonObject new_obj;
            new_obj.insert("key", the_key);

            QJsonObject signatures_obj;
            QJsonObject signature_obj;

            signature_obj.insert("ed25519:" + device_id, QString::fromUtf8((char*) signed_key, signed_key_length));
            signatures_obj.insert(user_id, signature_obj);

            new_obj.insert("signatures", signatures_obj);
            one_time_keys_obj.insert("signed_curve25519:" + key_id, new_obj);

            free(signed_key);
            k++;
        }

        QNetworkRequest req(hs_url + "/_matrix/client/r0/keys/upload");
        req.setRawHeader(QByteArray("Authorization"), (QString("Bearer " + access_token).toUtf8()));
        req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

        QJsonObject master_obj;
        master_obj.insert("one_time_keys", one_time_keys_obj);
        QJsonDocument upload_doc;
        upload_doc.setObject(master_obj);

        key_uploader->post(req, upload_doc.toJson());
        qDebug() << "Before saving account";
        save_account(account, user_id, &m_cryptoManager, m_local_key);

    } else {
        qWarning() << olm_account_last_error(account);
    }

    free(random_for_otk);
}

void Rooms::trigger_filter() {
    filterMaker = new QNetworkAccessManager(this);
    connect(filterMaker, &QNetworkAccessManager::finished, this, &Rooms::handleFilter);

    QNetworkRequest makeFilterRequest(hs_url + "/_matrix/client/r0/user/" + user_id + "/filter");
    makeFilterRequest.setRawHeader(QByteArray("Authorization"), (QString("Bearer " + access_token).toUtf8()));
    makeFilterRequest.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    filterMaker->post(makeFilterRequest, get_filter_json().toUtf8());
}

bool Rooms::load() {
    QByteArray secret_stored = get_secret(&m_secretManager, config_secret_id());
    if (secret_stored != nullptr) {
        QJsonDocument document = QJsonDocument::fromJson(secret_stored);

        if (document.object().value("home_server").toString() == ""
                || document.object().value("access_token").toString() == "") return false;

        manager = new QNetworkAccessManager(this);
        imageDownloader = new QNetworkAccessManager(this);
        leaver = new QNetworkAccessManager(this);
        key_uploader = new QNetworkAccessManager(this);
        account_data_getter = new QNetworkAccessManager(this);
        user_info_getter = new QNetworkAccessManager(this);

        connect(manager, &QNetworkAccessManager::finished, this, &Rooms::handleRooms);
        connect(key_uploader, &QNetworkAccessManager::finished, this, &Rooms::uploadKeysDone);
        connect(account_data_getter, &QNetworkAccessManager::finished, this, &Rooms::processAccountData);
        connect(user_info_getter, &QNetworkAccessManager::finished, this, &Rooms::processUserInfo);

        m_local_key = get_key(&m_cryptoManager, file_key());

        hs_url = document.object().value("home_server").toString();
        access_token = document.object().value("access_token").toString();
        user_id = document.object().value("user_id").toString();
        device_id = document.object().value("device_id").toString();

        QFile pickle_file(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/org.yeheng/sailtrix/pickle-jar.aes");
        if (pickle_file.open(QFile::ReadOnly)) {
            QByteArray decrypted = decrypt_bytes(&m_cryptoManager, m_local_key, pickle_file.readAll());
            QJsonDocument pickle_doc = QJsonDocument::fromJson(decrypted);
            QString pickle_str = pickle_doc.object().value("pickle").toString();
            qDebug() << pickle_str;

            account = (OlmAccount*) malloc(olm_account_size());
            account = olm_account(account);
            olm_unpickle_account(account, user_id.toUtf8().data(), user_id.toUtf8().size(), pickle_str.toUtf8().data(), pickle_str.toUtf8().size());
            qDebug() << olm_account_last_error(account);
            pickle_file.close();
        }

        *olm_sessions = read_olm_sessions(&m_cryptoManager, m_local_key);
        *megolm_sessions = read_megolm_sessions(&m_cryptoManager, m_local_key);

        QFile encryption_keys(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/org.yeheng/sailtrix/identity_keys.json");
        if (encryption_keys.open(QFile::ReadOnly)) {
            QJsonDocument ek_doc = QJsonDocument::fromJson(encryption_keys.readAll());
            m_curve25519_id = ek_doc.object().value("curve25519").toString();
            m_ed25519_id = ek_doc.object().value("ed25519").toString();

            encryption_keys.close();
        }

        QFile cache_file(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/org.yeheng/sailtrix/matrix-rooms.cache.json");
        if (cache_file.exists() && cache_file.open(QFile::ReadWrite)) {
            QJsonDocument cache_json = QJsonDocument::fromJson(cache_file.readAll());

            if (cache_json.object().value("version").isUndefined()) {
                qDebug() << "Getting all data...";

                QNetworkRequest reply(getUrl());
                reply.setRawHeader(QByteArray("Authorization"), (QString("Bearer ") + document.object().value("access_token").toString()).toUtf8());

                m_reply = manager->get(reply);

                trigger_filter();

                is_loaded = true;
                goto after;
            }

            loadCache(cache_json);
            next_batch = cache_json.object().value("next_batch").toString();

            if (!next_batch.isNull() && !next_batch.isEmpty()) {
                load_finished = true;
                emit done_chg();
            }

            filter_id = cache_json.object().value("filter_id").toString();

            if (filter_id.isNull() || filter_id.isEmpty()) {
                // Trigger the filter creator
                trigger_filter();
            }

            QNetworkRequest nextRequest(getUrl());
            nextRequest.setRawHeader(QByteArray("Authorization"), (QString("Bearer " + access_token).toUtf8()));

            m_reply = manager->get(nextRequest);
            *m_master_obj = cache_json.object();

            cache_file.close();
        } else {
            qDebug() << "Getting all data...";

            QNetworkRequest reply(getUrl());
            reply.setRawHeader(QByteArray("Authorization"), (QString("Bearer ") + document.object().value("access_token").toString()).toUtf8());

            m_reply = manager->get(reply);

            trigger_filter();

            is_loaded = true;

        }


        after:
        QNetworkRequest req(hs_url + "/_matrix/client/r0/user/" + user_id + "/account_data/m.direct");
        req.setRawHeader(QByteArray("Authorization"), (QString("Bearer ") + document.object().value("access_token").toString()).toUtf8());
        account_data_getter->get(req);

        connect(imageDownloader, &QNetworkAccessManager::finished, this, &Rooms::downloadRoomAvatar);
        is_loaded = true;


        return true;
    }

    is_loaded = true;

    return true;
}

void Rooms::processAccountData(QNetworkReply* reply) {
    if (reply->error() == QNetworkReply::NoError) {
        QString type = reply->url().fileName();
        if (type == QString("m.direct")) {
            QJsonObject obj = QJsonDocument::fromJson(reply->readAll()).object();
            qDebug() << "Got m.direct:" << obj;
            if (!obj.isEmpty()) {
                QFile direct_file(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/org.yeheng/sailtrix/direct.json");
                if (direct_file.open(QFile::WriteOnly)) {
                    direct_file.write(QJsonDocument(obj).toJson());
                    direct_file.close();
                }
            }

            dm_obj = obj;
        }
    } else {
        qWarning() << reply->errorString() << reply->readAll();
    }
}

void Rooms::handleRooms(QNetworkReply* reply) {
    get_all = false;

    if (reply->error() != QNetworkReply::NoError) {
        qWarning() << "sync error: " << reply->errorString();
        return;
    }

    qDebug() << "Processing" << reply->url();

    QJsonDocument replyDoc = QJsonDocument::fromJson(reply->readAll());
    QJsonObject replyObj = replyDoc.object();
    QJsonObject rooms = replyObj.value("rooms").toObject();
    QJsonObject joinedRooms = rooms.value("join").toObject();
    QJsonObject leftRooms = rooms.value("leave").toObject();
    QJsonObject inviteRooms = rooms.value("invite").toObject();

    qDebug() << "ROOMS" << rooms;
    qDebug() << "Left rooms:" << leftRooms;

    next_batch = replyObj.value("next_batch").toString();
    qDebug() << "Next batch:" << next_batch;

    QDir cache_dir(QStandardPaths::writableLocation(QStandardPaths::CacheLocation));

    if (!cache_dir.exists()) {
        cache_dir.mkpath(QStandardPaths::writableLocation(QStandardPaths::CacheLocation));
    }

    // save DMs

    QJsonArray dms = replyObj.value("account_data").toObject().value("events").toArray();
    qDebug() << "Account data:" << dms;

    bool dm_changed = false;
    for (QJsonValue value : dms) {
        QJsonObject val = value.toObject();
        if (val.value("type") == QString("m.direct")) {
            int thing_index = 0;
            for (QJsonValue thing : val.value("content").toObject()) {
                QString key = val.value("content").toObject().keys().at(thing_index);
                dm_obj.insert(key, thing);
                thing_index++;
                dm_changed = true;
                qDebug() << "Modified account data:" << dm_obj;
            }
            break;
        }

    }

    if (dm_changed) {
        QFile direct_file(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/org.yeheng/sailtrix/direct.json");
        if (direct_file.open(QFile::WriteOnly)) {
            direct_file.write(QJsonDocument(dm_obj).toJson());
            direct_file.close();
        }
    }

    QJsonObject rooms_cache = m_master_obj->value("rooms_list").toObject();
    QJsonArray to_Device_events = replyObj.value("to_device")
            .toObject()
            .value("events")
            .toArray();
    qDebug() << "NUMBER OF TO-DEVICE EVENTS: " << to_Device_events.size();

    qDebug() << to_Device_events;
    for (QJsonValue event : to_Device_events) {
        QJsonObject event_obj = event.toObject();
        QJsonObject content = event_obj.value("content").toObject();

        QString type = event_obj.value("type").toString();
        qDebug() << "DECRYPTING " << type;

        if (type == QString("m.room.encrypted")) {
            QString decrypted = decrypt(content.value("ciphertext"),
                                        content.value("algorithm").toString(),
                                        content.value("sender_key").toString(),
                                        content.value("device_id").toString(),
                                        content.value("session_id").toString(),
                                        m_curve25519_id,
                                        olm_sessions,
                                        megolm_sessions,
                                        user_id,
                                        account,
                                        event_obj.value("sender").toString(),
                                        message_index);

            write_olm_sessions(*olm_sessions, &m_cryptoManager, m_local_key);


            QJsonDocument decrypted_json = QJsonDocument::fromJson(decrypted.toUtf8());
            QJsonObject decrypted_obj = decrypted_json.object();

            QString encrypted_recipient = decrypted_obj.value("recipient").toString();

            if (!check_olm(decrypted,
                                     event_obj.value("sender").toString(),
                                     user_id,
                                     m_ed25519_id)) {
                qWarning() << "Olm plaintext properties mismatch!";
                continue;
            }


            if (decrypted_obj.value("type").toString() == QString("m.room_key")) {
                QJsonObject room_key_content = decrypted_obj.value("content").toObject();

                if (!megolm_sessions->contains(room_key_content.value("session_id").toString())) {
                    OlmInboundGroupSession* session = (OlmInboundGroupSession*) malloc(olm_inbound_group_session_size());
                    session = olm_inbound_group_session(session);

                    size_t result = olm_init_inbound_group_session(session, (unsigned char*) room_key_content.value("session_key").toString().toUtf8().data(), room_key_content.value("session_key").toString().toUtf8().length());
                    if (result != olm_error()) {
                        char* pickle_igs = (char*) malloc(olm_pickle_inbound_group_session_length(session));
                        size_t pickle_length = olm_pickle_inbound_group_session(session, user_id.toUtf8().data(), user_id.toUtf8().length(), pickle_igs, olm_pickle_inbound_group_session_length(session));

                        if (pickle_length != olm_error()) {
                            QJsonObject obj = room_key_content;
                            obj.insert("pickle", QString::fromUtf8(pickle_igs, pickle_length));
                            megolm_sessions->insert(room_key_content.value("session_id").toString(), obj);

                            write_megolm_sessions(*megolm_sessions, &m_cryptoManager, m_local_key);

                        } else {
                            qWarning() << "failed to pickle group session:" << olm_inbound_group_session_last_error(session);
                        }
                    } else {
                        qWarning() << "Olm init inbound group session error: " << olm_inbound_group_session_last_error(session);
                    }

                }
            }

        }
    }

    int left_room_index = 0;
    for (QJsonValue obj : leftRooms) {
        QString room_id = leftRooms.keys().at(left_room_index);
        qDebug() << "Left" << room_id;
        rooms_cache.remove(room_id);

        for (int i = 0; i < m_rooms->rowCount(); i++) {
            if (m_rooms->data(m_rooms->index(i, 0), RoomsModel::roomId).toString() == room_id) {
                if (m_rooms->data(m_rooms->index(i, 0), RoomsModel::section_name).toString() == QString("Direct Messages")) {
                    if (m_direct_total > 0)
                        m_direct_total--;
                }
                if (m_rooms->data(m_rooms->index(i, 0), RoomsModel::is_invite).toBool()) {
                    if (m_invite_total > 0)
                        m_invite_total--;
                }
                m_rooms->removeRow(i);
            }
        }

        left_room_index++;
    }

    int invite_index = 0;
    for (QJsonValue obj : inviteRooms) {
        QString room_id = inviteRooms.keys().at(invite_index);
        QJsonArray invite_state = obj.toObject().value("invite_state").toObject().value("events").toArray();

        QString room_name;

        bool found = false;
        bool has_name = false;
        bool has_preferred_name = false;
        bool is_direct = false;

        QString inviter;

        for (QJsonValue event : invite_state) {
            QJsonObject obj = event.toObject();

            if (obj.value("type").toString() == "m.room.name") {
                room_name = obj.value("content")
                        .toObject()
                        .value("name")
                        .toString();

                has_name = true;
                has_preferred_name = true;

                qDebug() << "Found invite name:" << room_name;

            }

            qDebug() << "Found invite state obj:" << obj;

            if (obj.value("type").toString() == "m.room.member") {
                if (obj.value("state_key").toString() == user_id) {
                    if (obj.value("content").toObject().value("is_direct").toBool()) {
                        is_direct = true;
                        qDebug() << obj.value("sender").toString();
                        inviter = obj.value("sender").toString();
                        qDebug() << "Found direct message invite from " << inviter;
                    }
                }
            }
        }

        bool is_in = false;
        int row = 0;;

        for (int i = 0; i < m_rooms->rowCount(); i++) {
            if (m_rooms->data(m_rooms->index(i, 0), RoomsModel::roomId).toString() == room_id) {
                is_in = true;
                row = i;
                break;
            }
        }

        if (is_direct) {
            room_name = inviter;
        }

        if (room_name.isNull() || room_name.isEmpty()) {
            room_name = "Unnamed room";
        }

        if (!is_in) {
            QStandardItem* item = new QStandardItem();
            item->setData(room_name, RoomsModel::name);
            item->setData(is_direct ? "Invite to direct message" : "Invite to room", RoomsModel::firstMessage);

            item->setData("Invites", RoomsModel::section_name);
            item->setData(0, RoomsModel::section_code);

            item->setData("image://theme/icon-s-invitation", RoomsModel::icon);
            item->setData(is_direct, RoomsModel::is_direct);


            item->setData(room_id, RoomsModel::roomId);
            item->setData(1, RoomsModel::unreadMessages);
            item->setData(true, RoomsModel::is_invite);

            m_rooms->insertRow(m_invite_total, item);
            m_invite_total++;
        } else {
            if (has_name) {
                m_rooms->setData(m_rooms->index(row, 0), room_name, RoomsModel::name);
            }
        }

        QJsonObject thisRoom;

        thisRoom.insert("id", m_rooms->data(m_rooms->index(row,0), RoomsModel::roomId).toString());
        thisRoom.insert("name", m_rooms->data(m_rooms->index(row,0), RoomsModel::name).toString());
        thisRoom.insert("latest_message", m_rooms->data(m_rooms->index(row,0), RoomsModel::firstMessage).toString());
        thisRoom.insert("mxc_url", m_rooms->data(m_rooms->index(row,0), RoomsModel::mxcUrl).toString());
        thisRoom.insert("unread_messages", m_rooms->data(m_rooms->index(row,0), RoomsModel::unreadMessages).toString());
        thisRoom.insert("avatar", m_rooms->data(m_rooms->index(row,0), RoomsModel::icon).toString());
        thisRoom.insert("section_name", m_rooms->data(m_rooms->index(row,0), RoomsModel::section_name).toString());
        thisRoom.insert("is_invite", true);
        thisRoom.insert("is_direct", is_direct);


        rooms_cache.insert(m_rooms->data(m_rooms->index(row,0), RoomsModel::roomId).toString(), thisRoom);
        ++invite_index;
    }

    int i = 0;
    for (QJsonValue obj : joinedRooms) {
        QJsonObject individualRoom = obj.toObject();
        QJsonArray events = individualRoom.value("state")
                .toObject()
                .value("events")
                .toArray();

        QString room_name;
        QString cached_mid = m_rooms->data(m_rooms->index(i,0), RoomsModel::mxcUrl).toString();
        QString avatar_mxc_url = cached_mid;
        bool found = false;
        bool has_name = false;
        bool has_preferred_name = false;

        for (QJsonValue event : events) {
            QJsonObject obj = event.toObject();
            if (obj.value("type").toString() == "m.room.name") {
                room_name = obj.value("content")
                        .toObject()
                        .value("name")
                        .toString();

                has_name = true;
                has_preferred_name = true;

            }
            else if (obj.value("type").toString() == "m.room.avatar") {
                avatar_mxc_url = obj.value("content")
                        .toObject()
                        .value("url")
                        .toString();

                found = true;
            }
        }

        QString room_id = joinedRooms.keys().at(i);

        QJsonArray heroes = individualRoom.value("summary").toObject().value("m.room.heroes").toArray();

        if (!heroes.isEmpty() && !has_name) {
            if (heroes.size() > 1) {
                room_name = heroes.at(0).toString() + " and " + (heroes.size()-1) + " more";
            } else {
                room_name = heroes.at(0).toString();
            }
            has_name = true;
        }

        bool is_direct = false;

        if (!has_name) {
            int j = 0;
            for (QJsonValue value : dm_obj) {
                QJsonArray arr = value.toArray();
                QString user_id = dm_obj.keys().at(j);

                if (arr.contains(room_id)) {
                    room_name = user_id;
                    qDebug() << "found DM";

                    is_direct = true;

                    if (!found) {
                        avatar_mxc_url = "image://theme/icon-m-users";
                        found = true;
                    }

                    has_name = true;

                    QNetworkRequest req(hs_url + QStringLiteral("/_matrix/client/r0/profile/") + user_id);
                    req.setRawHeader(QByteArray("Authorization"), (QString("Bearer " + access_token).toUtf8()));

                    user_info_getter->get(req);

                    break;
                }
                j++;
            }

        }

        if (!has_name) {
            room_name = "Unnamed room";
        }

        if (!found) {
            avatar_mxc_url = "image://theme/icon-m-chat";
        }


        QJsonObject latestMessage = individualRoom.value("timeline")
                .toObject()
                .value("events")
                .toArray()
                .at(0)
                .toObject();

        QString lastest_msg_text("");

        if (latestMessage.value("type").toString() == QStringLiteral("m.room.encrypted")) {
            lastest_msg_text = "Encrypted message";
        } else if (latestMessage.value("type").toString() == QStringLiteral("m.room.member")) {
            lastest_msg_text = "Member modification event for " + latestMessage.value("state_key").toString();
        } else {
            lastest_msg_text = latestMessage.value("content").toObject().value("body").toString();
        }

        qint64 timestamp = latestMessage.value("origin_server_ts").toVariant().toLongLong();

        lastest_msg_text.replace("\n", " / ");

        QDir cache(QStandardPaths::writableLocation(QStandardPaths::CacheLocation));
        QString media_id = QUrl(avatar_mxc_url).path();

        media_id.remove(0,1);

        int unreadNotificationsCount = individualRoom.value("unread_notifications")
                        .toObject()
                        .value("notification_count")
                        .toInt();


        bool isIn = false;

        int row_id = i + invite_index;

        for (int j = 0; j < m_rooms->rowCount(); j++) {
            QString orig_name = m_rooms->data(m_rooms->index(j,0), RoomsModel::name).toString();
            QString orig_id = m_rooms->data(m_rooms->index(j,0), RoomsModel::roomId).toString();

            if (orig_id == room_id) {
                isIn = true;
                row_id = j;
                QString orig_latestMsg = m_rooms->data(m_rooms->index(j,0), RoomsModel::firstMessage).toString();
                QString orig_mediaId = m_rooms->data(m_rooms->index(j,0), RoomsModel::mxcUrl).toString();
                bool was_direct_invite = m_rooms->data(m_rooms->index(j, 0), RoomsModel::is_direct).toBool();
                bool was_invite = m_rooms->data(m_rooms->index(j, 0), RoomsModel::is_invite).toBool();

                m_rooms->setData(m_rooms->index(j, 0), room_name.isEmpty() || !has_preferred_name ? orig_name : room_name, RoomsModel::name);
                m_rooms->setData(m_rooms->index(j, 0), lastest_msg_text, RoomsModel::firstMessage);
                m_rooms->setData(m_rooms->index(j, 0), timestamp, RoomsModel::timestamp);
                m_rooms->setData(m_rooms->index(j, 0), media_id.isEmpty() || (!orig_mediaId.isEmpty() && !orig_mediaId.isNull() && avatar_mxc_url == QString("image://theme/icon-m-chat")) ? orig_mediaId : avatar_mxc_url, RoomsModel::mxcUrl);
                m_rooms->setData(m_rooms->index(j, 0), unreadNotificationsCount, RoomsModel::unreadMessages);
                m_rooms->setData(m_rooms->index(j, 0), false, RoomsModel::is_invite);

                if (was_invite) {
                    m_rooms->setData(m_rooms->index(j,0), (is_direct || was_direct_invite ? "Direct Messages": "Rooms"), RoomsModel::section_name);
                    m_rooms->setData(m_rooms->index(j,0), (is_direct || was_direct_invite ? 1:2), RoomsModel::section_code);

                    m_rooms->setData(m_rooms->index(j, 0), "image://theme/icon-m-chat", RoomsModel::icon);
                    m_rooms->setData(m_rooms->index(j, 0), "", RoomsModel::firstMessage);

                    QList<QStandardItem*> row = m_rooms->takeRow(j);
                    if (is_direct || was_direct_invite) {
                        qDebug() << "Inserting DM:" << room_name;
                        m_invite_total--;
                        m_rooms->insertRow(m_invite_total + m_direct_total, row);
                        row_id = m_invite_total + m_direct_total;
                        m_direct_total++;
                    } else {
                        m_rooms->appendRow(row);
                    }
                }
            }
        }

        if (!isIn) {
            QStandardItem* item = new QStandardItem();
            item->setData(room_name, RoomsModel::name);
            item->setData(lastest_msg_text, RoomsModel::firstMessage);
            item->setData(avatar_mxc_url, RoomsModel::mxcUrl);

            item->setData(room_id, RoomsModel::roomId);
            item->setData(unreadNotificationsCount, RoomsModel::unreadMessages);

            item->setData((is_direct ? "Direct Messages": "Rooms"), RoomsModel::section_name);
            item->setData((is_direct ? 1: 2), RoomsModel::section_code);
            item->setData(timestamp, RoomsModel::timestamp);


            if (is_direct) {
                qDebug() << "Inserting DM:" << room_name;
                m_rooms->insertRow(m_invite_total + m_direct_total, item);
                row_id = m_invite_total + m_direct_total;
                m_direct_total++;
            } else {
                m_rooms->appendRow(item);
            }
        }

        // Download avatar - don't want to slow down roomsChanged event
        if (avatar_mxc_url != "" && avatar_mxc_url != "image://theme/icon-m-chat" && avatar_mxc_url != "image://theme/icon-m-users") {
            QString server_name = QUrl(avatar_mxc_url).host();
            // see if cached...

            if (cache_dir.exists() && cache_dir.exists(media_id + ".png")) {
                m_rooms->setData(m_rooms->index(row_id, 0), cache.absolutePath() + "/" + media_id + ".png", RoomsModel::icon);
            } else {
                imageDownloader->get(QNetworkRequest(hs_url + QString("/_matrix/media/r0/download/" + server_name + "/" + media_id)));
            }
        }

        if (!m_master_obj->keys().contains("rooms_list")) {
            m_master_obj->insert("rooms_list", QJsonObject());
        }

        QJsonObject thisRoom;

        thisRoom.insert("id", m_rooms->data(m_rooms->index(row_id,0), RoomsModel::roomId).toString());
        thisRoom.insert("name", m_rooms->data(m_rooms->index(row_id,0), RoomsModel::name).toString());
        thisRoom.insert("latest_message", m_rooms->data(m_rooms->index(row_id,0), RoomsModel::firstMessage).toString());
        thisRoom.insert("mxc_url", m_rooms->data(m_rooms->index(row_id,0), RoomsModel::mxcUrl).toString());
        thisRoom.insert("unread_messages", m_rooms->data(m_rooms->index(row_id,0), RoomsModel::unreadMessages).toString());
        thisRoom.insert("avatar", m_rooms->data(m_rooms->index(row_id,0), RoomsModel::icon).toString());

        thisRoom.insert("section_name", m_rooms->data(m_rooms->index(row_id,0), RoomsModel::section_name).toString());
        thisRoom.insert("timestamp", timestamp);

        rooms_cache.insert(m_rooms->data(m_rooms->index(row_id,0), RoomsModel::roomId).toString(), thisRoom);
        i++;

    }
    emit roomsChanged();


    m_master_obj->insert("rooms_list", QJsonValue(rooms_cache));

    m_master_obj->insert("next_batch", next_batch);
    m_master_obj->insert("filter_id", filter_id);

    load_finished = true;
    emit done_chg();

    qDebug() << *m_master_obj;

    write_cache();

    QJsonObject otk_count = replyObj.value("device_one_time_keys_count").toObject();
    num_keys_on_server = otk_count.value("signed_curve25519").toInt();

    maintain_key_count();

    if (!is_paused) {
        qDebug() << "Starting next batch";
        QNetworkRequest nextRequest(getUrl());
        nextRequest.setRawHeader(QByteArray("Authorization"), (QString("Bearer " + access_token).toUtf8()));

        m_reply = manager->get(nextRequest);
    }
    qDebug() << "Here";
}

void Rooms::downloadRoomAvatar(QNetworkReply* reply) {
    if (reply->error() != QNetworkReply::NoError) {
        qWarning() << reply->errorString() << reply->readAll();
        return;
    }
    qDebug() << "Downloading Avatar";
    QDir cache(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/org.yeheng/sailtrix");
    if (!cache.exists()) cache.mkpath(cache.absolutePath());

    QMimeDatabase db;
    QMimeType type = db.mimeTypeForName(reply->rawHeader("Content-Type"));

    QFile avatar(cache.absolutePath() + "/" + reply->url().fileName() + "." + type.preferredSuffix());

    if (avatar.open(QFile::WriteOnly)) {

        avatar.write(reply->readAll());
        avatar.close();


        for (int i = 0; i < m_rooms->rowCount(); i++) {
            QString media_id = QUrl(m_rooms->data(m_rooms->index(i, 0), RoomsModel::mxcUrl).toString()).path();
            media_id.remove(0,1);

            if (media_id == "") {
                continue;
            }


            if (media_id == reply->url().fileName()) {
                QString path = cache.absolutePath() + "/" + reply->url().fileName() + "." + type.preferredSuffix();

                m_rooms->setData(m_rooms->index(i, 0), path, RoomsModel::icon);


                QJsonObject rooms_cache = m_master_obj->value("rooms_list").toObject();
                QJsonObject this_room = rooms_cache.value(m_rooms->data(m_rooms->index(i, 0), RoomsModel::roomId).toString()).toObject();
                this_room.insert("avatar", path);
                rooms_cache.insert(m_rooms->data(m_rooms->index(i, 0), RoomsModel::roomId).toString(), this_room);
                m_master_obj->insert("rooms_list", rooms_cache);

                qDebug() << "Setting icon of " << m_rooms->data(m_rooms->index(i, 0), RoomsModel::name).toString() << "to " <<path << "orig mediaId:" << media_id;

                emit roomsChanged();

                break;
            }
        }
    }
    qDebug() << "Wrote avatar";

    emit avatarChanged();
}

void Rooms::loadCache(QJsonDocument doc) {
    QJsonObject mainObj = doc.object();
    qDebug() << "Loading Cache:" << mainObj;
    for (QJsonValue room : mainObj.value("rooms_list").toObject()) {
        QJsonObject thisRoom = room.toObject();

        QStandardItem* item = new QStandardItem();
        item->setData(thisRoom.value("name").toString(), RoomsModel::name);
        item->setData(thisRoom.value("latest_message").toString(), RoomsModel::firstMessage);
        item->setData(thisRoom.value("mxc_url").toString(), RoomsModel::mxcUrl);
        item->setData(thisRoom.value("section_name").toString(), RoomsModel::section_name);
        item->setData(thisRoom.value("section_name").toString() == QString("Invites") ? 0 : (thisRoom.value("section_name").toString() == QString("Direct Messages") ? 1 : 2), RoomsModel::section_code);


        item->setData(thisRoom.value("is_invite").toBool(), RoomsModel::is_invite);
        item->setData(thisRoom.value("is_direct").toBool(), RoomsModel::is_direct);
        item->setData(thisRoom.value("timestamp").toVariant().toLongLong(), RoomsModel::timestamp);


        QDir cache_dir(QStandardPaths::writableLocation(QStandardPaths::CacheLocation));

        item->setData(thisRoom.value("avatar").toString(), RoomsModel::icon);


        item->setData(thisRoom.value("id").toString(), RoomsModel::roomId);
        item->setData(thisRoom.value("unread_messages").toString(), RoomsModel::unreadMessages);

        if (thisRoom.value("section_name").toString() == QString("Direct Messages")) {
            m_rooms->insertRow(m_invite_total + m_direct_total, item);
            m_direct_total++;
        } else if (thisRoom.value("is_invite").toBool()) {
            m_rooms->insertRow(m_invite_total, item);
            m_invite_total++;
        } else {
            m_rooms->appendRow(item);
        }
    }

    *m_master_obj = QJsonObject(mainObj);
    emit roomsChanged();
}

void Rooms::handleFilter(QNetworkReply* reply) {
    qDebug() << "Processing new filter" << reply->errorString();
    if (reply->error() == QNetworkReply::NoError) {
        qDebug() << "Filter Created";
        QJsonDocument reply_doc = QJsonDocument::fromJson(reply->readAll());
        filter_id = reply_doc.object().value("filter_id").toString();
    } else {
        qDebug() << reply->readAll();
    }
}

void Rooms::leave(QString room_id) {
    QNetworkRequest req(hs_url + "/_matrix/client/r0/rooms/" + room_id + "/leave");
    req.setRawHeader(QByteArray("Authorization"), (QString("Bearer " + access_token).toUtf8()));
    leaver->post(req, "");

    int row_id = -1;

    for (int i = 0; i < m_rooms->rowCount(); i++) {
        if (m_rooms->data(m_rooms->index(i, 0), RoomsModel::roomId).toString() == room_id) {
            row_id = i;
        }
    }

    if (row_id != -1) {
        m_rooms->removeRow(row_id);
        qDebug() << row_id << "Removed.";
    }
}

void Rooms::uploadKeysDone(QNetworkReply* reply) {
    if (reply->error() == QNetworkReply::NoError) {
        olm_account_mark_keys_as_published(account);
        save_account(account, user_id, &m_cryptoManager, m_local_key);

        QJsonDocument doc = QJsonDocument::fromJson(reply->readAll());
        num_keys_on_server = doc.object().value("one_time_key_counts").toObject().value("signed_curve25519").toInt();
    } else {
        qDebug() << reply->readAll();
    }
}

void Rooms::pause() {
    is_paused = true;
    load_finished = false;
    m_reply->abort();
}

void Rooms::resume() {
    is_paused = false;

    *olm_sessions = read_olm_sessions(&m_cryptoManager, m_local_key);
    *megolm_sessions = read_megolm_sessions(&m_cryptoManager, m_local_key);

    get_all = true;

    qDebug() << "Continuing";
    QNetworkRequest nextRequest(getUrl());
    nextRequest.setRawHeader(QByteArray("Authorization"), (QString("Bearer " + access_token).toUtf8()));

    m_reply = manager->get(nextRequest);
}

bool Rooms::loaded() {
    return is_loaded;
}

void Rooms::mark_read(QString room_id) {
    if (room_id != "") {
        for (int i = 0; i < m_rooms->rowCount(); i++) {
            if (m_rooms->data(m_rooms->index(i, 0), RoomsModel::roomId).toString() == room_id) {
                m_rooms->setData(m_rooms->index(i, 0), 0, RoomsModel::unreadMessages);
            }
        }

        QJsonObject rooms_cache = m_master_obj->value("rooms_list").toObject();
        QJsonObject this_room = rooms_cache.value(room_id).toObject();
        this_room.insert("unread_messages", 0);
        rooms_cache.insert(room_id, this_room);
        m_master_obj->insert("rooms_list", rooms_cache);

        write_cache();
    }
}

QString Rooms::get_name(QString room_id) {
    return m_master_obj->value("rooms_list").toObject().value(room_id).toObject().value("name").toString();
}

void Rooms::processUserInfo(QNetworkReply* reply) {
    if (reply->error() == QNetworkReply::NoError) {
        QJsonObject response_obj = QJsonDocument::fromJson(reply->readAll()).object();
        QString avatar_url = response_obj.value("avatar_url").toString();
        QString displayname = response_obj.value("displayname").toString();
        QString user_id = reply->url().fileName();

        for (int i = 0; i < m_rooms->rowCount(); i++) {
            if (m_rooms->data(m_rooms->index(i,0), RoomsModel::name).toString() == user_id) {
                if (!displayname.isNull() && !displayname.isEmpty()) {
                    QString room_id = m_rooms->data(m_rooms->index(i, 0), RoomsModel::roomId).toString();

                    m_rooms->setData(m_rooms->index(i,0), displayname, RoomsModel::name);

                    if (!avatar_url.isNull() && !avatar_url.isEmpty()) {
                        m_rooms->setData(m_rooms->index(i,0), avatar_url, RoomsModel::mxcUrl);
                        imageDownloader->get(QNetworkRequest(hs_url + QString("/_matrix/media/r0/download/" + QUrl(avatar_url).host() + "/" + QUrl(avatar_url).fileName())));
                    }

                    QJsonObject rooms_cache = m_master_obj->value("rooms_list").toObject();
                    QJsonObject this_room = rooms_cache.value(room_id).toObject();
                    this_room.insert("name", displayname);
                    if (!avatar_url.isNull() && !avatar_url.isEmpty()) {
                        this_room.insert("mxc_url", avatar_url);
                    }
                    rooms_cache.insert(room_id, this_room);
                    m_master_obj->insert("rooms_list", rooms_cache);

                    write_cache();
                }
            }
        }
    }
}

