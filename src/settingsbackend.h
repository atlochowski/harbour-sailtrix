#ifndef SETTINGSBACKEND_H
#define SETTINGSBACKEND_H
#include <QObject>
#include <QNetworkReply>
#include <Sailfish/Secrets/secretmanager.h>


class SettingsBackend : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool notificationDisabled READ notificationDisabled WRITE setNotificationDisabled NOTIFY notificationDisabledChanged)
    Q_PROPERTY(int notifInterval READ notifInterval WRITE setNotifInterval NOTIFY notifIntervalChanged)
    Q_PROPERTY(int sortType READ sortType WRITE setSortType NOTIFY sortTypeChanged)

public:
    SettingsBackend();
    Q_INVOKABLE void clear_cache();
    Q_INVOKABLE void clear_config();
    bool notificationDisabled();
    void setNotificationDisabled(bool n);
    int notifInterval();
    void setNotifInterval(int n);
    int sortType();
    void setSortType(int n);
signals:
    void done();
    void configClearDone();
    void notificationDisabledChanged();
    void notifIntervalChanged();
    void sortTypeChanged();
private:
    Sailfish::Secrets::SecretManager m_secretManager;
    bool m_notification_disabled;
    int m_notif_interval = 0;
    int m_sort_type = 0;
private slots:
    void after_logout(QNetworkReply* reply);

};

#endif // SETTINGSBACKEND_H
