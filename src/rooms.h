#ifndef ROOMS_H
#define ROOMS_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QJsonDocument>
#include <QFile>
#include <QUrl>
#include <QJsonObject>
#include <olm/olm.h>
#include <QSortFilterProxyModel>
#include <Sailfish/Secrets/secretmanager.h>
#include <Sailfish/Crypto/cryptomanager.h>
#include <Sailfish/Crypto/key.h>
#include "roomsmodel.h"

using namespace Sailfish;

class Rooms : public QObject
{
    Q_OBJECT
    Q_PROPERTY(RoomsModel* rooms READ rooms WRITE setRooms NOTIFY roomsChanged )
    Q_PROPERTY(QSortFilterProxyModel* directRooms READ directRooms NOTIFY directRoomsChanged)
    Q_PROPERTY(QSortFilterProxyModel* regularRooms READ regularRooms NOTIFY regularRoomsChanged)
    Q_PROPERTY(QSortFilterProxyModel* invites READ invites NOTIFY invitesChanged)

    Q_PROPERTY(bool done READ done NOTIFY done_chg )

public:
    Rooms();
    ~Rooms();
    Q_INVOKABLE bool load();
    Q_INVOKABLE void leave(QString room_id);
    RoomsModel* rooms();
    QSortFilterProxyModel* directRooms();
    QSortFilterProxyModel* regularRooms();
    QSortFilterProxyModel* invites();

    void setRooms(RoomsModel* roomsModel);
    Q_INVOKABLE void pause();
    Q_INVOKABLE void resume();
    Q_INVOKABLE bool loaded();
    Q_INVOKABLE void mark_read(QString room_id);
    Q_INVOKABLE QString get_name(QString room_id);
    Q_INVOKABLE void reloadSettings();
    bool done();

signals:
    void roomsChanged();
    void avatarChanged();
    void done_chg();
    void directRoomsChanged();
    void regularRoomsChanged();
    void invitesChanged();
private:
    QNetworkAccessManager* manager = nullptr;
    QNetworkAccessManager* imageDownloader = nullptr;
    QNetworkAccessManager* filterMaker = nullptr;
    QNetworkAccessManager* leaver = nullptr;
    QNetworkAccessManager* key_uploader = nullptr;
    QNetworkAccessManager* account_data_getter = nullptr;
    QNetworkAccessManager* user_info_getter = nullptr;
    RoomsModel* m_rooms = nullptr;
    QSortFilterProxyModel* m_direct_rooms = nullptr;
    QSortFilterProxyModel* m_regular_rooms = nullptr;
    QSortFilterProxyModel* m_invites = nullptr;

    QJsonObject* m_master_obj = nullptr;
    QJsonObject* olm_sessions = nullptr;
    QJsonObject* megolm_sessions = nullptr;
    QNetworkReply *m_reply = nullptr;
    QString next_batch;
    QString access_token;
    QString user_id;
    QString hs_url;
    QString filter_id;
    QString device_id;
    QString m_curve25519_id;
    QString m_ed25519_id;

    OlmAccount* account;
    int num_keys_on_server;
    void loadCache(QJsonDocument doc);
    QString get_filter_json();
    void trigger_filter();
    void write_cache();
    void maintain_key_count();
    bool is_paused;
    bool is_loaded;
    bool load_finished;
    unsigned int* message_index = nullptr;
    bool get_all;
    Sailfish::Secrets::SecretManager m_secretManager;
    Crypto::CryptoManager m_cryptoManager;
    Crypto::Key m_local_key;
    QJsonObject dm_obj;
    int m_invite_total;
    int m_direct_total;
private slots:
    void handleRooms(QNetworkReply* reply);
    void downloadRoomAvatar(QNetworkReply* reply);
    void handleFilter(QNetworkReply* reply);
    void uploadKeysDone(QNetworkReply* reply);
    void processAccountData(QNetworkReply* reply);
    void processUserInfo(QNetworkReply* reply);
    void onRoomsChanged();
    QUrl getUrl();
};

#endif // ROOMS_H
