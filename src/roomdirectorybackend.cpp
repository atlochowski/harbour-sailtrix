#include "roomdirectorybackend.h"
#include "enc-util.h"
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QJsonArray>
#include <QMimeDatabase>
#include <QStandardPaths>
#include <QFile>
#include <QDir>

RoomDirectoryBackend::RoomDirectoryBackend(QObject *parent) : QObject(parent), searcher { new QNetworkAccessManager(this) }, avatar_downloader { new QNetworkAccessManager(this) }, m_searching { false }, m_model { new RoomDirectoryModel() }
{
    QByteArray secret_stored = get_secret(&m_secretManager, config_secret_id());
    if (secret_stored != nullptr) {
        QJsonDocument document = QJsonDocument::fromJson(secret_stored);
        m_access_token = document.object().value("access_token").toString();
        m_hs_url = document.object().value("home_server").toString();
        connect(searcher, &QNetworkAccessManager::finished, this, &RoomDirectoryBackend::processResults);
        connect(avatar_downloader, &QNetworkAccessManager::finished, this, &RoomDirectoryBackend::processAvatar);
    }
}

RoomDirectoryBackend::~RoomDirectoryBackend() {
    delete searcher;
    delete m_model;
    delete avatar_downloader;
}

void RoomDirectoryBackend::search(QString term, bool matrix_org) {
    m_model->removeRows(0, m_model->rowCount());
    emit resultsChanged();

    QJsonObject search;
    QJsonObject filter;
    filter.insert("generic_search_term", term);
    search.insert("filter", filter);
    search.insert("limit", 50);

    QNetworkRequest req(m_hs_url + "/_matrix/client/r0/publicRooms" + (matrix_org ? "?server=matrix.org" : ""));
    req.setRawHeader(QByteArray("Authorization"), (QString("Bearer " + m_access_token).toUtf8()));
    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    searcher->post(req, QJsonDocument(search).toJson());

    m_searching = true;
    emit searchingChanged();
}

void RoomDirectoryBackend::processResults(QNetworkReply* reply) {
    if (reply->error() == QNetworkReply::NoError) {

        QJsonObject obj = QJsonDocument::fromJson(reply->readAll()).object();
        QJsonArray chunks = obj.value("chunk").toArray();

        for (QJsonValue chunk_val : chunks) {
            QJsonObject chunk = chunk_val.toObject();
            QStandardItem* item = new QStandardItem();
            item->setData(chunk.value("room_id").toString(), RoomDirectoryModel::room_id);
            item->setData(chunk.value("name").toString(), RoomDirectoryModel::name);
            item->setData(chunk.value("canonical_alias").toString(), RoomDirectoryModel::alias);
            item->setData(chunk.value("topic").toString().replace("\n", " "), RoomDirectoryModel::description);
            item->setData(chunk.value("num_joined_members").toInt(), RoomDirectoryModel::joined_count);
            item->setData(chunk.value("avatar_url").toString(), RoomDirectoryModel::avatar_url);

            QUrl mxc(chunk.value("avatar_url").toString());

            QNetworkRequest get_avatar(m_hs_url + "/_matrix/media/r0/download/" + mxc.host() + "/" + mxc.fileName());
            get_avatar.setRawHeader(QByteArray("Authorization"), (QString("Bearer " + m_access_token).toUtf8()));

            avatar_downloader->get(get_avatar);

            m_model->appendRow(item);
        }

        emit resultsChanged();
    } else {
        qWarning() << "Search error:" << reply->errorString() << reply->readAll();
    }
    m_searching = false;
    emit searchingChanged();
}

bool RoomDirectoryBackend::searching() {
    return m_searching;
}

RoomDirectoryModel* RoomDirectoryBackend::results() {
    return m_model;
}

void RoomDirectoryBackend::processAvatar(QNetworkReply* reply) {
    if (reply->error() == QNetworkReply::NoError) {
        QDir cache(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/org.yeheng/sailtrix");
        if (!cache.exists()) cache.mkpath(cache.absolutePath());

        QMimeDatabase db;
        QMimeType type = db.mimeTypeForName(reply->rawHeader("Content-Type"));

        QString file_path = cache.absolutePath() + "/" + reply->url().fileName() + "." + type.preferredSuffix();
        QFile avatar(file_path);

        if (avatar.open(QFile::WriteOnly)) {
            avatar.write(reply->readAll());
            avatar.close();

            QString hostname = reply->url().path().split("/").at(5);
            QString media_id = reply->url().fileName();

            for (int i = 0; i < m_model->rowCount(); i++) {
                QUrl mxc(m_model->data(m_model->index(i, 0), RoomDirectoryModel::avatar_url).toString());
                if (mxc.host() == hostname && mxc.fileName() == media_id) {
                    m_model->setData(m_model->index(i,0), file_path, RoomDirectoryModel::avatar_path);
                    emit resultsChanged();
                }
            }
        }
    } else {
        qWarning() << "Get image error:" << reply->errorString() << reply->readAll();
    }
}
