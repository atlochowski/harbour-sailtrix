#include "settingsbackend.h"
#include <QDir>
#include <QStandardPaths>
#include <QNetworkAccessManager>
#include <QJsonDocument>
#include <QJsonObject>
#include "enc-util.h"

bool SettingsBackend::notificationDisabled() {
    return m_notification_disabled;
}

int SettingsBackend::notifInterval() {
    return m_notif_interval;
}

int SettingsBackend::sortType() {
    return m_sort_type;
}

void SettingsBackend::setNotificationDisabled(bool n) {
    m_notification_disabled = n;
    emit notificationDisabledChanged();

    QFile settings(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/org.yeheng/sailtrix/settings.json");
    if (settings.open(QFile::WriteOnly)) {
        QJsonDocument doc;
        QJsonObject obj;
        obj.insert("notification_interval", notifInterval());
        obj.insert("notifications_disabled", n);
        obj.insert("sort_type", sortType());
        doc.setObject(obj);
        settings.write(doc.toJson());
    }
}

void SettingsBackend::setNotifInterval(int n) {
    m_notif_interval = n;
    emit notifIntervalChanged();

    QFile settings(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/org.yeheng/sailtrix/settings.json");
    if (settings.open(QFile::WriteOnly)) {
        QJsonDocument doc;
        QJsonObject obj;
        obj.insert("notification_interval", n);
        obj.insert("notifications_disabled", notificationDisabled());
        obj.insert("sort_type", sortType());
        doc.setObject(obj);
        settings.write(doc.toJson());
    }
}

void SettingsBackend::setSortType(int n) {
    m_sort_type = n;
    emit sortTypeChanged();

    QFile settings(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/org.yeheng/sailtrix/settings.json");
    if (settings.open(QFile::WriteOnly)) {
        QJsonDocument doc;
        QJsonObject obj;
        obj.insert("notification_interval", notifInterval());
        obj.insert("notifications_disabled", notificationDisabled());
        obj.insert("sort_type", n);
        doc.setObject(obj);
        settings.write(doc.toJson());
    }
}

SettingsBackend::SettingsBackend() : m_notification_disabled { false } {
    QFile settings(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/org.yeheng/sailtrix/settings.json");
    if (settings.open(QFile::ReadOnly)) {
        QJsonObject obj = QJsonDocument::fromJson(settings.readAll()).object();
        m_notification_disabled = obj.value("notifications_disabled").toBool();
        m_notif_interval = obj.value("notification_interval").toInt();
        m_sort_type = obj.value("sort_type").toInt();
        emit notificationDisabledChanged();
        emit notifIntervalChanged();
        emit sortTypeChanged();
    }
}

void SettingsBackend::clear_cache() {
    QDir cache(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/org.yeheng/sailtrix");
    cache.removeRecursively();
    emit done();
}

void SettingsBackend::clear_config() {
    QNetworkAccessManager *manager = new QNetworkAccessManager();
    connect(manager, &QNetworkAccessManager::finished, this, &SettingsBackend::after_logout);

    QByteArray secret_stored = get_secret(&m_secretManager, config_secret_id());
    if (secret_stored != nullptr) {
        QJsonDocument document = QJsonDocument::fromJson(secret_stored);
        QString access_token = document.object().value("access_token").toString();
        QString hs_url = document.object().value("home_server").toString();

        QNetworkRequest req(hs_url + "/_matrix/client/r0/logout");
        req.setRawHeader(QByteArray("Authorization"), (QString("Bearer " + access_token).toUtf8()));

        manager->post(req, "");
    }

    delete_collection(&m_secretManager, QLatin1String("SailtrixWallet2"));

    QDir cache(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/org.yeheng/sailtrix");
    cache.removeRecursively();


    QDir config(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/org.yeheng/sailtrix");
    cache.removeRecursively();
}

void SettingsBackend::after_logout(QNetworkReply* reply) {
    qDebug() << "Here";
    emit configClearDone();
}
