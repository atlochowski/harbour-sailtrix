#ifndef ENCUTIL_H
#define ENCUTIL_H
#include <QString>
#include <QJsonValue>
#include <QJsonObject>
#include <QJsonDocument>
#include <QByteArray>
#include <olm/olm.h>
#include <Sailfish/Secrets/secretmanager.h>
#include <Sailfish/Secrets/secret.h>
#include <Sailfish/Crypto/key.h>

using namespace Sailfish;

QString decrypt(QJsonValue encrypted, QString algorithm, QString sender_key, QString device_id, QString session_id, QString m_curve25519_id, QJsonObject* olm_sessions, QJsonObject* megolm_sessions, QString m_user_id, OlmAccount* account, QString sender, unsigned int* message_index);
void write_olm_sessions(QJsonObject sessions, Crypto::CryptoManager* manager, Crypto::Key key);
void write_megolm_sessions(QJsonObject sessions, Crypto::CryptoManager* manager, Crypto::Key key);
QJsonObject read_olm_sessions(Crypto::CryptoManager* manager, Crypto::Key key);
QJsonObject read_megolm_sessions(Crypto::CryptoManager* manager, Crypto::Key key);
bool verify_signed_json(QJsonObject json, QString expected_sender, QString expected_device_id, QString verification_key);
QString canonical_json(QJsonDocument doc);
void save_account(OlmAccount* account, QString user_id, Crypto::CryptoManager* manager, Crypto::Key key);
QString get_olm_plaintext(QJsonObject obj, QString sender, QString recipient, QString recipient_ed25519, QString sender_ed25519);
QString bad_encrypt_str(QString sender, QString reason);
bool check_olm(QString decrypted, QString sender_user_id, QString m_user_id, QString my_real_key);
// bool check_olm_no_verify(QString decrypted, QString sender_user_id, QString m_user_id);
QString get_uuid();
bool init_secrets(Sailfish::Secrets::SecretManager *m_secretManager, Sailfish::Secrets::Secret::Identifier id);
bool store_secret(Secrets::SecretManager* manager, QByteArray& to_store, Secrets::Secret::Identifier id);
Secrets::Secret::Identifier config_secret_id();
QByteArray get_secret(Secrets::SecretManager* manager, Secrets::Secret::Identifier id);
bool delete_secret(Secrets::SecretManager* manager, Secrets::Secret::Identifier id);
bool delete_collection(Secrets::SecretManager* manager, QLatin1String collection_name);
Crypto::Key::Identifier file_key();
bool create_key(Crypto::CryptoManager* manager, QString auth_token, Secrets::Secret::Identifier id, Crypto::Key::Identifier key_id);
Crypto::Key get_key(Crypto::CryptoManager* manager, Crypto::Key::Identifier id);
QByteArray encrypt_bytes(Crypto::CryptoManager* manager, Crypto::Key key, QByteArray data);
QByteArray decrypt_bytes(Crypto::CryptoManager* manager, Crypto::Key key, QByteArray data);
QByteArray file_dec(QByteArray ciphertext, QByteArray key, QByteArray iv);
QByteArray file_dec2(Crypto::CryptoManager* manager, QByteArray ciphertext, QByteArray key, QByteArray iv);
QByteArray encrypt_bytes(Crypto::CryptoManager* manager, QByteArray data, QString& key_str, QString& iv_str);

#endif // ENCUTIL_H
