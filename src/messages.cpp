#include "messages.h"
#include <QFile>
#include <QStandardPaths>
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkReply>
#include <QUrl>
#include <QJsonArray>
#include <QMimeDatabase>
#include <QMimeData>
#include <QDir>
#include <QQueue>
#include <QEventLoop>
#include <QTimer>
#include <QDateTime>
#include <QDesktopServices>
#include <QHash>
#include <QCryptographicHash>
#include "messagesmodel.h"
#include <openssl/rand.h>
#include <olm/olm.h>
#include "enc-util.h"

Messages::Messages() : m_messages { new MessagesModel() }, users { new QJsonObject() }, json_messages { new QJsonObject }, olm_sessions { new QJsonObject }, megolm_sessions { new QJsonObject }, devices { new QJsonObject }, messages_queue { new QQueue<QJsonObject>()}, encrypted_messages_queue { new QQueue<QJsonObject>() }, uploads {new QHash<QString,EncryptedFile>() }, m_local_event_ids { new QList<QString> }, m_sending { false }, m_fully_loaded { false }, outbound_megolm_session { nullptr }, message_index {0}, send_to_device_room_key { new QJsonObject }, m_messages_sent {0}, creation_time {0}, seen_message_indices { new QJsonObject }, m_encrypted { false }, last_sent_fully_read{ QString() }, get_immediately { true }, m_filter_id( QString("" ))
{
    QFile cache_file(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/org.yeheng/sailtrix/users.json");
    if (cache_file.open(QFile::ReadOnly)) {
        QJsonDocument doc = QJsonDocument::fromJson(cache_file.readAll());
        *users = doc.object();
        cache_file.close();
    }
}

Messages::~Messages() {
    qDebug() << "Deleting";
    delete m_messages;
    delete getter;
    delete avatar_getter;
    delete message_sender;
    delete device_getter;
    delete otk_claimer;
    delete rk_sender;
    delete member_getter;
    delete marker_setter;
    delete filter_maker;
    delete users;
    delete json_messages;
    delete olm_sessions;
    delete megolm_sessions;
    delete devices;
    delete messages_queue;
    delete encrypted_messages_queue;
    delete m_local_event_ids;
    delete send_to_device_room_key;
    delete seen_message_indices;
    delete image_getter;
    delete file_uploader;
    delete uploads;
}
MessagesModel* Messages::messages() {
    return m_messages;
}

void Messages::setMessages(MessagesModel* model) {
    m_messages = model;
}

QString Messages::rid() {
    return m_id;
}

void Messages::setRid(QString id) {
    if (id == m_id) {
        return;
    }

    m_id = id;
}

bool Messages::typing() {
    return m_typing;
}

void Messages::setTyping(bool typing) {
    m_typing = typing;
}


bool Messages::fully_loaded() {
    return m_fully_loaded;
}

void Messages::setFullyLoaded(bool fullyLoaded) {
    m_fully_loaded = fullyLoaded;
}

QString Messages::reply_mid() {
    return m_reply_mid;
}

void Messages::setReplyMid(QString reply_mid) {
    m_reply_mid = reply_mid;
}

QString Messages::reply_orig_uid() {
    return m_reply_orig_uid;
}

void Messages::setReplyOrigUid(QString reply_orig_uid) {
    m_reply_orig_uid = reply_orig_uid;
}

QString Messages::reply_orig_body() {
    return m_reply_orig_body;
}

void Messages::setReplyOrigBody(QString reply_orig_body) {
    m_reply_orig_body = reply_orig_body;
}

QString Messages::reply_orig_formatted() {
    return m_reply_orig_formatted;
}

void Messages::setReplyOrigFormatted(QString reply_orig_formatted) {
    m_reply_orig_formatted = reply_orig_formatted;
}

QString Messages::edit_event_id() {
    return m_edit_event_id;
}

void Messages::setEditEventId(QString edit_event_id) {
    m_edit_event_id = edit_event_id;
}

QUrl Messages::getUrl() {
    QString filter_str = getFilter();
    if (!m_filter_id.isNull() && !m_filter_id.isEmpty()) {
        filter_str = m_filter_id;
    }
    if (get_immediately) {
        if (m_next_batch.isNull() || m_next_batch.isEmpty()) {
            return QUrl(m_hs_url + "/_matrix/client/r0/sync?filter=" + filter_str);
        }
        return QUrl(m_hs_url + "/_matrix/client/r0/sync?filter=" + filter_str + "&since=" + m_next_batch);
    }
    if (m_next_batch.isNull() || m_next_batch.isEmpty()) {
        return QUrl(m_hs_url + "/_matrix/client/r0/sync?filter=" + filter_str);
    }
    return QUrl(m_hs_url + "/_matrix/client/r0/sync?filter=" + filter_str + "&since=" + m_next_batch + "&timeout=30000");
}

QString Messages::getFilter() {
    return QString("{ \"account_data\": { \"types\" : [\"m.direct\", \"m.ignored_user_list\"] }, \"room\": { \"rooms\" : [\"" + m_id +"\"], \"account_data\": { \"types\" : []}, \"state\": { \"lazy_load_members\": " + (m_encrypted ? "false" : "true") + ", \"rooms\": [\"" + m_id + "\"], \"types\": [\"m.room.name\", \"m.room.avatar\", \"m.room.member\", \"m.room.encryption\", \"m.room.encrypted\"]}, \"ephemeral\": { \"types\": [], \"limit\" : 1}, \"timeline\":{ \"rooms\": [\"" + m_id + "\"], \"types\":[\"m.room.message\", \"m.room.redaction\", \"m.room.encrypted\",\"m.room.member\"],\"limit\":30}}}");
}

void Messages::process_remote_echo(QString event_id, QString txn_id) {
    qDebug() << "Processing: " << txn_id;

    if (m_local_event_ids->contains(txn_id)) {
        m_local_event_ids->removeAll(txn_id);

        int row_id = -1;
        for (int i = 0; i < m_messages->rowCount(); i++) {
            if (m_messages->data(m_messages->index(i, 0), MessagesModel::txn_id).toString() == txn_id) {
                row_id = i;
                break;
            }
        }

        if (row_id != -1) {
            m_messages->removeRow(row_id);
        }

        emit messagesChanged();
        qDebug() << "ROW ID" << row_id;
    }
}

void Messages::write_cache() {
    QString sanitized = m_id;
    sanitized.replace("!", "");
    sanitized.replace(":", "");

    QFile file(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/org.yeheng/sailtrix/room-" + sanitized + ".aes");
    if (file.open(QFile::WriteOnly)) {
        QJsonArray arr;
        for (int i = 0; i < m_messages->rowCount(); i++) {
            QJsonObject for_this_row;
            for_this_row.insert("user_id", m_messages->data(m_messages->index(i,0), MessagesModel::user_id).toString());
            for_this_row.insert("display_name", m_messages->data(m_messages->index(i,0), MessagesModel::display_name).toString());
            for_this_row.insert("content", m_messages->data(m_messages->index(i,0), MessagesModel::content).toString());
            for_this_row.insert("avatar_mxc", m_messages->data(m_messages->index(i,0), MessagesModel::avatar_mxc).toString());
            for_this_row.insert("avatar", m_messages->data(m_messages->index(i,0), MessagesModel::avatar).toString());
            for_this_row.insert("is_self", m_messages->data(m_messages->index(i,0), MessagesModel::is_self).toBool());
            for_this_row.insert("event_id", m_messages->data(m_messages->index(i,0), MessagesModel::event_id).toString());
            for_this_row.insert("orig_body", m_messages->data(m_messages->index(i, 0), MessagesModel::orig_body).toString());
            for_this_row.insert("is_deleted", m_messages->data(m_messages->index(i,0), MessagesModel::is_deleted).toBool());
            for_this_row.insert("is_image", m_messages->data(m_messages->index(i, 0), MessagesModel::is_image).toBool());
            for_this_row.insert("img_url", m_messages->data(m_messages->index(i, 0), MessagesModel::img_url).toString());
            for_this_row.insert("thumbnail_url", m_messages->data(m_messages->index(i, 0), MessagesModel::thumbnail_url).toString());
            for_this_row.insert("image_path", m_messages->data(m_messages->index(i, 0), MessagesModel::image_path).toString());
            for_this_row.insert("thumbnail_path", m_messages->data(m_messages->index(i, 0), MessagesModel::thumbnail_path).toString());
            for_this_row.insert("image_width", m_messages->data(m_messages->index(i, 0), MessagesModel::image_width).toInt());
            for_this_row.insert("image_name", m_messages->data(m_messages->index(i, 0), MessagesModel::image_name).toString());
            for_this_row.insert("is_notice", m_messages->data(m_messages->index(i, 0), MessagesModel::is_notice).toBool());
            for_this_row.insert("is_audio", m_messages->data(m_messages->index(i, 0), MessagesModel::is_audio).toBool());
            for_this_row.insert("is_video", m_messages->data(m_messages->index(i, 0), MessagesModel::is_video).toBool());
            for_this_row.insert("is_file", m_messages->data(m_messages->index(i, 0), MessagesModel::is_file).toBool());
            for_this_row.insert("timestamp", m_messages->data(m_messages->index(i, 0), MessagesModel::timestamp).toLongLong());


            arr.append(for_this_row);
        }

        json_messages->insert("msg", arr);
        json_messages->insert("next_batch", m_next_batch);
        json_messages->insert("encrypted", m_encrypted);
        json_messages->insert("encryption_type", m_encryption_type);
        json_messages->insert("rotation_period_ms", m_rotation_ms);
        json_messages->insert("rotation_period_msg", m_rotation_msg);
        json_messages->insert("messages_sent", m_messages_sent);
        json_messages->insert("creation_time", creation_time);
        json_messages->insert("devices", *devices);
        json_messages->insert("seen_message_indices", *seen_message_indices);
        json_messages->insert("filter_id", m_filter_id);
        json_messages->insert("version", "1.3");

        if (m_encrypted && (outbound_megolm_session != nullptr)) {
            char* pickled_outbound_megolm = (char*) malloc(olm_pickle_outbound_group_session_length(outbound_megolm_session));

            if (pickled_outbound_megolm == nullptr) {
                qFatal("Cannot allocate memory for pickle");
            }
            size_t result = olm_pickle_outbound_group_session(outbound_megolm_session, m_user_id.toUtf8().data(), m_user_id.toUtf8().length(),
                                                              pickled_outbound_megolm, olm_pickle_outbound_group_session_length(outbound_megolm_session));

            if (result != olm_error()) {
                json_messages->insert("outbound_session", QString::fromUtf8(pickled_outbound_megolm, result));
            }
        }

        QJsonDocument doc;
        doc.setObject(*json_messages);
        QByteArray encrypted = encrypt_bytes(&m_cryptoManager, m_local_key, doc.toJson());
        file.write(encrypted);
        file.close();
    }

}

void Messages::read_cache() {
    QString sanitized = m_id;
    sanitized.replace("!", "");
    sanitized.replace(":", "");

    QFile file(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/org.yeheng/sailtrix/room-" + sanitized + ".aes");

    if (file.open(QFile::ReadOnly)) {
        QByteArray decrypted = decrypt_bytes(&m_cryptoManager, m_local_key, file.readAll());
        QJsonDocument doc = QJsonDocument::fromJson(decrypted);
        file.close();

        if (doc.object().value("version").isUndefined()) {
            return;
        }

        *json_messages = doc.object();

        m_next_batch = json_messages->value("next_batch").toString();
        m_encrypted = json_messages->value("encrypted").toBool();
        m_encryption_type = json_messages->value("encryption_type").toString();
        m_rotation_ms = json_messages->value("rotation_period_ms").toInt();
        m_rotation_msg = json_messages->value("rotation_period_msg").toInt();
        m_messages_sent = json_messages->value("messages_sent").toInt();
        creation_time = json_messages->value("creation_time").toVariant().toLongLong();
        *devices = json_messages->value("devices").toObject();
        *seen_message_indices = json_messages->value("seen_message_indices").toObject();
        m_filter_id = json_messages->value("filter_id").toString();

        if (m_encrypted) {
            QString pickled_outbound = json_messages->value("outbound_session").toString();
            if (!pickled_outbound.isEmpty() && !pickled_outbound.isNull()) {
                outbound_megolm_session = (OlmOutboundGroupSession*) malloc(olm_outbound_group_session_size());

                if (outbound_megolm_session == nullptr) {
                    qFatal("Cannot allocate memory");
                }

                outbound_megolm_session = olm_outbound_group_session(outbound_megolm_session);

                size_t result = olm_unpickle_outbound_group_session(outbound_megolm_session, m_user_id.toUtf8().data(),
                                                                    m_user_id.toUtf8().length(),
                                                                    pickled_outbound.toUtf8().data(),
                                                                    pickled_outbound.toUtf8().length());

                unsigned char* id_bytes = (unsigned char*) malloc(olm_outbound_group_session_id_length(outbound_megolm_session));
                size_t id_result = olm_outbound_group_session_id(outbound_megolm_session, id_bytes, olm_outbound_group_session_id_length(outbound_megolm_session));

                if (result == olm_error() || id_result == olm_error()) {
                    free(outbound_megolm_session);
                    outbound_megolm_session = nullptr;
                    free(id_bytes);
                    qWarning() << "Get outbound megolm error: " << olm_outbound_group_session_last_error(outbound_megolm_session);
                } else {
                    QString session_id = QString::fromUtf8((char*) id_bytes, id_result);
                    free(id_bytes);

                    m_outbound_session_id = session_id;
                }
            }
        }

        QJsonArray msgs = json_messages->value("msg").toArray();

        for (QJsonValue value : msgs) {
            QJsonObject this_message = value.toObject();

            QStandardItem* this_row = new QStandardItem();
            this_row->setData(this_message.value("user_id").toString(), MessagesModel::user_id);
            this_row->setData(this_message.value("display_name").toString(), MessagesModel::display_name);
            this_row->setData(this_message.value("content").toString(), MessagesModel::content);
            this_row->setData(this_message.value("avatar_mxc").toString(), MessagesModel::avatar_mxc);
            this_row->setData(this_message.value("avatar").toString(), MessagesModel::avatar);
            this_row->setData(this_message.value("is_self").toBool(), MessagesModel::is_self);
            this_row->setData(this_message.value("event_id").toString(), MessagesModel::event_id);
            this_row->setData(this_message.value("orig_body").toString(), MessagesModel::orig_body);
            this_row->setData(this_message.value("is_deleted").toBool(), MessagesModel::is_deleted);
            this_row->setData(this_message.value("is_image").toBool(), MessagesModel::is_image);
            this_row->setData(this_message.value("img_url").toString(), MessagesModel::img_url);
            this_row->setData(this_message.value("thumbnail_url").toString(), MessagesModel::thumbnail_url);
            this_row->setData(this_message.value("image_path").toString(), MessagesModel::image_path);
            this_row->setData(this_message.value("thumbnail_path").toString(), MessagesModel::thumbnail_path);
            this_row->setData(this_message.value("image_width").toInt(), MessagesModel::image_width);
            this_row->setData(this_message.value("image_name").toString(), MessagesModel::image_name);
            this_row->setData(this_message.value("is_notice").toBool(), MessagesModel::is_notice);
            this_row->setData(this_message.value("is_audio").toBool(), MessagesModel::is_audio);
            this_row->setData(this_message.value("is_video").toBool(), MessagesModel::is_video);
            this_row->setData(this_message.value("is_file").toBool(), MessagesModel::is_file);
            this_row->setData(this_message.value("timestamp").toVariant().toLongLong(), MessagesModel::timestamp);

            if (m_messages->data(m_messages->index(m_messages->rowCount()-1,0), MessagesModel::user_id).toString() == this_message.value("user_id").toString()) {
                this_row->setData(true, MessagesModel::is_grouped);
            }

            m_messages->appendRow(this_row);
        }

        emit newMessage();
    }


}
bool Messages::load() {
    QByteArray secret_stored = get_secret(&m_secretManager, config_secret_id());

    if (secret_stored != nullptr) {
        QJsonDocument config_doc = QJsonDocument::fromJson(secret_stored);

        if (config_doc.object().value("home_server").toString() == ""
                || config_doc.object().value("access_token").toString() == "") return false;

        m_hs_url = config_doc.object().value("home_server").toString();
        m_access_token = config_doc.object().value("access_token").toString();
        m_user_id = config_doc.object().value("user_id").toString();
        m_device_id = config_doc.object().value("device_id").toString();

        m_local_key = get_key(&m_cryptoManager, file_key());

        avatar_getter = new QNetworkAccessManager(this);
        connect(avatar_getter, &QNetworkAccessManager::finished, this, &Messages::processAvatar);

        getter = new QNetworkAccessManager(this);
        connect(getter, &QNetworkAccessManager::finished, this, &Messages::processMessages);

        message_sender = new QNetworkAccessManager(this);
        connect(message_sender, &QNetworkAccessManager::finished, this, &Messages::processSentMessage);

        redactor = new QNetworkAccessManager(this);
        connect(redactor, &QNetworkAccessManager::finished, this, &Messages::afterRedact);

        device_getter = new QNetworkAccessManager(this);
        connect(device_getter, &QNetworkAccessManager::finished, this, &Messages::processDevices);

        otk_claimer = new QNetworkAccessManager(this);
        connect(otk_claimer, &QNetworkAccessManager::finished, this, &Messages::processNewOneTimeKeys);

        rk_sender = new QNetworkAccessManager(this);
        connect(rk_sender, &QNetworkAccessManager::finished, this, &Messages::afterSendRoomKeys);

        member_getter = new QNetworkAccessManager(this);
        connect(member_getter, &QNetworkAccessManager::finished, this, &Messages::processMembers);

        marker_setter = new QNetworkAccessManager(this);
        connect(marker_setter, &QNetworkAccessManager::finished, this, &Messages::afterFullyRead);

        filter_maker = new QNetworkAccessManager(this);
        connect(filter_maker, &QNetworkAccessManager::finished, this, &Messages::processFilter);

        image_getter = new QNetworkAccessManager(this);
        connect(image_getter, &QNetworkAccessManager::finished, this, &Messages::processImage);

        file_uploader = new QNetworkAccessManager(this);
        connect(file_uploader, &QNetworkAccessManager::finished, this, &Messages::afterUpload);

        read_cache();

        QNetworkRequest req(getUrl());
        req.setRawHeader(QByteArray("Authorization"), QString("Bearer " + m_access_token).toUtf8());

        getter->get(req);
    }

    QFile encryption_keys(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/org.yeheng/sailtrix/identity_keys.json");
    if (encryption_keys.open(QFile::ReadOnly)) {
        QJsonDocument ek_doc = QJsonDocument::fromJson(encryption_keys.readAll());
        m_curve25519_id = ek_doc.object().value("curve25519").toString();
        m_ed25519_key = ek_doc.object().value("ed25519").toString();
        encryption_keys.close();
    }

    QFile pickle_file(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/org.yeheng/sailtrix/pickle-jar.aes");
    if (pickle_file.open(QFile::ReadOnly)) {
        QByteArray decrypted = decrypt_bytes(&m_cryptoManager, m_local_key, pickle_file.readAll());
        QJsonDocument pickle_doc = QJsonDocument::fromJson(decrypted);
        QString pickle_str = pickle_doc.object().value("pickle").toString();

        account = (OlmAccount*) malloc(olm_account_size());

        if (account == nullptr) {
            qFatal("Cannot allocate memory for account");
        }

        account = olm_account(account);
        olm_unpickle_account(account, m_user_id.toUtf8().data(), m_user_id.toUtf8().size(), pickle_str.toUtf8().data(), pickle_str.toUtf8().size());
        pickle_file.close();
    }

    *olm_sessions = read_olm_sessions(&m_cryptoManager, m_local_key);
    *megolm_sessions = read_megolm_sessions(&m_cryptoManager, m_local_key);

    json_messages = new QJsonObject();

    return true;
}


void Messages::processFilter(QNetworkReply* reply) {
    qDebug() << "Processing new filter" << reply->errorString();
    if (reply->error() == QNetworkReply::NoError) {
        qDebug() << "Filter Created";
        QJsonDocument reply_doc = QJsonDocument::fromJson(reply->readAll());
        m_filter_id = reply_doc.object().value("filter_id").toString();
    } else {
        qDebug() << reply->readAll();
    }
}

void Messages::processMessages(QNetworkReply* reply) {
    if (reply->error() != reply->NoError) {
        qDebug() << "ERROR! " << reply->errorString();
    }

    if (m_filter_id.isNull() || m_filter_id.isEmpty()) {
        QNetworkRequest makeFilterRequest(m_hs_url + "/_matrix/client/r0/user/" + m_user_id + "/filter");
        makeFilterRequest.setRawHeader(QByteArray("Authorization"), (QString("Bearer " + m_access_token).toUtf8()));
        makeFilterRequest.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

        filter_maker->post(makeFilterRequest, getFilter().toUtf8());
    }

    get_immediately = false;
    QJsonDocument reply_doc = QJsonDocument::fromJson(reply->readAll());
    qDebug() << reply->url();
    QJsonObject master_obj = reply_doc.object();

    QJsonObject timeline = master_obj.value("rooms")
            .toObject()
            .value("join")
            .toObject()
            .value(m_id)
            .toObject()
            .value("timeline")
            .toObject();
    QJsonArray events = timeline.value("events").toArray();

    QJsonObject state = master_obj.value("rooms")
            .toObject()
            .value("join")
            .toObject()
            .value(m_id)
            .toObject()
            .value("state")
            .toObject();

    QJsonArray to_Device_events = master_obj.value("to_device")
            .toObject()
            .value("events")
            .toArray();
    QJsonArray state_events = state.value("events").toArray();
    qDebug() << "NUMBER OF TO-DEVICE: " << to_Device_events.size();


    for (QJsonValue event : to_Device_events) {
        QJsonObject event_obj = event.toObject();
        QJsonObject content = event_obj.value("content").toObject();

        QString type = event_obj.value("type").toString();
        qDebug() << "Type of to-device:" << type;

        if (type == QString("m.room.encrypted")) {
            qDebug() << "Got encrypted to-device";
            QString decrypted = decrypt(content.value("ciphertext"),
                                        content.value("algorithm").toString(),
                                        content.value("sender_key").toString(),
                                        content.value("device_id").toString(),
                                        content.value("session_id").toString(),
                                        m_curve25519_id,
                                        olm_sessions,
                                        megolm_sessions,
                                        m_user_id,
                                        account,
                                        event_obj.value("sender").toString(),
                                        &message_index);
            write_olm_sessions(*olm_sessions, &m_cryptoManager, m_local_key);

            if (!check_olm(decrypted,
                           event_obj.value("sender").toString(),
                           m_user_id, m_ed25519_key)) {
                qWarning() << "Olm plaintext properties mismatch!";
                continue;
            }

            QJsonDocument decrypted_json = QJsonDocument::fromJson(decrypted.toUtf8());
            QJsonObject decrypted_obj = decrypted_json.object();

            qDebug() << "The type is:" << decrypted_obj.value("type").toString();

            if (decrypted_obj.value("type").toString() == QString("m.room_key")) {
                QJsonObject room_key_content = decrypted_obj.value("content").toObject();

                if (!megolm_sessions->contains(room_key_content.value("session_id").toString())) {
                    OlmInboundGroupSession* session = (OlmInboundGroupSession*) malloc(olm_inbound_group_session_size());
                    if (session == nullptr) {
                        qFatal("Cannot allocate memory for session");
                    }

                    session = olm_inbound_group_session(session);

                    size_t result = olm_init_inbound_group_session(session, (unsigned char*) room_key_content.value("session_key").toString().toUtf8().data(), room_key_content.value("session_key").toString().toUtf8().length());
                    if (result != olm_error()) {
                        char* pickle_igs = (char*) malloc(olm_pickle_inbound_group_session_length(session));

                        if (pickle_igs == nullptr) {
                            qFatal("Cannot allocate memory for pickle");
                        }

                        size_t pickle_length = olm_pickle_inbound_group_session(session, m_user_id.toUtf8().data(), m_user_id.toUtf8().length(), pickle_igs, olm_pickle_inbound_group_session_length(session));

                        if (pickle_length != olm_error()) {
                            QJsonObject obj = room_key_content;
                            obj.insert("pickle", QString::fromUtf8(pickle_igs, pickle_length));
                            megolm_sessions->insert(room_key_content.value("session_id").toString(), obj);
                            write_megolm_sessions(*megolm_sessions, &m_cryptoManager, m_local_key);
                        } else {
                            qWarning() << "failed to pickle group session:" << olm_inbound_group_session_last_error(session);
                        }

                        free(pickle_igs);
                    } else {
                        qWarning() << "Olm init inbound group session error: " << olm_inbound_group_session_last_error(session);
                    }

                    free(session);
                }
            }

        }
    }

    bool new_members = has_new_members;
    QJsonArray* new_users = new QJsonArray();
    for (QJsonValue event : state_events) {
        QJsonObject event_obj = event.toObject();
        QJsonObject content = event_obj.value("content").toObject();


        if (event_obj.value("type").toString() == QString("m.room.encrypted")) {
            QString decrypted = decrypt(content.value("ciphertext"),
                                        content.value("algorithm").toString(),
                                        content.value("sender_key").toString(),
                                        content.value("device_id").toString(),
                                        content.value("session_id").toString(),
                                        m_curve25519_id,
                                        olm_sessions,
                                        megolm_sessions,
                                        m_user_id,
                                        account,
                                        event_obj.value("sender").toString(),
                                        &message_index);
            write_olm_sessions(*olm_sessions, &m_cryptoManager, m_local_key);

            if (!check_olm(decrypted,
                           event_obj.value("sender").toString(),
                           m_user_id, m_ed25519_key)) {
                qWarning() << "Olm plaintext properties mismatch!";
                continue;
            }

            QJsonDocument decrypted_json = QJsonDocument::fromJson(decrypted.toUtf8());
            QJsonObject decrypted_obj = decrypted_json.object();

            if (!new_members)
                new_members = process_state_event(decrypted_obj, new_users);
            else
                process_state_event(decrypted_obj, new_users);
        } else {
            if (!new_members)
                new_members = process_state_event(event_obj, new_users);
            else
                process_state_event(event_obj, new_users);
        }
    }

    QJsonObject device_lists = master_obj.value("device_lists").toObject();
    QJsonArray changed = device_lists.value("changed").toArray();
    QJsonArray left = device_lists.value("left").toArray();

    for (QJsonValue left_val : left) {
        users->remove(left_val.toString());

    }

    if (!changed.isEmpty()) {
        has_new_members = true;
    }


    // delete new_users;


    bool added = false;

    for (QJsonValue event : events) {
        QJsonObject event_obj = event.toObject();

        if (event_obj.value("type").toString() == QString("m.room.member") || event_obj.value("type").toString() == QString("m.room.encryption")) {
            if (!new_members) {
                new_members = process_state_event(event_obj, new_users);
            } else {
                process_state_event(event_obj, new_users);
            }
        }

        QJsonObject content = event_obj.value("content").toObject();

        if (event_obj.value("type").toString() == QString("m.room.encrypted")) {
            QString algorithm = content.value("algorithm").toString();

            if (algorithm == QString("m.megolm.v1.aes-sha2")) {
                qDebug() << "Got a MegOlm event!!";
                QString decrypted = decrypt(content.value("ciphertext"),
                                            content.value("algorithm").toString(),
                                            content.value("sender_key").toString(),
                                            content.value("device_id").toString(),
                                            content.value("session_id").toString(),
                                            m_curve25519_id,
                                            olm_sessions,
                                            megolm_sessions,
                                            m_user_id,
                                            account,
                                            event_obj.value("sender").toString(),
                                            &message_index);

                QJsonDocument processed = QJsonDocument::fromJson(decrypted.toUtf8());

                QJsonArray indicies_arr = seen_message_indices->value(content.value("session_id").toString()).toArray();

                if (processed.object().value("content").toObject().value("msgtype").toString() != "m.bad_encrypt" && indicies_arr.contains((int) message_index)) {
                    QJsonDocument doc = QJsonDocument::fromJson(bad_encrypt_str(event_obj.value("sender").toString(), "Repeated message_index detected, possible replay attack").toUtf8());
                    added = process_message(doc.object());
                } else {
                    if (processed.object().value("content").toObject().value("msgtype").toString() != "m.bad_encrypt")
                        indicies_arr.append((int) message_index);

                    if (m_fully_loaded || processed.object().value("content").toObject().value("msgtype").toString() != "m.bad_encrypt")
                        added = process_message(processed.object(), event_obj.value("event_id").toString(), event_obj.value("unsigned").toObject().value("transaction_id").toString(), event_obj.value("origin_server_ts").toVariant().toLongLong(), event_obj.value("content").toObject().value("m.relates_to").toObject());

                    seen_message_indices->insert(content.value("session_id").toString(), indicies_arr);
                }
            }
        } else {
            added = process_message(event_obj);
        }

    }

    has_new_members = new_members;
    qDebug() << "New members:" << has_new_members;
    if (new_members && m_encrypted) {
        QJsonDocument get_devices_doc;
        QJsonObject get_devices_obj;
        QJsonObject devices_obj;

        int i = 0;
        for (QJsonValue val : *new_users) {
            QString this_user_id = val.toString();
            qDebug() << "Adding" << this_user_id << "to device get.";
            devices_obj.insert(this_user_id, QJsonArray());
            i++;
        }

        if (!new_users->isEmpty())
            has_all_devices = false;

        get_devices_obj.insert("device_keys", devices_obj);
        get_devices_obj.insert("timeout", 10000);
        get_devices_doc.setObject(get_devices_obj);

        QNetworkRequest req(m_hs_url + "/_matrix/client/r0/keys/query");
        req.setRawHeader(QByteArray("Authorization"), QString("Bearer " + m_access_token).toUtf8());
        req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");


        device_getter->post(req, get_devices_doc.toJson());
    } else {
        qDebug() << "No New Members";
        has_all_devices = true;
    }


    // write cache

    QFile cache_file(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/org.yeheng/sailtrix/users.json");
    if (cache_file.open(QFile::WriteOnly)) {
        QJsonDocument doc;
        doc.setObject(*users);
        cache_file.write(doc.toJson());
        cache_file.close();
    }

    if (added) {
        emit newMessage();
    }

    emit messagesChanged();

    m_next_batch = master_obj.value("next_batch").toString();

    delete new_users;

    write_cache();

    m_fully_loaded = true;
    emit loaded();

    QNetworkRequest req(getUrl());
    req.setRawHeader(QByteArray("Authorization"), QString("Bearer " + m_access_token).toUtf8());

    getter->get(req);

}

void Messages::processAvatar(QNetworkReply* reply) {
    qDebug() << reply->error();
    if (reply->error() == QNetworkReply::NoError) {
        QString hostname = reply->url().path().split("/").at(5);        QString media_id = reply->url().fileName();
        QMimeDatabase db;
        QString ext = db.mimeTypeForName(reply->rawHeader("Content-Type")).preferredSuffix();
        QFile output(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/org.yeheng/sailtrix/user_avatar/" + hostname + "/" + media_id + "." + ext);
        QDir output_dir(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/org.yeheng/sailtrix/user_avatar/" + hostname);
        output_dir.mkpath(output_dir.path());

        if (output.open(QFile::WriteOnly)) {
            qDebug() << "opened";
            output.write(reply->readAll());
            output.close();

            // find the one to change...
            bool changed = false;
            for (int i = 0; i < m_messages->rowCount(); i++) {
                QString user = m_messages->data(m_messages->index(i, 0), MessagesModel::user_id).toString();
                QString mxc = users->value(user).toObject().value("avatar_mxc").toString();

                QUrl mxc_url(mxc);

                if (mxc_url.fileName() == media_id) { // TODO: This relies on the media ID to be random across servers
                    m_messages->setData(m_messages->index(i, 0), QFileInfo(output).filePath(), MessagesModel::avatar);
                    QJsonObject user_json = users->value(user).toObject();
                    user_json.insert("avatar", QFileInfo(output).filePath());
                    users->insert(user, user_json);
                    changed = true;
                    qDebug() << "Changed";
                }
            }

            if (changed) {
                emit messagesChanged();
            }
        }
    }
}

void Messages::send_rk(QJsonValue device, QString device_id) {
    if (olm_sessions->contains(device.toObject().value("curve25519_key").toString())) {
        OlmSession* session = (OlmSession*) malloc(olm_session_size());

        if (session == nullptr) {
            qFatal("Cannot allocate memory");
        }

        session = olm_session(session);

        QString pickled = olm_sessions->value(device.toObject().value("curve25519_key").toString())
                .toObject()
                .value("sessions")
                .toArray()
                .at(0)
                .toString();


        size_t unpickle_result = olm_unpickle_session(session, m_user_id.toUtf8().data(), m_user_id.toUtf8().length(),
                                                      pickled.toUtf8().data(), pickled.toUtf8().length());

        if (unpickle_result == olm_error()) {
            qWarning() << "Could not unpickle";
            return;
        }

        QString user_id = device.toObject().value("user_id").toString();
        QString device_ed25519_key = device.toObject().value("ed25519_key").toString();
        qDebug() << "already have session for:" << user_id;
        QJsonObject encrypted_event;
        encrypted_event.insert("type", "m.room.encrypted");

        QJsonObject encrypted_content;
        encrypted_content.insert("algorithm", "m.olm.v1.curve25519-aes-sha2");
        encrypted_content.insert("sender_key", m_curve25519_id);
        QJsonObject ciphertext;

        int msg_type = olm_encrypt_message_type(session);

        unsigned char* random_for_encrypt = (unsigned char*) malloc(olm_encrypt_random_length(session));

        if (random_for_encrypt == nullptr) {
            qFatal("Cannot allocate memory");
        }

        RAND_bytes(random_for_encrypt, olm_encrypt_random_length(session));

        QString to_encrypt_str = get_olm_plaintext(room_key_to_encrypt, m_user_id, user_id, device_ed25519_key, m_ed25519_key);

        char* ciphertext_msg = (char*) malloc(olm_encrypt_message_length(session, to_encrypt_str.toUtf8().length()));

        if (ciphertext_msg == nullptr) {
            qFatal("cannot allocate memory");
        }

        size_t encrypt_result = olm_encrypt(session, to_encrypt_str.toUtf8().data(),
                                            to_encrypt_str.toUtf8().length(), random_for_encrypt,
                                            olm_encrypt_random_length(session), ciphertext_msg,
                                            olm_encrypt_message_length(session, to_encrypt_str.toUtf8().length()));

        if (encrypt_result == olm_error()) {
            qWarning() << "cannot encrypt message:" << olm_session_last_error(session);
            free(session);
            free(ciphertext_msg);
            free(random_for_encrypt);
            return;
        }

        qDebug() << olm_session_last_error(session);


        QString ciphertext_str = QString::fromUtf8(ciphertext_msg, encrypt_result);

        QJsonObject device_ciphertext;
        device_ciphertext.insert("body", ciphertext_str);
        device_ciphertext.insert("type", msg_type);

        ciphertext.insert(devices->value(device_id).toObject().value("curve25519_key").toString(), device_ciphertext);
        encrypted_content.insert("ciphertext", ciphertext);

        encrypted_event.insert("content", encrypted_content);

        QJsonObject messages_obj = send_to_device_room_key->value("messages").toObject();
        QJsonObject user_obj = messages_obj.value(user_id).toObject();

        user_obj.insert(device.toObject().value("device_id").toString(), encrypted_content);

        messages_obj.insert(user_id, user_obj);
        send_to_device_room_key->insert("messages", messages_obj);
        m_needed_otk--;


        // pickle olm session
        char* pickle_arr = (char*) malloc(olm_pickle_session_length(session));

        if (pickle_arr == nullptr) {
            qFatal("Cannot allocate memory");
        }

        size_t pickle_result = olm_pickle_session(session, m_user_id.toUtf8().data(), m_user_id.toUtf8().length(), pickle_arr,
                                                  olm_pickle_session_length(session));

        if (pickle_result == olm_error()) {
            qWarning() << "Pickling error" << olm_session_last_error(session);
            free(session);
            free(ciphertext_msg);
            free(random_for_encrypt);
            return;
        }
        QJsonArray arr =  olm_sessions->value(devices->value(device_id).toObject().value("curve25519_key").toString())
                .toObject()
                .value("sessions")
                .toArray();

        arr.insert(0, QString::fromUtf8(pickle_arr, pickle_result));

        QJsonObject obj;
        obj.insert("sessions", arr);

        olm_sessions->insert(devices->value(device_id).toObject().value("curve25519_key").toString(), obj);
        write_olm_sessions(*olm_sessions, &m_cryptoManager, m_local_key);

        qDebug() << m_needed_otk << "more needed";
        if (m_needed_otk <= 0) {
            send_room_key();
        }

        free(session);
        free(ciphertext_msg);
        free(random_for_encrypt);

    } else {
        qDebug() << "Before creating session";
        start_olm_session(device.toObject().value("user_id").toString(), device.toObject().value("device_id").toString());

        qDebug() << "After creating session";
    }
}

void Messages::send_encrypted(QJsonObject obj) {
    m_messages_sent++;

    qint64 current_time = QDateTime::currentDateTimeUtc().toMSecsSinceEpoch();

    qDebug() << m_messages_sent << "messages have already been sent.";
    qDebug() << (current_time - creation_time) << "ms have passed since the creation time.";
    qDebug() << m_rotation_msg << "messages are required to rotate.";
    qDebug() << m_rotation_ms << "ms are required to rotate.";
    if (outbound_megolm_session == nullptr || m_messages_sent > m_rotation_msg || (current_time - creation_time) > m_rotation_ms) {
        encrypted_messages_queue->enqueue(obj);
        qDebug() << "Creating new session";
        create_outbound_megolm_session();
        return;
    } else {
        if (has_new_members) {
            unsigned char* key_bytes = (unsigned char*) malloc(olm_outbound_group_session_key_length(outbound_megolm_session));

            if (key_bytes == nullptr) {
                qFatal("Cannot allocate memory for key bytes");
            }

            size_t key_result = olm_outbound_group_session_key(outbound_megolm_session, key_bytes, olm_outbound_group_session_key_length(outbound_megolm_session));
            if (key_result == olm_error()) {
                free(key_bytes);
                qWarning() << "Error getting megolm key:" << olm_outbound_group_session_last_error(outbound_megolm_session);
                return;
            }

            QString key = QString::fromUtf8((char*) key_bytes, key_result);
            QJsonObject master_room_key;
            master_room_key.insert("algorithm", "m.megolm.v1.aes-sha2");
            master_room_key.insert("room_id", m_id);
            master_room_key.insert("session_id", m_outbound_session_id);
            master_room_key.insert("session_key", key);

            QJsonObject master_room_obj;
            master_room_obj.insert("content", master_room_key);
            master_room_obj.insert("type", "m.room_key");

            QJsonDocument master_room_key_doc;
            master_room_key_doc.setObject(master_room_obj);
            room_key_to_encrypt = master_room_obj;

            encrypted_messages_queue->enqueue(obj);

            m_needed_otk = devices->size();
            if (has_all_devices) {
                int k = 0;
                for (QJsonValue device : *devices) {
                    QString device_id = devices->keys().at(k);
                    send_rk(device, device_id);
                    k++;
                }
            }

            has_new_members = false;
        } else {
            qDebug() << "Enqueuing to encrypted queue";
            encrypted_messages_queue->enqueue(obj);
            send_messages_in_queue();
        }
        return;
    }
}

void Messages::send(QString message) {
    QJsonObject obj = get_message_json(message);
    m_edit_event_id = QString();
    m_reply_mid = QString();

    if (m_encrypted) {
        send_encrypted(obj);
    } else {
        messages_queue->enqueue(obj);

        QJsonDocument doc;
        doc.setObject(obj);

        if (!m_sending) {
            QString txn_id = get_uuid();
            QNetworkRequest req(m_hs_url + "/_matrix/client/r0/rooms/" + m_id + "/send/m.room.message/" + txn_id);
            req.setRawHeader(QByteArray("Authorization"), QString("Bearer " + m_access_token).toUtf8());

            m_sending = true;
            m_local_event_ids->append(txn_id);

            message_sender->put(req, doc.toJson());

            // Local echo

            if (m_edit_event_id.isNull() || m_edit_event_id.isEmpty()) {
                QStandardItem* this_row = new QStandardItem();
                this_row->setData(m_user_id, MessagesModel::user_id);
                this_row->setData("Sending...", MessagesModel::display_name);
                this_row->setData(message, MessagesModel::content);
                this_row->setData("image://theme/icon-s-cloud-upload", MessagesModel::avatar);
                this_row->setData(true, MessagesModel::is_self);
                this_row->setData(txn_id, MessagesModel::txn_id);
                m_messages->appendRow(this_row);
            }

        }
    }
}

void Messages::processSentMessage(QNetworkReply* reply) {
    if (reply->error() != QNetworkReply::NoError) {
        qWarning() << reply->errorString();
        qWarning() << "Response:" << reply->readAll();
        return;
    }

    QJsonDocument doc = QJsonDocument::fromJson(reply->readAll());

    QJsonObject doc_obj = doc.object();

    if (m_encrypted) {
       // encrypted_messages_queue->dequeue();
    } else {
        if (!messages_queue->isEmpty()) {
            messages_queue->dequeue();
        }
    }

    if (messages_queue->isEmpty()) {
        m_sending = false;
    } else {
        qDebug() << "Dequeueing";

        QString txn_id = get_uuid();

        QJsonObject obj = messages_queue->dequeue();
        QJsonDocument doc;
        doc.setObject(obj);

        QNetworkRequest req(m_hs_url + "/_matrix/client/r0/rooms/" + m_id + "/send/m.room.message/" + txn_id);
        req.setRawHeader(QByteArray("Authorization"), QString("Bearer " + m_access_token).toUtf8());

        m_local_event_ids->append(txn_id);
        message_sender->put(req, doc.toJson());
    }
}

void Messages::redact(QString event_id) {
    QNetworkRequest req(m_hs_url + "/_matrix/client/r0/rooms/" + m_id + "/redact/" + event_id + "/" + get_uuid());
    req.setRawHeader(QByteArray("Authorization"), QString("Bearer " + m_access_token).toUtf8());
    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    redactor->put(req, "{}");

    for (int i = 0; i < m_messages->rowCount(); i++) {
        if (m_messages->data(m_messages->index(i, 0), MessagesModel::event_id).toString() == event_id) {
            m_messages->setData(m_messages->index(i, 0), true, MessagesModel::is_deleted);
            break;
        }
    }
}

bool Messages::process_message(QJsonObject event_obj, QString old_event_id, QString old_txn_id, qint64 orig_timestamp, QJsonObject old_relates_to) {
    QJsonObject content = event_obj.value("content").toObject();
    qint64 timestamp;

    if (orig_timestamp == 0L)
         timestamp = event_obj.value("origin_server_ts").toVariant().toLongLong();
    else
        timestamp = orig_timestamp;

    if (event_obj.value("type").toString() == QString("m.room.message")) {
        QJsonObject relates_to;
        if (orig_timestamp != 0L) {
            relates_to = old_relates_to;
        } else {
            relates_to = content.value("m.relates_to").toObject();
        }

        if (relates_to.value("rel_type").toString() == "m.replace") {
            // It is an edit
            QString changed_event = relates_to.value("event_id").toString();

            for (int i = 0; i < m_messages->rowCount(); i++) {
                if (m_messages->data(m_messages->index(i, 0), MessagesModel::event_id).toString() == changed_event) {
                    QJsonObject new_content = content.value("m.new_content").toObject();
                    if (new_content.value("msgtype").toString() == QString("org.matrix.custom.html")) {
                        m_messages->setData(m_messages->index(i, 0), new_content.value("formatted_body").toString(), MessagesModel::content);
                    } else {
                        m_messages->setData(m_messages->index(i, 0), get_html_body(new_content.value("body").toString(), timestamp), MessagesModel::content);
                    }

                    m_messages->setData(m_messages->index(i, 0), new_content.value("body").toString(), MessagesModel::orig_body);

                }
            }
        } else {
            QStandardItem* item = new QStandardItem();
            item->setData(event_obj.value("sender").toString(), MessagesModel::user_id);

            qDebug() << "Processing message of type" << content.value("msgtype").toString();
            if (content.value("msgtype").toString() == QString("m.image") || content.value("msgtype").toString() == QString("m.audio") || content.value("msgtype").toString() == QString("m.video") || content.value("msgtype").toString() == QString("m.file")) {
                if (!content.value("file").isUndefined() && !content.value("file").isNull()) {
                    QString key = content.value("file").toObject().value("key").toObject().value("k").toString();
                    QString iv = content.value("file").toObject().value("iv").toString();
                    QString url = content.value("file").toObject().value("url").toString();

                    qDebug() << "Found media:" << content.value("file");

                    if (content.value("msgtype").toString() == QString("m.image")) item->setData(true, MessagesModel::is_image);
                    if (content.value("msgtype").toString() == QString("m.audio")) item->setData(true, MessagesModel::is_audio);
                    if (content.value("msgtype").toString() == QString("m.video")) item->setData(true, MessagesModel::is_video);
                    if (content.value("msgtype").toString() == QString("m.file")) item->setData(true, MessagesModel::is_file);


                    item->setData(url, MessagesModel::img_url);
                    item->setData(key, MessagesModel::key);
                    item->setData(iv, MessagesModel::iv);
                    item->setData(content.value("file").toObject().value("mimetype").toString(), MessagesModel::mime_type);
                    item->setData(content.value("file").toObject().value("hashes").toObject().value("sha256").toString(), MessagesModel::sha256hash);

                    if (content.value("msgtype").toString() == QString("m.image")) {
                        QNetworkRequest req(m_hs_url + "/_matrix/media/r0/download/" + QUrl(url).host() + "/" + QUrl(url).fileName());
                        req.setRawHeader(QByteArray("Authorization"), QString("Bearer " + m_access_token).toUtf8());
                        image_getter->get(req);
                    }
                } else {
                    QJsonObject image_info = content.value("info").toObject();

                    qDebug() << "Got" << content.value("msgtype").toString();

                    if (content.value("msgtype").toString() == QString("m.image")) item->setData(true, MessagesModel::is_image);
                    if (content.value("msgtype").toString() == QString("m.audio")) item->setData(true, MessagesModel::is_audio);
                    if (content.value("msgtype").toString() == QString("m.video")) item->setData(true, MessagesModel::is_video);
                    if (content.value("msgtype").toString() == QString("m.file")) item->setData(true, MessagesModel::is_file);


                    item->setData(image_info.value("thumbnail_url").toString(), MessagesModel::thumbnail_url);
                    item->setData(content.value("url").toString(), MessagesModel::img_url);

                    if (content.value("msgtype").toString() == QString("m.image")) {
                        QNetworkRequest req(m_hs_url + "/_matrix/media/r0/download/" + QUrl(content.value("url").toString()).host() + "/" + QUrl(content.value("url").toString()).fileName());
                        req.setRawHeader(QByteArray("Authorization"), QString("Bearer " + m_access_token).toUtf8());
                        image_getter->get(req);
                    }
                }
            }

            QString prefix;
            if (content.value("msgtype").toString() == QString("m.emote")) {
                if (users->value(event_obj.value("sender").toString()).toObject().value("display_name").toString().isEmpty() ||
                        users->value(event_obj.value("sender").toString()).toObject().value("display_name").toString().isNull()) {
                    prefix = QString("* " + event_obj.value("sender").toString() + " ");
                } else {
                    prefix = QString("* " + users->value(event_obj.value("sender").toString()).toObject().value("display_name").toString() + " ");
                }
            }

            if (content.value("msgtype").toString() == QString("m.notice")) {
                item->setData(true, MessagesModel::is_notice);
            } else {
                item->setData(false, MessagesModel::is_notice);
            }

            if (content.value("format").toString() == QString("org.matrix.custom.html")) {
                item->setData(prefix + content.value("formatted_body").toString(), MessagesModel::content);
            } else {
                item->setData(get_html_body(prefix + content.value("body").toString(), timestamp), MessagesModel::content);
            }

            item->setData(content.value("body").toString(), MessagesModel::orig_body);

            item->setData(users->value(event_obj.value("sender").toString()).toObject().value("display_name"), MessagesModel::display_name);
            item->setData(users->value(event_obj.value("sender").toString()).toObject().value("avatar"), MessagesModel::avatar);
            item->setData(m_user_id == event_obj.value("sender").toString(), MessagesModel::is_self);

            QJsonObject unsigned_data = event_obj.value("unsigned").toObject();


            if (old_event_id.isNull() || old_event_id.isEmpty()) {
                item->setData(event_obj.value("event_id").toString(), MessagesModel::event_id);
            } else {
                item->setData(old_event_id, MessagesModel::event_id);
            }

            item->setData(timestamp, MessagesModel::timestamp);


            if (old_txn_id.isNull() || old_event_id.isEmpty()) {
                if (old_event_id.isNull() || old_event_id.isEmpty()) {
                    process_remote_echo(event_obj.value("event_id").toString(), unsigned_data.value("transaction_id").toString());
                } else {
                    process_remote_echo(old_event_id, unsigned_data.value("transaction_id").toString());
                }
            } else {
                if (old_event_id.isNull() || old_event_id.isEmpty()) {
                    process_remote_echo(event_obj.value("event_id").toString(), old_txn_id);
                } else {
                    process_remote_echo(old_event_id, old_txn_id);
                }
            }

            if (m_messages->data(m_messages->index(m_messages->rowCount()-1,0), MessagesModel::user_id).toString() == event_obj.value("sender").toString()) {
                item->setData(true, MessagesModel::is_grouped);
            }

            m_messages->appendRow(item);

            return true;
        }
    } else if (event_obj.value("type").toString() == QString("m.room.redaction")) {
        QString redacted_event = event_obj.value("redacts").toString();
        QString redact_reason = content.value("reason").toString();
        QString deleter = users->value(event_obj.value("sender").toString()).toObject().value("display_name").toString();
        if (deleter.isEmpty() || deleter.isNull()) {
            deleter = event_obj.value("sender").toString();
        }


        for (int i = 0; i < m_messages->rowCount(); i++) {
            if (m_messages->data(m_messages->index(i, 0), MessagesModel::event_id) == redacted_event) {
                QString deletion_prefix;
                if (event_obj.value("sender").toString() != m_messages->data(m_messages->index(i,0), MessagesModel::user_id).toString()) {
                    deletion_prefix = " by " + deleter;
                }

                if (redact_reason.isNull() || redact_reason.isEmpty())
                    m_messages->setData(m_messages->index(i, 0), QString("🗑️ Message deleted") + deletion_prefix, MessagesModel::content);
                else
                    m_messages->setData(m_messages->index(i, 0), QString("🗑️ Message deleted" + deletion_prefix + " Reason: ") + redact_reason, MessagesModel::content);
                m_messages->setData(m_messages->index(i, 0), true, MessagesModel::is_deleted);
                m_messages->setData(m_messages->index(i, 0), false, MessagesModel::is_image);
                m_messages->setData(m_messages->index(i, 0), false, MessagesModel::is_audio);

                qDebug() << "Found a deleted message";
            }

        }
    }

    return false;
}

bool Messages::process_state_event(QJsonObject event_obj, QJsonArray* arr) {
    if (event_obj.value("content").toObject().value("membership").toString() == QString("leave") ||
            event_obj.value("content").toObject().value("membership").toString() == QString("ban")) {
        remove_devices_of(event_obj.value("sender").toString());
        free(outbound_megolm_session);
        outbound_megolm_session = nullptr;
        qDebug() << "Removing session";
        write_cache();
        return false;
    }

    if (event_obj.value("type").toString() == QString("m.room.member")) {
        if (event_obj.value("content").toObject().value("membership").toString() != QString("join")) {
            qDebug() << "invite found";
            return false;
        }

        QJsonObject thisUser;
        thisUser.insert("user_id", event_obj.value("state_key").toString());
        thisUser.insert("display_name", event_obj.value("content").toObject().value("displayname").toString());
        thisUser.insert("avatar_mxc", event_obj.value("content").toObject().value("avatar_url").toString());
        thisUser.insert("membership", event_obj.value("content").toObject().value("membership").toString());


        if (!arr->contains(event_obj.value("sender").toString())) {
            arr->append(event_obj.value("sender").toString());
        }

        users->insert(event_obj.value("sender").toString(), thisUser);
        qDebug() << "Found new user:" << thisUser;

        // Trigger the downloader

        QString mxc_url = event_obj.value("content").toObject().value("avatar_url").toString();
        if (!mxc_url.isNull() && !mxc_url.isEmpty() && !m_encrypted) { // encrypted avatars Not Implemented Yet
            QUrl mxc(mxc_url);
            QDir cache_test_dir(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/org.yeheng/sailtrix/user_avatar/" + QUrl(m_hs_url).host()); // TODO use the MXC HS instead
            QDir cache_test_dir2(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/org.yeheng/sailtrix/user_avatar"); // TODO use the MXC HS instead

            bool found = false;

            for (QString fileName : cache_test_dir.entryList()) {
                if (QFileInfo(fileName).completeBaseName() == mxc.fileName()) {
                    found = true;
                    thisUser.insert("avatar", cache_test_dir.filePath(fileName));
                    break;
                }
            }

            if (!found)
                avatar_getter->get(QNetworkRequest(QString(m_hs_url + "/_matrix/media/r0/download/" + mxc.host() + mxc.path())));
        }
        return true;

    } else if (event_obj.value("type").toString() == QString("m.room.encryption")) {
        if (!m_encrypted) { // See implementation guide for why this is done
            m_encrypted = true;

            m_encryption_type = event_obj.value("content").toObject().value("algorithm").toString();

            m_rotation_ms = event_obj.value("content").toObject().value("rotation_period_ms").toInt(604800000);
            m_rotation_msg = event_obj.value("content").toObject().value("rotation_period_msgs").toInt(100);

            // Get all the members
            QNetworkRequest req(m_hs_url + "/_matrix/client/r0/rooms/" + m_id + "/joined_members");
            req.setRawHeader(QByteArray("Authorization"), QString("Bearer " + m_access_token).toUtf8());

            member_getter->get(req);
        }
    }

    return false;
}

void Messages::processDevices(QNetworkReply* reply) {
    qDebug() << "Processing Devices";
    if (!m_encrypted) {
        return;
    }


    if (reply->error() == QNetworkReply::NoError) {
        QJsonDocument reply_doc = QJsonDocument::fromJson(reply->readAll());
        qDebug() << "DEVICES" << reply_doc;
        QJsonObject device_keys = reply_doc.object().value("device_keys").toObject();

        int i = 0;
        for (QJsonValue user : device_keys) {
            QString user_id = device_keys.keys().at(i);

            remove_devices_of(user_id);

            int j = 0;
            for (QJsonValue device : user.toObject()) {
                QString device_id = user.toObject().keys().at(j);

                bool sign_verified = verify_signed_json(device.toObject(),
                                                        user_id,
                                                        device_id,
                                                        device.toObject().value("keys").toObject().value("ed25519:" + device_id).toString());
                if (sign_verified) {
                    if (devices->contains(user_id + ":" + device_id)) {
                        QJsonObject old_dev_obj = devices->value(user_id + ":" + device_id).toObject();
                        if (old_dev_obj.value("ed25519_key").toString() != device.toObject().value("keys").toObject().value("ed25519:" + device_id).toString()) {
                            // Signature key mismatch -reject
                            j++;
                            continue;
                        }
                    }
                    QJsonObject dev_obj;
                    dev_obj.insert("user_id", user_id);
                    dev_obj.insert("device_id", device_id);
                    dev_obj.insert("ed25519_key", device.toObject().value("keys").toObject().value("ed25519:" + device_id).toString());
                    dev_obj.insert("curve25519_key", device.toObject().value("keys").toObject().value("curve25519:" + device_id).toString());
                    devices->insert(user_id + ":" + device_id, dev_obj);
                }
                j++;
            }
            i++;
        }

        has_all_devices = true;
        write_cache();
    } else {
        qWarning() << reply->errorString();
    }
}

bool Messages::create_outbound_megolm_session() {
    outbound_megolm_session = (OlmOutboundGroupSession*) malloc(olm_outbound_group_session_size());
    send_to_device_room_key = new QJsonObject();

    if (outbound_megolm_session == nullptr) {
        qFatal("Cannot allocate memory");
    }

    outbound_megolm_session = olm_outbound_group_session(outbound_megolm_session);

    unsigned char* random = (unsigned char*) malloc(olm_init_outbound_group_session_random_length(outbound_megolm_session));

    if (random == nullptr) {
        qFatal("Cannot allocate memory");
    }

    RAND_bytes(random, olm_init_outbound_group_session_random_length(outbound_megolm_session));
    size_t create_result = olm_init_outbound_group_session(outbound_megolm_session, random, olm_init_outbound_group_session_random_length(outbound_megolm_session));

    free(random);
    if (create_result == olm_error()) {
        qWarning() << "Create megolm session error: " << olm_outbound_group_session_last_error(outbound_megolm_session);
        return false;
    }

    unsigned char* id_bytes = (unsigned char*) malloc(olm_outbound_group_session_id_length(outbound_megolm_session));

    if (id_bytes == nullptr) {
        qFatal("Cannot allocate memory");
    }

    size_t id_result = olm_outbound_group_session_id(outbound_megolm_session, id_bytes, olm_outbound_group_session_id_length(outbound_megolm_session));

    if (id_result == olm_error()) {
        free(id_bytes);
        qWarning() << "Get outbound megolm error: " << olm_outbound_group_session_last_error(outbound_megolm_session);
        return false;
    }

    QString session_id = QString::fromUtf8((char*) id_bytes, id_result);
    free(id_bytes);

    m_outbound_session_id = session_id;

    unsigned char* key_bytes = (unsigned char*) malloc(olm_outbound_group_session_key_length(outbound_megolm_session));

    if (key_bytes == nullptr) {
        qFatal("Cannot allocate memory");
    }

    size_t key_result = olm_outbound_group_session_key(outbound_megolm_session, key_bytes, olm_outbound_group_session_key_length(outbound_megolm_session));
    if (key_result == olm_error()) {
        free(key_bytes);
        qWarning() << "Error getting megolm key:" << olm_outbound_group_session_last_error(outbound_megolm_session);
        return false;
    }

    QString key = QString::fromUtf8((char*) key_bytes, key_result);
    free(key_bytes);

    QJsonObject rk_content_obj;
    rk_content_obj.insert("algorithm", "m.megolm.v1.aes-sha2");
    rk_content_obj.insert("room_id", m_id);
    rk_content_obj.insert("session_id", session_id);
    rk_content_obj.insert("session_key", key);
    rk_content_obj.insert("sender_key", m_curve25519_id);

    OlmInboundGroupSession* session = (OlmInboundGroupSession*) malloc(olm_inbound_group_session_size());

    if (session == nullptr) {
        qFatal("Cannot allocate memory");
    }

    session = olm_inbound_group_session(session);
    olm_init_inbound_group_session(session, (unsigned char*) key.toUtf8().data(), key.toUtf8().length());

    char* pickle_igs = (char*) malloc(olm_pickle_inbound_group_session_length(session));
    size_t pickle_length = olm_pickle_inbound_group_session(session, m_user_id.toUtf8().data(), m_user_id.toUtf8().length(), pickle_igs, olm_pickle_inbound_group_session_length(session));

    if (pickle_length != olm_error()) {
        rk_content_obj.insert("pickle", QString::fromUtf8(pickle_igs, pickle_length));
        megolm_sessions->insert(rk_content_obj.value("session_id").toString(), rk_content_obj);
        write_megolm_sessions(*megolm_sessions, &m_cryptoManager, m_local_key);
    } else {
        qDebug() << "Error pickling:  " << olm_inbound_group_session_last_error(session);
        return false;
    }

    m_messages_sent = 0;
    creation_time = QDateTime::currentDateTimeUtc().toMSecsSinceEpoch();
    write_cache();

    QJsonObject master_room_key;
    master_room_key.insert("algorithm", "m.megolm.v1.aes-sha2");
    master_room_key.insert("room_id", m_id);
    master_room_key.insert("session_id", session_id);
    master_room_key.insert("session_key", key);

    QJsonObject master_room_obj;
    master_room_obj.insert("content", master_room_key);
    master_room_obj.insert("type", "m.room_key");

    QJsonDocument master_room_key_doc;
    master_room_key_doc.setObject(master_room_obj);
    room_key_to_encrypt = master_room_obj;

    qDebug() << "Sending to devices";

    m_needed_otk = devices->size();

    qDebug() << has_all_devices;
    qDebug() << *devices;
    if (has_all_devices) {
        int k = 0;
        for (QJsonValue device : *devices) {
            QString device_id = devices->keys().at(k);
            send_rk(device, device_id);
            k++;
        }
    } else {
        qDebug() << "Don't have all devices yet!";
        return false;
    }
    free(session);
    return true;
}

void Messages::start_olm_session(QString target_user,  QString target_device) {
    QNetworkRequest req(m_hs_url + "/_matrix/client/r0/keys/claim");
    req.setRawHeader(QByteArray("Authorization"), QString("Bearer " + m_access_token).toUtf8());
    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    QJsonObject req_obj;
    req_obj.insert("timeout", 10000);
    QJsonObject one_time_keys;
    QJsonObject user;
    user.insert(target_device, "signed_curve25519");

    one_time_keys.insert(target_user, user);
    req_obj.insert("one_time_keys", one_time_keys);

    QJsonDocument req_doc;
    req_doc.setObject(req_obj);

    qDebug() << req_doc;

    otk_claimer->post(req, req_doc.toJson());}

void Messages::processNewOneTimeKeys(QNetworkReply* reply) {
    if (reply->error() != QNetworkReply::NoError) {
        qWarning() << "Network error while processing one time keys:" << reply->errorString();
    }

    QJsonDocument reply_doc = QJsonDocument::fromJson(reply->readAll());
    QJsonObject otk_obj = reply_doc.object().value("one_time_keys").toObject();
    qDebug() << "OTK:" << reply_doc;

    int i = 0;

    qDebug() << "Staring to process one time keys";

    OlmUtility* util = (OlmUtility*) malloc(olm_utility_size());

    if (util == nullptr) {
        qWarning() << "Cannot allocate memory!";
        return;
    }

    util = olm_utility(util);

    if (otk_obj.isEmpty()) {
        m_needed_otk--;
        qWarning() << "No one time keys to process";
        if (m_needed_otk <= 0) {
            send_room_key();
        }

        return;
    }

    for (QJsonValue user : otk_obj) {
        QString user_id = otk_obj.keys().at(i);
        QJsonObject user_obj = user.toObject();
        int j = 0;
        for (QJsonValue device : user_obj) {
            QString device_id = user_obj.keys().at(j);

            for (QJsonValue key : device.toObject()) {
                QJsonObject key_obj = key.toObject();
                QString signature = key_obj.value("signatures").toObject().value(user_id).toObject().value("ed25519:" + device_id).toString();


                QJsonObject verifiable_key_obj = key_obj;
                verifiable_key_obj.remove("signatures");
                verifiable_key_obj.remove("unsigned");

                QJsonDocument verifiable_key_doc;
                verifiable_key_doc.setObject(verifiable_key_obj);

                QString canonical_json = verifiable_key_doc.toJson(QJsonDocument::Compact);
                QString device_ed25519_key = devices->value(user_id + ":" + device_id).toObject().value("ed25519_key").toString();
                QString device_curve25519_key = devices->value(user_id + ":" + device_id).toObject().value("curve25519_key").toString();

                size_t signature_check = olm_ed25519_verify(util,
                                                            device_ed25519_key.toUtf8().data(),
                                                            device_ed25519_key.toUtf8().length(),
                                                            canonical_json.toUtf8().data(),
                                                            canonical_json.toUtf8().length(),
                                                            signature.toUtf8().data(),
                                                            signature.toUtf8().length());

                if (signature_check != olm_error()) {
                    OlmSession* session = (OlmSession*) malloc(olm_session_size());
                    session = olm_session(session);


                    if (session == nullptr) {
                        qFatal("Cannot allocate memory");
                    }


                    unsigned char* random = (unsigned char*) malloc(olm_create_outbound_session_random_length(session));


                    if (random == nullptr) {
                        qFatal("Cannot allocate memory");
                    }

                    RAND_bytes(random, olm_create_outbound_session_random_length(session));

                    size_t result = olm_create_outbound_session(session,
                                                                account,
                                                                device_curve25519_key.toUtf8().data(),
                                                                device_curve25519_key.toUtf8().length(),
                                                                key_obj.value("key").toString().toUtf8().data(),
                                                                key_obj.value("key").toString().toUtf8().length(),
                                                                random,
                                                                olm_create_outbound_session_random_length(session));


                    if (result == olm_error()) {
                        qWarning() << olm_session_last_error(session);
                        free(session);
                        free(random);
                        continue;
                    }

                    m_needed_otk--;
                    save_account(account, m_user_id, &m_cryptoManager, m_local_key);


                    QJsonObject encrypted_event;
                    encrypted_event.insert("type", "m.room.encrypted");

                    QJsonObject encrypted_content;
                    encrypted_content.insert("algorithm", "m.olm.v1.curve25519-aes-sha2");
                    encrypted_content.insert("sender_key", m_curve25519_id);
                    QJsonObject ciphertext;

                    int msg_type = olm_encrypt_message_type(session);

                    unsigned char* random_for_encrypt = (unsigned char*) malloc(olm_encrypt_random_length(session));


                    if (random_for_encrypt == nullptr) {
                        qFatal("Cannot allocate memory");
                    }

                    RAND_bytes(random_for_encrypt, olm_encrypt_random_length(session));
                    QString rk_str = get_olm_plaintext(room_key_to_encrypt, m_user_id, user_id, device_ed25519_key, m_ed25519_key);


                    char* ciphertext_msg = (char*) malloc(olm_encrypt_message_length(session, rk_str.toUtf8().length()));

                    size_t encrypt_result = olm_encrypt(session, rk_str.toUtf8().data(),
                                                        rk_str.toUtf8().length(), random_for_encrypt,
                                                        olm_encrypt_random_length(session), ciphertext_msg,
                                                        olm_encrypt_message_length(session, rk_str.toUtf8().length()));

                    if (encrypt_result == olm_error()) {
                        qWarning() << "cannot encrypt message:" << olm_session_last_error(session);
                        free(session);
                        free(ciphertext_msg);
                        free(random_for_encrypt);
                        free(random);
                        continue;
                    }

                    QString ciphertext_str = QString::fromUtf8(ciphertext_msg, encrypt_result);
                    QJsonObject device_ciphertext;
                    device_ciphertext.insert("body", ciphertext_str);
                    device_ciphertext.insert("type", msg_type);

                    ciphertext.insert(devices->value(user_id + ":" + device_id).toObject().value("curve25519_key").toString(), device_ciphertext);
                    encrypted_content.insert("ciphertext", ciphertext);

                    encrypted_event.insert("content", encrypted_content);

                    QJsonObject messages_obj = send_to_device_room_key->value("messages").toObject();
                    QJsonObject user_obj = messages_obj.value(user_id).toObject();

                    user_obj.insert(device_id, encrypted_content);

                    messages_obj.insert(user_id, user_obj);
                    send_to_device_room_key->insert("messages", messages_obj);

                    qDebug() << m_needed_otk << "more needed";

                    // pickle olm session
                    char* pickle_arr = (char*) malloc(olm_pickle_session_length(session));


                    if (pickle_arr == nullptr) {
                        qFatal("Cannot allocate memory");
                    }

                    size_t pickle_result = olm_pickle_session(session, m_user_id.toUtf8().data(), m_user_id.toUtf8().length(), pickle_arr,
                                                              olm_pickle_session_length(session));

                    QJsonArray arr =  olm_sessions->value(devices->value(user_id + ":" + device_id).toObject().value("curve25519_key").toString())
                            .toObject()
                            .value("sessions")
                            .toArray();

                    arr.insert(0, QString::fromUtf8(pickle_arr, pickle_result));

                    QJsonObject obj;
                    obj.insert("sessions", arr);

                    olm_sessions->insert(devices->value(user_id + ":" + device_id).toObject().value("curve25519_key").toString(), obj);
                    write_olm_sessions(*olm_sessions, &m_cryptoManager, m_local_key);

                    qDebug() << m_needed_otk << "more needed";
                    if (m_needed_otk <= 0) {
                        send_room_key();
                    }

                    free(session);
                    free(ciphertext_msg);
                    free(random_for_encrypt);
                    free(pickle_arr);
                    free(random);
                } else {
                    qWarning() << "signature mismatch";
                }
            }
            j++;
        }
        i++;
    }

    qDebug() << "end";
    free(util);
}

void Messages::send_room_key() {
    QNetworkRequest req(m_hs_url + "/_matrix/client/r0/sendToDevice/m.room.encrypted/" + get_uuid());
    req.setRawHeader(QByteArray("Authorization"), QString("Bearer " + m_access_token).toUtf8());
    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    QJsonDocument doc;
    doc.setObject(*send_to_device_room_key);

    qDebug() << "Sending";
    rk_sender->put(req, doc.toJson());
}

void Messages::afterSendRoomKeys(QNetworkReply* reply) {
    if (reply->error() == QNetworkReply::NoError) {
        qDebug() << "sent keys successfully";
        send_messages_in_queue();
    } else {
        qWarning() << "Didn't send keys successfully:" << reply->errorString();
    }
}

void Messages::send_messages_in_queue() {
    while (!encrypted_messages_queue->isEmpty()) {
        QJsonObject content = encrypted_messages_queue->dequeue();
        QJsonObject unencrypted;
        unencrypted.insert("type", "m.room.message");
        unencrypted.insert("room_id", m_id);

        unencrypted.insert("content", content);

        QJsonDocument to_encrypt;
        to_encrypt.setObject(unencrypted);
        QString unencrypted_str = to_encrypt.toJson(QJsonDocument::Compact);

        unsigned char* encrypted = (unsigned char*) malloc(olm_group_encrypt_message_length(outbound_megolm_session, unencrypted_str.toUtf8().length()));

        if (encrypted == nullptr) {
            qFatal("Cannot allocate memory");
        }

        size_t encrypted_length = olm_group_encrypt(outbound_megolm_session,
                                                    (unsigned char*) unencrypted_str.toUtf8().data(),
                                                    unencrypted_str.toUtf8().length(),
                                                    encrypted,
                                                    olm_group_encrypt_message_length(outbound_megolm_session, unencrypted_str.toUtf8().length()));

        if (encrypted_length == olm_error()) {
            qWarning() << "Encryption error:" << olm_outbound_group_session_last_error(outbound_megolm_session);
            free(encrypted);
            continue;
        }

        QJsonObject encrypted_obj;
        encrypted_obj.insert("algorithm", "m.megolm.v1.aes-sha2");
        encrypted_obj.insert("sender_key", m_curve25519_id);
        encrypted_obj.insert("ciphertext", QString::fromUtf8((char*) encrypted, encrypted_length));
        encrypted_obj.insert("session_id", m_outbound_session_id);
        encrypted_obj.insert("device_id", m_device_id);

        if (!m_reply_mid.isNull() && !m_reply_mid.isEmpty()) {
            QJsonObject relates_to;
            QJsonObject reply_to;
            reply_to.insert("event_id", m_reply_mid);
            relates_to.insert("m.in_reply_to", reply_to);
            encrypted_obj.insert("m.relates_to", relates_to);
        }

        if (!m_edit_event_id.isNull() && !m_edit_event_id.isEmpty()) {
            QJsonObject relates_to;
            relates_to.insert("rel_type", "m.replace");
            relates_to.insert("event_id", m_edit_event_id);
            encrypted_obj.insert("m.relates_to", relates_to);
        }

        QJsonDocument encrypted_doc;
        encrypted_doc.setObject(encrypted_obj);

        QString txn_id = get_uuid();

        QNetworkRequest req(m_hs_url + "/_matrix/client/r0/rooms/" + m_id + "/send/m.room.encrypted/" + txn_id);
        req.setRawHeader(QByteArray("Authorization"), QString("Bearer " + m_access_token).toUtf8());
        req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");


        if (m_edit_event_id.isNull() || m_edit_event_id.isEmpty()) {
            QStandardItem* this_row = new QStandardItem();
            this_row->setData(m_user_id, MessagesModel::user_id);
            this_row->setData("Sending...", MessagesModel::display_name);
            this_row->setData(content.value("body"), MessagesModel::content);
            this_row->setData("image://theme/icon-s-cloud-upload", MessagesModel::avatar);
            this_row->setData(true, MessagesModel::is_self);
            this_row->setData(txn_id, MessagesModel::txn_id);
            m_messages->appendRow(this_row);
        }

        m_edit_event_id = QString();
        m_reply_mid = QString();

        write_cache();

        qDebug() << "Sending message";
        m_local_event_ids->append(txn_id);
        message_sender->put(req, encrypted_doc.toJson());

        free(encrypted);
    }
}

void Messages::send_file(QString file) {
    QNetworkRequest upload(m_hs_url + "/_matrix/media/r0/upload");
    upload.setRawHeader(QByteArray("Authorization"), QString("Bearer " + m_access_token).toUtf8());

    QFile file_obj(file);
    if (file_obj.open(QFile::ReadOnly)) {
        QByteArray file_contents = file_obj.readAll();
        QString type = QMimeDatabase().mimeTypeForFileNameAndData(file, file_contents).name();

        if (m_encrypted) {
            QString uuid = get_uuid();
            upload.setHeader(QNetworkRequest::ContentTypeHeader, QByteArray("application/octet-stream"));
            upload.setRawHeader("X-Sailtrix-UploadId", uuid.toUtf8());
            QString key;
            QString iv;

            QByteArray ciphertext = encrypt_bytes(&m_cryptoManager, file_contents, key, iv);

            EncryptedFile file;
            file.key = key;
            file.iv = iv;
            file.mimeType = type;
            file.hash = QCryptographicHash::hash(ciphertext, QCryptographicHash::Sha256).toBase64(QByteArray::OmitTrailingEquals);

            uploads->insert(uuid, file);
            file_uploader->post(upload, ciphertext);
        } else {
            upload.setHeader(QNetworkRequest::ContentTypeHeader, type);
            file_uploader->post(upload, file_contents);
        }
        file_obj.close();
    } else {
        emit fileError();
    }
}

void Messages::afterUpload(QNetworkReply* reply) {
    if (reply->error() == QNetworkReply::NoError) {
        QJsonObject reply_obj = QJsonDocument::fromJson(reply->readAll()).object();
        QNetworkRequest req = reply->request();
        QString content_type = req.header(QNetworkRequest::ContentTypeHeader).toString();
        QString upload_id = req.rawHeader("X-Sailtrix-UploadId");

        QJsonObject to_send;

        if (!upload_id.isNull() && !upload_id.isEmpty()) {
            QString key = uploads->value(upload_id).key;
            QString iv = uploads->value(upload_id).iv;
            QString type = uploads->value(upload_id).mimeType;
            QString hash = uploads->value(upload_id).hash;

            content_type = type;

            uploads->remove(upload_id);

            QJsonObject encryptedFile;
            encryptedFile.insert("url", reply_obj.value("content_uri").toString());
            encryptedFile.insert("mimetype", type);
            encryptedFile.insert("v", "v2");
            encryptedFile.insert("iv", iv);

            QJsonObject hashes;
            hashes.insert("sha256", hash);

            encryptedFile.insert("hashes", hashes);

            QJsonObject jwk;
            jwk.insert("kty", "oct");
            jwk.insert("alg", "A256CTR");
            jwk.insert("ext", true);
            jwk.insert("k", key);

            QJsonArray key_ops;
            key_ops.append("encrypt");
            key_ops.append("decrypt");

            jwk.insert("key_ops", key_ops);

            encryptedFile.insert("key", jwk);
            to_send.insert("file", encryptedFile);

        } else {
            to_send.insert("url", reply_obj.value("content_uri"));
        }


        if (content_type.startsWith("image/")) {
            to_send.insert("msgtype", "m.image");
        } else if (content_type.startsWith("video/")) {
            to_send.insert("msgtype", "m.video");
        } else if (content_type.startsWith("audio/")) {
            to_send.insert("msgtype", "m.audio");
        } else {
            to_send.insert("msgtype", "m.file");
        }

        to_send.insert("body", "sailtrix-file." + QMimeDatabase().mimeTypeForName(content_type).preferredSuffix());

        QString txn_id = get_uuid();
        QNetworkRequest send_req(m_hs_url + "/_matrix/client/r0/rooms/" + m_id + "/send/m.room.message/" + txn_id);
        send_req.setRawHeader(QByteArray("Authorization"), QString("Bearer " + m_access_token).toUtf8());

        if (m_encrypted) {
            send_encrypted(to_send);
        } else {
            m_sending = true;
            m_local_event_ids->append(txn_id);

            message_sender->put(send_req, QJsonDocument(to_send).toJson());

            // Local echo

            if (m_edit_event_id.isNull() || m_edit_event_id.isEmpty()) {
                QStandardItem* this_row = new QStandardItem();
                this_row->setData(m_user_id, MessagesModel::user_id);
                this_row->setData("Sending...", MessagesModel::display_name);
                this_row->setData("File", MessagesModel::content);
                this_row->setData("image://theme/icon-s-cloud-upload", MessagesModel::avatar);
                this_row->setData(true, MessagesModel::is_self);
                this_row->setData(txn_id, MessagesModel::txn_id);
                m_messages->appendRow(this_row);

                emit messagesChanged();
            }
        }

    } else {
        qWarning() << "Could not upload:" << reply->error() << reply->errorString() << reply->readAll();
        emit fileError();
    }
}

void Messages::fully_read() {
    QString event_read = m_messages->data(m_messages->index(m_messages->rowCount() - 1, 0), MessagesModel::event_id).toString();
    if (event_read.isNull() || event_read.isEmpty() || event_read == last_sent_fully_read) {
        return;
    }

    QNetworkRequest req(m_hs_url + "/_matrix/client/r0/rooms/" + m_id + "/read_markers");
    req.setRawHeader(QByteArray("Authorization"), QString("Bearer " + m_access_token).toUtf8());
    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    QJsonDocument doc;
    QJsonObject obj;
    obj.insert("m.fully_read", event_read);
    obj.insert("m.read", event_read);
    doc.setObject(obj);

    marker_setter->post(req, doc.toJson());
}

void Messages::processMembers(QNetworkReply* reply) {
    if (reply->error() != QNetworkReply::NoError) {
        qWarning() << "Could not get members:" << reply->errorString();
        return;
    }

    QJsonDocument doc = QJsonDocument::fromJson(reply->readAll());
    QJsonObject joined = doc.object().value("joined").toObject();

    if (m_encrypted) {
        QJsonDocument get_devices_doc;
        QJsonObject get_devices_obj;
        QJsonObject devices_obj;
        for (QString val : joined.keys()) {
            devices_obj.insert(val, QJsonArray());
        }

        if (!(joined.keys()).isEmpty())
            has_all_devices = false;

        get_devices_obj.insert("device_keys", devices_obj);
        get_devices_obj.insert("timeout", 10000);
        get_devices_doc.setObject(get_devices_obj);

        QNetworkRequest req(m_hs_url + "/_matrix/client/r0/keys/query");
        req.setRawHeader(QByteArray("Authorization"), QString("Bearer " + m_access_token).toUtf8());
        req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");


        device_getter->post(req, get_devices_doc.toJson());
    }
}

void Messages::remove_devices_of(QString user_id) {
    for (int i = devices->size() - 1; i >= 0; i--) {
        if (devices->keys().at(i).startsWith(user_id + ":")) {
            devices->remove(devices->keys().at(i));
        }
    }
}

void Messages::afterFullyRead(QNetworkReply* reply) {
    qDebug() << reply->errorString();
    qDebug() << reply->readAll();
}

void Messages::afterRedact(QNetworkReply* reply) {
    qDebug() << reply->error() << reply->errorString() << reply->readAll();
}

void Messages::save_image(QByteArray arr, QString out_path, QString hostname, QString media_id, QString mime_type) {
    if (!mime_type.startsWith("image/") && !mime_type.startsWith("audio/") && !mime_type.startsWith("video/")) {
        for (int i = 0; i < m_messages->rowCount(); i++) {
            if (m_messages->data(m_messages->index(i, 0), MessagesModel::is_file).toBool()) {
                QUrl file_mxc(m_messages->data(m_messages->index(i,0), MessagesModel::img_url).toString());
                if (file_mxc.host() == hostname && file_mxc.fileName() == media_id) {
                    qDebug() << "Found file!";
                    QString filename = m_messages->data(m_messages->index(i,0), MessagesModel::content).toString();
                    out_path = QStandardPaths::writableLocation(QStandardPaths::DownloadLocation) + "/Sailtrix/" + filename;

                    QDir mkdir(QStandardPaths::writableLocation(QStandardPaths::DownloadLocation));
                    mkdir.mkpath("Sailtrix");
                }
            }
        }
    }


    QFile output(out_path);
    qDebug() << "Saving file to " << out_path;

    if (output.open(QFile::WriteOnly)) {
        qDebug() << "opened";
        output.write(arr);
        output.close();

        if (mime_type.startsWith("image/")) {
            for (int i = 0; i < m_messages->rowCount(); i++) {
                if (m_messages->data(m_messages->index(i, 0), MessagesModel::is_image).toBool()) {
                    QImage image(out_path);
                    QUrl thumbnail_mxc(m_messages->data(m_messages->index(i,0), MessagesModel::thumbnail_url).toString());
                    if (thumbnail_mxc.host() == hostname && thumbnail_mxc.fileName() == media_id) {
                        qDebug() << "Got a thumbnail";
                        m_messages->setData(m_messages->index(i,0), out_path, MessagesModel::thumbnail_path);
                        m_messages->setData(m_messages->index(i, 0), image.width(), MessagesModel::image_width);
                        emit messagesChanged();
                        write_cache();
                    }

                    QUrl img_mxc(m_messages->data(m_messages->index(i,0), MessagesModel::img_url).toString());
                    if (img_mxc.host() == hostname && img_mxc.fileName() == media_id) {
                        qDebug() << "Found image!";
                        m_messages->setData(m_messages->index(i,0), out_path, MessagesModel::image_path);
                        m_messages->setData(m_messages->index(i, 0), image.width(), MessagesModel::image_width);

                        emit messagesChanged();
                        write_cache();
                    }
                }

            }
        } else if (mime_type.startsWith("audio/")) {
            for (int i = 0; i < m_messages->rowCount(); i++) {
                if (m_messages->data(m_messages->index(i, 0), MessagesModel::is_audio).toBool()) {
                    QUrl vid_mxc(m_messages->data(m_messages->index(i,0), MessagesModel::img_url).toString());
                    if (vid_mxc.host() == hostname && vid_mxc.fileName() == media_id) {
                        qDebug() << "Found audio!";
                        m_messages->setData(m_messages->index(i,0), out_path, MessagesModel::image_path);
                        emit messagesChanged();
                        write_cache();
                    }
                }

            }
        } else if (mime_type.startsWith("video/")) {
            for (int i = 0; i < m_messages->rowCount(); i++) {
                if (m_messages->data(m_messages->index(i, 0), MessagesModel::is_video).toBool()) {
                    QUrl vid_mxc(m_messages->data(m_messages->index(i,0), MessagesModel::img_url).toString());
                    if (vid_mxc.host() == hostname && vid_mxc.fileName() == media_id) {
                        qDebug() << "Found video!";
                        m_messages->setData(m_messages->index(i,0), out_path, MessagesModel::image_path);
                        emit messagesChanged();
                        write_cache();
                    }
                }

            }
        } else {
            emit fileDownloaded();
        }
    }
}

void Messages::processImage(QNetworkReply* reply) {
    if (reply->error() == QNetworkReply::NoError) {
        QString hostname = reply->url().path().split("/").at(5);
        qDebug() << "GOT IMAGE:" << hostname;
        QString media_id = reply->url().fileName();
        QString ext;
        QMimeDatabase db;

        if (m_encrypted) {
            ext = "aes";
        } else {
            ext = db.mimeTypeForName(reply->rawHeader("Content-Type")).preferredSuffix();
        }

        if (m_encrypted) {
            for (int i = 0; i < m_messages->rowCount(); i++) {
                if (m_messages->data(m_messages->index(i, 0), MessagesModel::is_image).toBool() ||
                        m_messages->data(m_messages->index(i, 0), MessagesModel::is_video).toBool() ||
                        m_messages->data(m_messages->index(i, 0), MessagesModel::is_audio).toBool()) {
                    QUrl img_mxc(m_messages->data(m_messages->index(i,0), MessagesModel::img_url).toString());
                    if (img_mxc.host() == hostname && img_mxc.fileName() == media_id) {
                        QString key = m_messages->data(m_messages->index(i, 0), MessagesModel::key).toString();
                        QString iv = m_messages->data(m_messages->index(i, 0), MessagesModel::iv).toString();
                        QString sha256hash = m_messages->data(m_messages->index(i, 0), MessagesModel::sha256hash).toString();

                        QByteArray ciphertext = reply->readAll();
                        QByteArray decrypted = file_dec2(&m_cryptoManager, ciphertext, QByteArray::fromBase64(key.toUtf8(), QByteArray::Base64UrlEncoding), QByteArray::fromBase64(iv.toUtf8()));

                        QByteArray hash_of_ciphertext = QCryptographicHash::hash(ciphertext, QCryptographicHash::Sha256);
                        QByteArray orig_hash = QByteArray::fromBase64(sha256hash.toUtf8());

                        if (hash_of_ciphertext != orig_hash) {
                            m_messages->setData(m_messages->index(i, 0), false, MessagesModel::is_image);
                            m_messages->setData(m_messages->index(i, 0), false, MessagesModel::is_audio);

                            m_messages->setData(m_messages->index(i, 0), "Unable to decrypt file: hash mismatch", MessagesModel::content);
                            m_messages->setData(m_messages->index(i, 0), "Unable to decrypt file: hash mismatch", MessagesModel::orig_body);
                            emit messagesChanged();
                            return;
                        }

                        ext = db.mimeTypeForName(m_messages->data(m_messages->index(i, 0), MessagesModel::mime_type).toString()).preferredSuffix();

                        QString out_path(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/org.yeheng/sailtrix/files/" + hostname + "/" + media_id + "." + ext);
                        QFile output(out_path);
                        QDir output_dir(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/org.yeheng/sailtrix/files/" + hostname);
                        output_dir.mkpath(output_dir.path());

                        qDebug() << "Saving encrypted image";

                        save_image(decrypted, out_path, hostname, media_id, m_messages->data(m_messages->index(i, 0), MessagesModel::mime_type).toString());
                    }
                }
            }
        } else {
            QString out_path(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/org.yeheng/sailtrix/images/" + hostname + "/" + media_id + "." + ext);
            QFile output(out_path);
            QDir output_dir(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/org.yeheng/sailtrix/images/" + hostname);
            output_dir.mkpath(output_dir.path());

            save_image(reply->readAll(), out_path, hostname, media_id, reply->rawHeader("Content-Type"));
        }

    } else {
        qWarning() << "Cannot process image:" << reply->errorString() << reply->readAll();
    }
}

QJsonObject Messages::get_message_json(QString message) {
    QJsonObject obj;

    if (message.startsWith("/me")) {
        obj.insert("msgtype", "m.emote");
        message = message.remove(0, 4);
    } else {
        obj.insert("msgtype", "m.text");
    }

    obj.insert("body", message);

    if (!m_reply_mid.isNull() && !m_reply_mid.isEmpty()) {
        QJsonObject relates_to;
        QJsonObject reply_to;
        reply_to.insert("event_id", m_reply_mid);
        relates_to.insert("m.in_reply_to", reply_to);
        obj.insert("m.relates_to", relates_to);

        QString new_body = QString("> <") + m_reply_orig_uid + QString("> ") + m_reply_orig_body + "\n\n" + message;
        obj.insert("body", new_body);

        QString new_formatted = QString("<mx-reply><blockquote>"
                                          "<a href=\"httpsL//matrix.to/#/" + m_id + "/" + m_reply_mid + "\">In reply to</a>"
                                          "<a href=\"https://matrix.to/#/" + m_reply_orig_uid + "\">" + m_reply_orig_uid + "</a>"
                                          "<br />"
                                          + (m_reply_orig_body == m_reply_orig_formatted ? m_reply_orig_body.toHtmlEscaped() : m_reply_orig_formatted) +
                                        "</blockquote>"
                                      "</mx-reply>"
                                       + message.toHtmlEscaped());

        obj.insert("formatted_body", new_formatted);
        obj.insert("format", "org.matrix.custom.html");
    }

    if (!m_edit_event_id.isNull() && !m_edit_event_id.isEmpty()) {
        QJsonObject relates_to;
        relates_to.insert("rel_type", "m.replace");
        relates_to.insert("event_id", m_edit_event_id);
        obj.insert("m.relates_to", relates_to);

        QString new_body = QString("* ") + message;
        obj.insert("body", new_body);

        QJsonObject new_content;
        new_content.insert("msgtype", "m.text");
        new_content.insert("body", message);

        obj.insert("m.new_content", new_content);
    }

    qDebug() << "Edit" << m_edit_event_id;
    qDebug() << "Reply" << m_reply_mid;

    return obj;
}

void Messages::openLink(QString link) {
    QDesktopServices::openUrl(QUrl(link));
}

void Messages::download(QString url) {
    QNetworkRequest req(m_hs_url + "/_matrix/media/r0/download/" + QUrl(url).host() + "/" + QUrl(url).fileName());
    req.setRawHeader(QByteArray("Authorization"), QString("Bearer " + m_access_token).toUtf8());
    image_getter->get(req);
}

QString Messages::get_html_body(const QString &plaintext_body, qint64 timestamp) {
    QString result = plaintext_body;
    result.replace("\n", "<br>");
    return result;
}
